;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; FIND UNBOUND KEYS
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(use-package free-keys
  :commands free-keys)

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; GLOBAL UNBIND KEYS
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; disabling M-k, normally bound to (kill-sentence &optional ARG)
;; it's a bit dangerous
(global-unset-key "\M-k")

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; GLOBAL KEY BINDINGS
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; NOTE: may interfere with mc hydra's l
;; get a copy of where point is on file:linenumber in clipboard.  it also prints it.
(define-key global-map (kbd "M-RET") 'copy-current-line-position-to-clipboard)

;; https://github.com/bbatsov/crux
(use-package crux
  :bind ("C-k" . 'crux-smart-kill-line))

;; global keyboard-quit ergo key aliases
(define-key key-translation-map (kbd "C-q") (kbd "C-g"))

;; conflict with auto-fill
;; (define-key key-translation-map (kbd "M-q") (kbd "C-g"))

(define-key global-map (kbd "<f10>") 'my-menu-bar-toggle)

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; KEY CHORDS
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; chords borrowed from prelude.
(use-package key-chord

  :config
  (progn
    (setq key-chord-one-key-delay 0.5)
    (key-chord-mode 1)
    ;; (key-chord-define-global "bb" 'crux-switch-to-previous-buffer)
    (key-chord-define-global "uu" 'undo-tree-visualize)

    ;; copy/cut/paste, aka kill/copy-as-kill/yank
    (key-chord-define-global "XX" 'xah-cut-line-or-region)
    (key-chord-define-global "CC" 'xah-copy-line-or-region)
    (key-chord-define-global "VV" 'yank)
    (key-chord-define-global "ZZ" 'undo)

    (key-chord-define-global "yy" 'counsel-yank-pop)

    ;; disable key-chord-mode in the minibuffer
    (defun disable-key-chord-mode ()
      (set (make-local-variable 'input-method-function) nil))

    ;; disable key-chord in the following buffers and mods
    (add-hook 'minibuffer-setup-hook #'disable-key-chord-mode)
    (add-hook 'magit-mode-hook #'disable-key-chord-mode)))



;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; MENU SYSTEM
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(with-eval-after-load 'hydra

  ;; https://www.reddit.com/r/emacs/comments/78j51h/worlds_greatest_hydra_needs_your_help/

  (defhydra hydra-nav (:color yellow :hint nil)
    "
  _b__f__k__j_ ←→↑↓   _h_/_l_ word   _a_/_e_ b/eol   _d_/_r_ del
  _o_ther-frame-or-window   _y_ank        _u_/_U_ndo/redo
  _k_/_w_/_H_/_J_ kill line/region/lw/rw      _j_/_k_ move-line-or-region
  _=_/_-_ expand/contract     _SPC_ mark    _D_/_W_ dup/copy line/region
---------------------------------------------------------------
  _m_ark-ring    _s_wiper     _g_oto-word   _q_u_i_t
      "
    ("f"   forward-char)
    ("F"   forward-sexp)
    ("b"   backward-char)
    ("B"   backward-sexp)
    ("k"   previous-line)
    ("j"   next-line)
    ("a"   xah-beginning-of-line-or-block)
    ("e"   xah-end-of-line-or-block)
    ("A"   beginning-of-defun)
    ("E"   end-of-defun)
    ("h"   left-word)
    ("l"   right-word)
    ("H"   backward-kill-word)
    ("J"   kill-word)
    ("w"   kill-region)
    ("W"   kill-ring-save)
    ("u"   undo-tree-undo)
    ("U"   undo-tree-redo)
    ("d"   delete-char)
    ("o"   other-window-or-frame)
    ("y"   yank)
    ("t"   transpose-chars)
    ;; ("k"   kill-line)
    ("C"   comment-or-uncomment-region-or-line)
    ("P"   elpy-nav-move-line-or-region-up)
    ("N"   elpy-nav-move-line-or-region-down)
    ("D"   duplicate-current-line-or-region)
    ("z"   zap-to-char)
    ("q"   nil)
    ("x"   quit-and-send-C-x :color blue)
    ("c"   quit-and-send-C-c :color blue)
    ("r"   backward-delete-char-untabify)
    ;; ("C-l"   recenter-top-bottom)
    ("v"   scroll-up-command)
    ("V"   scroll-down-command)
    (","   set-mark-and-deactive)
    ("."   exchange-point-and-mark)
    ("="   er/expand-region)
    ("-"   er/contract-region)
    ("SPC" set-mark-command)
    ("g"   avy-goto-word-1 :color blue)
    ("i"   nil :color blue)
    ("s"   swiper :color blue) ;; blue is sadly needed!
    ("S"   save-buffer)
    ("K"   volatile-kill-buffer)
    ("m"   counsel-mark-ring :color blue)
    )

  (defhydra hydra-undo-tree (:color yellow :hint nil)
    "
  _p_: undo  _n_: redo _s_: save _l_: load   "
    ("p"   undo-tree-undo)
    ("n"   undo-tree-redo)
    ("s"   undo-tree-save-history)
    ("l"   undo-tree-load-history)
    ("u"   undo-tree-visualize "visualize" :color blue)
    ("q"   nil "quit" :color blue)))

(defun helm-ag-.emacs.d-core ()
  (interactive)
  (helm-ag (expand-file-name "core" user-emacs-directory)))

(defun helm-do-ag-default-directory ()
  (interactive)
  (helm-do-ag default-directory))

(defun helm-do-rg-select-directory ()
  (interactive)
  (let ((default-directory
          (car (helm-read-file-name
                "Search in file(s): "
                :default default-directory
                :marked-candidates t :must-match t))))
    (call-interactively 'helm-rg))
  )

(defun helm-rg-.emacs.d-core ()
  (interactive)
  (let ((default-directory "~/.emacs.d/core"))
    (call-interactively 'helm-rg))
  )

(defun my-treemacs-follow-file ()
  (interactive)
  (let ((tm-wind (treemacs-get-local-window)))
    (unless tm-wind (treemacs))
    (treemacs--follow)
    (with-selected-window tm-wind
      ;; (recenter)
      (treemacs--maybe-recenter 'always)
      )
    )
  )



(general-define-key
 :keymaps '(normal visual emacs motion esc-map)
 :prefix "SPC"
 :non-normal-prefix "C-SPC"
 "" nil

 "SPC" 'helm-M-x

 ;; using ag for search
 ;; "/p" 'helm-do-ag-project-root ;; use project dir
 ;; "/." 'helm-do-ag-default-directory ;; use default dir
 ;; "/d" 'helm-do-ag ;; select the dir
 ;; "/c" 'helm-ag-.emacs.d-core ;; use ~/.emacs.d/core dir

 ;; using rg for search
 "/p" 'helm-projectile-rg ;; use project dir
 "/." 'helm-rg ;; use default dir
 "/d" 'helm-do-rg-select-directory ;; select the dir
 "/c" 'helm-rg-.emacs.d-core ;; use ~/.emacs.d/core dir

 "c" '(:ignore t :which-key "comment")
 "cl" 'comment-line
 "cr" 'comment-region

 "f" '(:ignore t :which-key "file")
 ;; "ff" 'helm-find-files
 "ff" 'my-treemacs-follow-file
 "fr" 'helm-recentf
 "fl" 'helm-locate
 "fs" 'save-buffer
 "fei" 'edit-init-file
 "fez" 'edit-zsh-dotfile
 "feR" 'reload-init-file

 "TAB" 'switch-to-prev-buffer
 "SPC" 'helm-M-x

 "q" '(:ignore t :which-key "quit")
 "qs"  'save-buffers-kill-terminal

 ;; "P" '(go-playground)

 ;; Git
 "g" '(:ignore t :which-key "git")
 "gs" 'magit-status
 "gg"  'hydra-git-gutter/body

 ;; Buffers
 "b" '(:ignore t :which-key "buffer")
 "bb"  'helm-buffers-list
 "bm"  'switch-to-messages
 "bs"  'switch-to-scratch
 "bdb" 'switch-to-dashboard
 "bc" 'switch-to-copy-log
 "bk"  'kill-this-buffer

 ;; Org-mode
 "o" '(:ignore t :which-key "org")
 "orr" 'helm-org-rifle
 "orc" 'helm-org-rifle-current-buffer
 "ord" 'helm-org-rifle-directories
 "orf" 'helm-org-rifle-files

 ;; Regex
 "r" '(:ignore t :which-key "regex")
 "rt" 'regex-tool
 "rh" 'helm-regexp

 ;; etc.
 "u"   'hydra-undo-tree/body
 "n"   'hydra-nav/body
 )



;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; PLATFORM ADAPTATION
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; NOTE: when running a MacOS modified ver of Emacs
;; e.g. https://github.com/railwaycat/homebrew-emacsmacport
(when *is-a-mac*
  (progn
    (setq mac-option-modifier 'meta
          mac-command-modifier 'super
          mac-right-option-modifier 'none)
    (global-set-key (kbd "s-c") 'kill-ring-save)
    (global-set-key (kbd "s-v") 'yank)
    (global-set-key (kbd "s-x") 'kill-region)
    (global-set-key (kbd "s-a") 'mark-whole-buffer)
    (global-set-key (kbd "s-z") 'undo)
    (global-set-key (kbd "s-f") 'isearch-forward)
    (global-set-key (kbd "s-g") 'isearch-repeat-forward)
    (global-set-key (kbd "s-o") 'find-file)
    (global-set-key (kbd "s-n") 'find-file)
    (global-set-key (kbd "s-s") 'save-buffer)
    (global-set-key (kbd "s-m") 'iconify-frame)
    (global-set-key (kbd "s-q") 'save-buffers-kill-emacs)
    (global-set-key (kbd "s-.") 'keyboard-quit)
    (global-set-key (kbd "s-l") 'goto-line)
    (global-set-key (kbd "s-k") 'kill-buffer)
    (global-set-key (kbd "s-<up>")    'beginning-of-buffer)
    (global-set-key (kbd "s-<down>")  'end-of-buffer)
    (global-set-key (kbd "s-<left>")  'beginning-of-line)
    (global-set-key (kbd "s-<right>") 'end-of-line)
    (global-set-key [(meta down)]     'forward-paragraph)
    (global-set-key [(meta up)]       'backward-paragraph)))



(provide 'ergo-keys)
