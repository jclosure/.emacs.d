;;; Commentary:
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;      desktops/perspectives settings
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;; -*- lexical-binding: t; -*-
;;; Code:

(message "******************* LOADING FINAL **********************")

(require 'bookmarking)
(require 'ergo-keys)

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; DASHBOARD
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; (use-package dashboard                  ;
;;   :bind (
;;          ;; NOTE: addresses random chars when clicking in dashboard
;;          ;;; https://github.com/emacs-dashboard/emacs-dashboard/issues/45
;; 	       :map dashboard-mode-map
;; 	       ("<down-mouse-1>" . nil)
;; 	       ("<mouse-1>" . widget-button-click)
;; 	       ("<mouse-2>" . widget-button-click))
;;   :demand t
;;   :init
;;   ;; content is not centered by default. To center, set
;;   (setq dashboard-center-content t)

;;   ;; to disable shortcut "jump" indicators for each section, set
;;   (setq dashboard-show-shortcuts nil)

;;   ;; (setq show-week-agenda-p t)

;;   (setq dashboard-items '((agenda . 5)
;;                           (recents  . 10)
;;                           (projects . 10)
;;                           (bookmarks . 4)
;;                           (registers . 2)
;;                           ))

;;   (setq dashboard-set-navigator t)
;;   (setq dashboard-set-heading-icons t)
;;   (setq dashboard-set-file-icons t)
;;   (setq dashboard-startup-banner (expand-file-name "core/black-hole.png" user-emacs-directory))
;;   :config
;;   (dashboard-setup-startup-hook))





(provide 'final)
