;;; PACKAGE --- Summary
;;; Code:
;;; Commentary:

;; TODO: these are required here for contents, but try to eliminate
;; (require 'company)

;; 1. detect system type
;; 2. run setup.sh for correct system type
;; 3. copy .zshrc into place
;; 4.

;;; USAGE
;; TODO: .... load buffer w/ key ....

;;; DETECT ENVIRONMENT

;;; PLATFORM INSTALLATION



;;; EMACS SERVER
;; ;; enable server by default
;; (load "server")
;; (unless (server-running-p)
;;   (add-hook 'after-init-hook 'server-start t))


;; (use-package server
;;   :config (or (server-running-p) (server-mode)))



;;; REGULAR PACKAGES



;; on the fly syntax checking
(use-package flycheck
  :diminish flycheck-mode
  ;; :config (global-flycheck-mode t)
  )

;; snippets and snippet expansion
;; NOTE: creating custom snippets: https://simpleit.rocks/lisp/emacs/adding-custom-snippets-to-yasnippet/
;; (setq yas-snippet-dirs
;;       '("~/.emacs.d/snippets"                    ;; personal snippets
;;         ;; "/path/to/some/collection/"           ;; foo-mode and bar-mode snippet collection
;;         ;; "/path/to/yasnippet/yasmate/snippets" ;; the yasmate collection
;;         ))


;; (use-package helm-c-yasnippet
;;   :after (helm yasnippet)
;;   :bind
;;   (("C-c y" . helm-yas-complete)))

;; NOTE: read this: https://jaketrent.com/post/code-snippets-spacemacs/
;; (use-package yasnippet
;;   :after (no-littering)
;;   :init
;;   (add-hook 'prog-mode-hook #'yas-minor-mode)
;;   :config
;;   (define-key yas-minor-mode-map "\C-j" 'yas-expand)
;;   (define-key yas-keymap "\C-j" 'yas-next-field-or-maybe-expand)
;;   (dolist (keymap (list yas-minor-mode-map yas-keymap))
;;     (define-key keymap (kbd "TAB") nil)
;;     (define-key keymap [(tab)] nil)))

;; (use-package yasnippet-snippets
;;   :after yasnippet)
































;;; PROGRAMMING SUPPORT




;; LANGUAGE CUSTOMIZATION



;; R
(use-package ess-site
  :disabled
  :commands R)


;; SYMBOLS
(use-package xah-math-input
  :init
  (add-hook 'text-mode-hook 'xah-math-input-mode)
  (add-hook 'org-mode-hook 'xah-math-input-mode))

;; LUA
(use-package lua-mode)

;; C/C++

;; additional cpp file extensions
(add-hook 'after-init-hook
	        (function (lambda()
		                  (add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))
		                  (add-to-list 'auto-mode-alist '("\\.inl\\'" . c++-mode))
		                  (add-to-list 'auto-mode-alist '("\\.inc\\'" . c++-mode))
		                  (add-to-list 'auto-mode-alist '("\\.cc\\'" . c++-mode))
		                  )))

;; TODO: make it so these don't run everytime a c file is loaded.  ordered with company, etc..
(add-hook 'c-mode-common-hook
          (lambda()
            (define-key c-mode-map  (kbd "TAB") 'my-tab-indent-or-company-complete)
            (define-key c++-mode-map  (kbd "TAB") 'my-tab-indent-or-company-complete)
            ))

(use-package cmake-mode)
;; (use-package flymake-cursor)
;; (use-package flymake-google-cpplint
;;   :init
;;   :config
;;   (add-hook 'c-mode-hook 'flymake-google-cpplint-load)
;;   (add-hook 'c++-mode-hook 'flymake-google-cpplint-load)
;;   (custom-set-variables
;;    ;; MacOS
;;    ;; '(flymake-google-cpplint-command "/System/Library/Frameworks/Python.framework/Versions/2.7/bin/cpplint")
;;    ;; Linux
;;    '(flymake-google-cpplint-command "/usr/local/bin/cpplint")
;;    '(flymake-google-cpplint-verbose "--verbose=0")
;;    ;; Note that I've turned off build rules. May want those on, e.g. +build/include
;;    '(flymake-google-cpplint-filter "--filter=-whitespace/line_length,-build")))

;; ;; ;; FIXME: straight-fix
;; (use-package google-c-style
;;   :init
;;   (add-hook 'c-mode-common-hook 'google-set-c-style)
;;   (add-hook 'c-mode-common-hook 'google-make-newline-indent))






;; C#
;; NOTE: requires: M-x
;; omnisharp-install-server
;; omnisharp-start-omnisharp-server
(use-package omnisharp
  :after csharp-mode
  :init
  (defun my-csharp-mode-hook ()
    "Hook for `emacs-lisp-mode'"
    (set (make-local-variable 'company-backends)
         '((company-capf company-dabbrev-code company-omnisharp company-yasnippet company-files)))
    (local-set-key (kbd "C-c C-c") #'recompile))
  :bind (:map omnisharp-mode-map
              ("C-c r" . omnisharp-run-code-action-refactoring)
              ("M-." . omnisharp-go-to-definition)
              ;; ("M-." . omnisharp-find-implementations)
              ("M-?" . omnisharp-find-usages))
  :hook ((csharp-mode . omnisharp-mode)))





;; (use-package gradle-mode)






;;; INFRASTRUCTURE







;; TODO: document this at top
;; contract with C-- C-= - - -
;; ^=    ^[[61;5u
;; (use-package expand-region
;;   :bind (("C-=" . er/expand-region))
;;   :config
;;   (progn
;;     ;; (defadvice er/expand-region (around fill-out-region activate)
;;     ;;   (if (or (not (region-active-p))
;;     ;;           (eq last-command 'er/expand-region))
;;     ;;       ad-do-it
;;     ;;     (if (< (point) (mark))
;;     ;;         (let ((beg (point)))
;;     ;;           (goto-char (mark))
;;     ;;           (end-of-line)
;;     ;;           (forward-char 1)
;;     ;;           (push-mark)
;;     ;;           (goto-char beg)
;;     ;;           (beginning-of-line))
;;     ;;       (let ((end (point)))
;;     ;;         (goto-char (mark))
;;     ;;         (beginning-of-line)
;;     ;;         (push-mark)
;;     ;;         (goto-char end)
;;     ;;         (end-of-line)
;;     ;;         (forward-char 1)))))

;;     ;; (defhydra hydra-expand (:hint nil :columns 1 :body-pre (er/expand-region 1))
;;     ;;   ("c" er/contract-region "Contract")
;;     ;;   ("e" er/expand-region "Expand"))
;;     )
;;   )


;; TODO: move this section above language modes and remove demand




;; ;; clean auto-indent and backspace unindent
;; ;; https://github.com/pmarinov/clean-aindent-mode
;; (use-package clean-aindent-mode
;;   :config
;;   (add-hook 'prog-mode-hook 'clean-aindent-mode))





;; minimap
;; (use-package sublimity
;;   :config
;;   (sublimity-mode 1))








;; (use-package paradox
;;   :after async
;;   :commands paradox-list-packages
;;   :config
;;   (setq paradox-automatically-star t
;;         paradox-execute-asynchronously t))















;; EXPERIMENTAL



;; (use-package thingatpt+
;;   :quelpa (thingatpt+ :fetcher wiki))

;; (progn
;;   (require 'mouse)
;;   (xterm-mouse-mode t)
;;   (defun track-mouse (e))
;;   (setq mouse-sel-mode t))



;; (use-package yascroll
;;
;;   :config
;;   (global-yascroll-bar-mode 1))



;; ;; not used for ff because helm owning minibuffer
;; (use-package zlc
;;
;;   :config
;;   (let ((map minibuffer-local-map))
;;   ;;; like menu select
;;     (define-key map (kbd "<down>")  'zlc-select-next-vertical)
;;     (define-key map (kbd "<up>")    'zlc-select-previous-vertical)
;;     (define-key map (kbd "<right>") 'zlc-select-next)
;;     (define-key map (kbd "<left>")  'zlc-select-previous)
;;   ;;; reset selection
;;     (define-key map (kbd "C-c") 'zlc-reset)))


;; TODO: TRY THESE ORG CONFIGS
;; https://git.sr.ht/~nsh/emacs.d/tree/ad7780b33993b59afa901c00d447d00bb3457055/init.el#L342
;; TODO: do full org config
;; indent src blocks according to lang type
;; (use-package org
;;   :ensure org-plus-contrib
;;   :pin org
;;   :config
;;   (setq org-src-tab-acts-natively t)
;;   (require 'org-tempo)
;;   ;; (setq company-global-modes '(not org-mode))
;;   )



;; ;; Pin your eyes to the centre of the screen (except when you don't)
;; (use-package centered-cursor-mode
;;   :diminish centered-cursor-mode
;;   :init
;;   (setq ccm-recenter-at-end-of-file t)
;;   :config
;;   (define-global-minor-mode my-global-centered-cursor-mode centered-cursor-mode
;;     (lambda ()
;;       (when (not (memq major-mode (list 'compilation-mode
;;                                         'dired-mode
;;                                         'ein:notebook-multilang-mode
;;                                         'eshell-mode
;;                                         'eww-mode
;;                                         'inferior-python-mode
;;                                         'inferior-sml-mode
;;                                         'magit-mode
;;                                         'messages-buffer-mode
;;                                         'org-mode
;;                                         'sql-interactive-mode
;;                                         'w3m-mode)))
;;         (centered-cursor-mode))))
;;   (my-global-centered-cursor-mode 1))




;; (use-package indent-shift
;;   :bind (("C-c <" . indent-shift-left)
;;          ("C-c >" . indent-shift-right)))



;; (use-package sqlplus
;;   ;; :quelpa (sqlplus :fetcher wiki)
;;   :quelpa (sqlplus :fetcher github :repo "emacsmirror/sqlplus"))


;; ;; lojban https://dustinlacewell.github.io/sutysisku.el/
;; (use-package sutysisku
;;
;;   :quelpa (sutysisku :fetcher github :repo "dustinlacewell/sutysisku.el"))


;; Zoom window like tmux
(use-package zoom-window

  :init
  (global-set-key [S-return] 'zoom-window-zoom))




;; HOW TO USE use-package to load a package from local dir (not already in load-path)
;; (use-package some-feature-package
;;   :load-path some-other-dir)




;;  NOTE: be sure this isn't causing problems before adopting
;; (use-package aggressive-indent
;;   :if enable-experimental-packages
;;   :config
;;   (global-aggressive-indent-mode 1)
;;   (add-to-list 'aggressive-indent-excluded-modes 'html-mode)
;;   (add-to-list 'aggressive-indent-excluded-modes 'slime-repl-mode)
;;   (add-to-list 'aggressive-indent-excluded-modes 'haskell-mode)
;;   (add-to-list 'aggressive-indent-excluded-modes 'lisp-mode)
;;   (add-to-list 'aggressive-indent-excluded-modes 'emacs-lisp-mode)
;;   (delete 'lisp-mode aggressive-indent-modes-to-prefer-defun)
;;   (delete 'emacs-lisp-mode aggressive-indent-modes-to-prefer-defun)
;;   (add-to-list 'aggressive-indent-dont-indent-if
;;                '(not (null (string-match (rx (zero-or-more space) (syntax comment-start) (zero-or-more anything)) (thing-at-point 'line))))))


;; (use-package scroll-restore


;;   :if enable-experimental-packages
;;   :config
;;   (defun enable-scroll-restore ()
;;     (interactive)
;;     (require 'scroll-restore)
;;     (scroll-restore-mode 1)
;;     ;; Allow scroll-restore to modify the cursor face
;;     (setq scroll-restore-handle-cursor t)
;;     ;; Make the cursor invisible while POINT is off-screen
;;     (setq scroll-restore-cursor-type nil)
;;     ;; Jump back to the original cursor position after scrolling
;;     (setq scroll-restore-jump-back t)
;;     ;; Toggle scroll-restore-mode with the Scroll Lock key
;;     ;; (global-set-key (kbd "<Scroll_Lock>") 'scroll-restore-mode)
;;     (scroll-restore-mode)))


;;; #############################################################
;;; Dash docsets requires gnutls support in emacs
;;; #############################################################
;; (use-package counsel-dash
;;   :init
;;   (setq counsel-dash-docsets-path "~/.emacs.d/docsets"
;;         counsel-dash-docsets-url "https://api.github.com/repos/Kapeli/feeds/contents"
;;         counsel-dash-browser-func 'browse-url)
;;   :config
;;   (counsel-dash-install-docset "Emacs Lisp"))
;;
;; ;; get docsets: https://kapeli.com/docsets#dashdocsetfeed
;; (use-package helm-dash
;;   :config
;;   (progn
;;     (setq helm-dash-browser-func 'eww)
;;     (setq helm-dash-docsets-path (expand-file-name "~/.emacs.d/docsets")
;;     )
;;     ;;(helm-dash-activate-docset "Go")
;;     ;; (helm-dash-activate-docset "Python 3")
;;     ;; (helm-dash-activate-docset "CMake")
;;     ;; (helm-dash-activate-docset "Bash")
;;     ;; (helm-dash-activate-docset "Zsh")
;;     ;; (helm-dash-activate-docset "Django")
;;     ;; (helm-dash-activate-docset "Redis")
;;     ;; (helm-dash-activate-docset "Emacs Lisp")
;;     ))
;;
;; (use-package dash-at-point
;;   :ensure t)
;;
;;; #############################################################
;;; End Dash docsets
;;; #############################################################


;; (use-package helm-dash
;;   :init
;;   (global-set-key (kbd "C-c d") 'helm-dash-at-point)
;;   (defun c-doc ()
;;     (setq helm-dash-docsets '("C")))
;;   (defun c++-doc ()
;;     (setq helm-dash-docsets '("C" "C++")))
;;   (add-hook 'c-mode-hook 'c-doc)
;;   (add-hook 'c++-mode-hook 'c++-doc))


(progn
  "
ref: https://joppot.info/en/2018/02/04/4144
There's this config and a full walkthrough on using vc-git/vc-dir
"

  (require 'vc-dir)
  ;; In vc-git and vc-dir for git buffers,
  ;; make (C-x v)
  ;; - a run git add,
  ;; - u run git reset,
  ;; - r run git reset and checkout from head

  (defun my-vc-git-command (verb fn)
    (let* ((fileset-arg (or vc-fileset (vc-deduce-fileset nil t)))
           (backend (car fileset-arg))
           (files (nth 1 fileset-arg)))
      (if (eq backend 'Git)
          (progn (funcall fn files)
                 (message (concat verb " " (number-to-string (length files))
                                  " file(s).")))
        (message "Not in a vc git buffer."))))

  (defun my-vc-git-add (&optional revision vc-fileset comment)
    (interactive "P")
    (my-vc-git-command "Staged" 'vc-git-register))

  (defun my-vc-git-reset (&optional revision vc-fileset comment)
    (interactive "P")
    (my-vc-git-command "Unstaged"
                       (lambda (files) (vc-git-command nil 0 files "reset" "-q" "–"))))

  (eval-after-load "vc-dir"
    '(progn
       (define-key vc-prefix-map [(r)] 'vc-revert-buffer)
       (define-key vc-dir-mode-map [(r)] 'vc-revert-buffer)
       (define-key vc-prefix-map [(a)] 'my-vc-git-add)
       (define-key vc-dir-mode-map [(a)] 'my-vc-git-add)
       (define-key vc-prefix-map [(u)] 'my-vc-git-reset)
       (define-key vc-dir-mode-map [(u)] 'my-vc-git-reset)

       ;; hide up to date files after refreshing in vc-dir
       (define-key vc-dir-mode-map [(g)]
         (lambda () (interactive) (vc-dir-refresh) (vc-dir-hide-up-to-date)))
       )))




;; TODO: use lsp and clang setup from:
;; https://emacs.nasy.moe/
;; Language Server Protocol & Debug Adapter Protocol
;; lsp-mode


;;; ; http://lists.gnu.org/archive/html/help-gnu-emacs/2007-05/msg00975.html

;; (defvar sticky-buffer-previous-header-line-format)
;; (define-minor-mode sticky-buffer-mode
;;   "Make the current window always display this buffer."
;;   nil " sticky" nil
;;   (if sticky-buffer-mode
;;       (progn
;;         (set (make-local-variable 'sticky-buffer-previous-header-line-format)
;;              header-line-format)
;;         (set-window-dedicated-p (selected-window) sticky-buffer-mode))
;;     (set-window-dedicated-p (selected-window) sticky-buffer-mode)
;;     (setq header-line-format sticky-buffer-previous-header-line-format)))


;; (use-package helm-systemd
;;   :commands helm-systemd)


;; NOTE: show last 2 marks
;; (defface visible-mark-active ;; put this before (use-package visible-mark)
;;   '((((type tty) (class mono)))
;;     (t (:background "magenta"))) "")

;; (use-package visible-mark
;;   :init (global-visible-mark-mode 1)
;;   :config
;;   (progn
;;     (setq visible-mark-max 2)
;;     (setq visible-mark-faces `(visible-mark-face1 visible-mark-face2))))





;; (use-package auto-mark
;;
;;   :ensure nil
;;   :config
;;   (progn
;;     (setq auto-mark-command-class-alist
;;           '((anything . anything)
;;             (goto-line . jump)
;;             (undo . ignore)))
;;     (setq auto-mark-command-classifiers
;;           (list (lambda (command)
;;                   (if (and (eq command 'self-insert-command)
;;                            (eq last-command-char ? ))
;;                       'ignore))))
;;     (global-auto-mark-mode 1)
;;     ))


;; indentation all 2 - this may be problematic
(setq-default
 indent-tabs-mode nil
 tab-width 2
 sh-basic-offset 2
 sh-indentation 2
 c-basic-offset 2)



;; ;; solaire-mode is an aesthetic plugin that helps visually distinguish file-visiting windows
;; ;; from other types of windows (like popups or sidebars) by giving them a slightly different
;; ;; – often brighter – background.
;; (use-package solaire-mode
;;   :ensure t
;;
;;   :config
;;   (setq solaire-mode-remap-modeline nil)

;;   ;; brighten buffers (that represent real files)
;;   (add-hook 'after-change-major-mode-hook #'turn-on-solaire-mode)
;;   ;; To enable solaire-mode unconditionally for certain modes:
;;   (add-hook 'ediff-prepare-buffer-hook #'solaire-mode)

;;   ;; ...if you use auto-revert-mode, this prevents solaire-mode from turning
;;   ;; itself off every time Emacs reverts the file
;;   (add-hook 'after-revert-hook #'turn-on-solaire-mode)

;;   ;; highlight the minibuffer when it is activated:
;;   (add-hook 'minibuffer-setup-hook #'solaire-mode-in-minibuffer))





;; ;; easy-kill (similar to expand-region)
;; ;; https://emacsredux.com/blog/2018/11/09/an-easy-kill/
;; (use-package easy-kill
;;   :ensure t
;;
;;   :config
;;   (global-set-key [remap kill-ring-save] #'easy-kill)
;;   (global-set-key [remap mark-sexp] #'easy-mark))


;; ;; http://blog.lujun9972.win/emacs-document/blog/2019/03/14/evil-guide/
;; (use-package evil
;;
;;   :init
;;   (setq evil-want-integration t) ;; This is optional since it's already set to t by default.
;;   (setq evil-want-keybinding nil)
;;   :config
;;   (progn
;;    (define-key evil-emacs-state-map [escape] 'evil-normal-state)
;;    (evil-mode 1)))

;; (use-package evil-collection
;;   :after evil
;;   :ensure t
;;   :config
;;   (evil-collection-init))

;; (use-package general
;;   :ensure t
;;   :init
;;   (setq general-override-states '(insert
;;                                   emacs
;;                                   hybrid
;;                                   normal
;;                                   visual
;;                                   motion
;;                                   operator
;;                                   replace))
;;   :config
;;   (general-define-key
;;    :states '(normal visual motion)
;;    :keymaps 'override
;;    "SPC" 'hydra-space/body))
;;    ;; Replace 'hydra-space/body with your leader function.




;; ;; https://github.com/DamienCassou/auth-password-store
;; ;; research
;; (use-package auth-source-pass
;;   :ensure nil
;;   :after auth-source
;;   :init
;;   (progn
;;     (setq auth-sources '(password-store))))


;; ;; bash-completion
;; ;; sudo apt-get install bash-completion
;; (use-package bash-completion
;;   :hook (shell-dynamic-complete-functions . bash-completion-dynamic-complete))


;; ;; TODO: get this working
;; ;; brew install transmission
;; ;; sudo apt-get install transmission
;; (use-package transmission
;;   :config (progn
;;             (defun transmission-add-magnet (url)
;;               "Like `transmission-add', but with no file completion."
;;               (interactive "sMagnet url: ")
;;               (transmission-add url))))

;; abbrevs and dabbrevs




;; (use-package company-dabbrev
;;   :config (progn
;;             (setq dabbrev-check-all-buffers t)
;;             (setq company-dabbrev-ignore-case t)
;;             (setq company-dabbrev-downcase nil)))













;; FACE CUSTOMIZATION

;; style the window divider

;; solid gray looks good but fat (theme overrides)
(set-face-background 'vertical-border "gray20")
(set-face-foreground 'vertical-border (face-background 'vertical-border))


;; set base highlight style, so that hightlight and all inherited faces look good
;; (set-face-foreground 'highlight nil) ;; remove forground so syntax highlighting still works
;; (set-face-background 'highlight "#1c1c1c")

;; ;; region selected color (black)
;; (set-face-attribute 'region nil :background "color-236")

;; comment keyword highlighting
;; TODO: possibly replace with comment-tags-mode in the future. currently doesn't support regex.
(defface my-comment-keywords-face
  '((t :foreground "magenta"
       :weight bold))
  "Face for specific keywords that appear in comments."
  :group 'my-faces)

;; (defun my-highlight-comment-keywords ()
;;   "minimal face change for comment keywords."
;;   (interactive)
;;   (font-lock-add-keywords nil '(("\\b\\(FIXME[\\(\\)a-zA-Z0-9]*:\\|TODO[\\(\\)a-zA-Z0-9]*:\\|BUG[\\(\\)a-zA-Z0-9]*:\\|NOTE[\\(\\)a-zA-Z0-9]*:\\)" 1 'my-comment-keywords-face t))))


;; ;; add hooks where comment keywords should be highlighted
;; (mapc (lambda (hook) (add-hook hook 'my-highlight-comment-keywords))
;;       '(
;; 	      text-mode-hook
;; 	      latex-mode-hook
;; 	      html-mode-hook
;; 	      emacs-lisp-mode-hook
;; 	      prog-mode-hook
;; 	      python-mode-hook
;; 	      ))



;; set the face for volatile highlights
;; timing issues with gui mode setting face, so using an idle timer to set it
(progn
  ;; variable for the timer object
  (defvar idle-timer-vhl-timer nil)

  ;; callback function
  (defun idle-timer-vhl-callback ()
    (progn
      ;; we can always all make face before setting in incase it's not there yet
      (make-face 'vhl/default-face)
      (set-face-attribute 'vhl/default-face nil
                          :background "blue4")))

  ;; start functions
  (defun idle-timer-vhl-run-once ()
    (interactive)
    (when (timerp idle-timer-vhl-timer)
      (cancel-timer idle-timer-vhl-timer))
    (setq idle-timer-vhl-timer
          (run-with-idle-timer 1 nil #'idle-timer-vhl-callback)))

  (idle-timer-vhl-run-once))



;; hadcoded colors for terminal emacs.
;; TODO: hoist these up into the use-packages to which they apply
(when *is-in-terminal*
  (progn

    (use-package terminal-focus-reporting
      :hook (after-init . terminal-focus-reporting-mode))

    ;; to see all current faces colors, run
    ;; list-faces-display


    ;; ;; set inactive windows' background(s) to a slightly lighter color to visually distingush
    ;; (defun highlight-selected-window ()
    ;;   "Highlight selected window with a different background color."
    ;;   (walk-windows (lambda (w)
    ;;                   (unless (eq w (selected-window))
    ;;                     (with-current-buffer (window-buffer w)
    ;;                       (buffer-face-set '(:background "#111"))))))
    ;;   (buffer-face-set 'default))
    ;; (add-hook 'buffer-list-update-hook 'highlight-selected-window)



    ;; TODO: move into csf directive in pkg-appearance
    ;; tweak these to be more inline with doom theme
    ;; set the default comment color to light grey
    (custom-set-faces
     ;; custom-set-faces was added by Custom.
     ;; If you edit it by hand, you could mess it up, so be careful.
     ;; Your init file should contain only one such instance.
     ;; If there is more than one, they won't work right.
     '(font-lock-variable-name-face ((t (:foreground "violet")))))


    ;; ;; TODO: do we still want this?
    ;; ;; set highlight face for dark screens
    ;; (set-face-background 'highlight "darkblue")
    ;; (set-face-background 'secondary-selection "darkblue")

    ;;; ivy
    (with-eval-after-load 'ivy
      (progn
        ;; the background color of selection line in ivy
        (set-face-attribute 'ivy-current-match nil
                            :background "blue")))

    ;;; helm

    (with-eval-after-load 'helm
      (progn

        (set-face-attribute 'helm-source-header nil
                            :background "color-17" :foreground "black" :weight 'bold :height 1.3 :family "Sans Serif" )

        ;; the background color of selection line in helm
        (set-face-attribute 'helm-selection nil
                            :background "blue")
        ))


    ;; ;;; show-paren-mode
    ;; ;; NOTE: letting theme own this for now
    ;; (if (face-exists-p 'show-paren-match)
    ;;     (set-face-attribute 'show-paren-match t
    ;;                         :background "light sky blue"))


    ;; ;;; neotree
    ;; ;; NOTE: the 'neo-vc-default-face is not visible on black backgrounds
    ;; ;; setting it explicitly here or must set:
    ;; ;; (setq frame-background-mode 'light) before neo loads
    ;; ;; otherwise, you can override like this
    ;; (with-eval-after-load 'neotree
    ;;   (progn
    ;;     (setq default-neo-tree-file-color "color-244")
    ;;     (set-face-attribute 'neo-vc-default-face t
    ;;                         :foreground default-neo-tree-file-color)
    ;;     (set-face-attribute 'neo-file-link-face t
    ;;                         :foreground default-neo-tree-file-color)))
    ))



(message "Initialized dotfiles")

(provide 'old-init-dotfiles)

;;; old-init-dotfiles.el ends here
