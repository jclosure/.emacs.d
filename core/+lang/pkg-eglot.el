;; nec

(use-package eglot
  :ensure t
  :demand t
  ;; :quelpa (eglot :fetcher github :repo "joaotavora/eglot")
  ;; :straight t
  :bind (:map eglot-mode-map
	            ("C-c C-d" . eglot-help-at-point)
	            ("C-c C-r" . eglot-code-actions))
  ;; :init
  ;; (add-hook 'sql-mode-hook 'eglot-ensure)
  :config
  (progn
    (setq eglot-sync-connect 1)

    (add-to-list 'display-buffer-alist
                 '("\\*sqls\\*"
                   (display-buffer-reuse-window display-buffer-at-bottom)
                   (reusable-frames . visible)
                   (window-height . 0.3)))

    (defclass eglot-sqls (eglot-lsp-server) () :documentation "SQL's Language Server")
    (add-to-list 'eglot-server-programs '(sql-mode . (eglot-sqls "sqls")))

    (cl-defmethod eglot-execute-command
      ((server eglot-sqls) (command (eql executeQuery)) arguments)
      "For executeQuery."
      ;; (ignore-errors
      (let* ((beg (eglot--pos-to-lsp-position (if (use-region-p) (region-beginning) (point-min))))
             (end (eglot--pos-to-lsp-position (if (use-region-p) (region-end) (point-max))))
             (res (jsonrpc-request server :workspace/executeCommand
                                   `(:command ,(format "%s" command) :arguments ,arguments
                                              :timeout 0.5 :range (:start ,beg :end ,end))))
             (buffer (generate-new-buffer "*sqls*")))
        (with-current-buffer buffer
          (eglot--apply-text-edits `[
                                     (:range
                                      (:start
                                       (:line 0 :character 0)
                                       :end
                                       (:line 0 :character 0))
                                      :newText ,res)
                                     ]
                                   )
          (org-mode))
        (pop-to-buffer buffer))
      )

    (cl-defmethod eglot-execute-command
      ((server eglot-sqls) (_cmd (eql switchDatabase)) arguments)
      "For switchDatabase."
      (let* ((res (jsonrpc-request server :workspace/executeCommand
                                   `(:command "showDatabases" :arguments ,arguments :timeout 0.5)))
             (menu-items (split-string res "\n"))
             (menu `("Eglot code actions:" ("dummy" ,@menu-items)))
             (db (if (listp last-nonmenu-event)
                     (x-popup-menu last-nonmenu-event menu)
                   (completing-read "[eglot] Pick an database: "
                                    menu-items nil t
                                    nil nil (car menu-items))
                   ))
             )
        (jsonrpc-request server :workspace/executeCommand
                         `(:command "switchDatabase" :arguments [,db] :timeout 0.5))
        ))
    ))

(require 'eglot)

(defun project-root (project)
  (car (project-roots project)))

;; (use-package eglot
;;   :straight t
;;   :bind (:map eglot-mode-map
;; 	            ("C-c C-d" . eglot-help-at-point)
;; 	            ("C-c C-r" . eglot-code-actions))
;;   :config
;;   (add-to-list 'eglot-server-programs '(dart-mode . ("danal")))
;;   (setq eglot-sync-connect 1)
;;   (setq eglot-ignored-server-capabilites '(:documentHighlightProvider))
;;   (add-to-list 'eglot-server-programs '(go-mode . ("gopls")))
;;   :hook
;;   ((c-mode . eglot-ensure)
;;    ;; (dart-mode . eglot-ensure)
;;    (go-mode . eglot-ensure)
;;    (kotlin-mode . eglot-ensure)))



;; (use-package eglot :ensure t)
;; (add-to-list 'eglot-server-programs '((c++-mode c-mode) "clangd"))
;; (add-hook 'c-mode-hook 'eglot-ensure)
;; (add-hook 'c++-mode-hook 'eglot-ensure)
;; (add-hook 'python-mode-hook 'eglot-ensure)

;; (defconst my-eclipse-jdt-home "/home/zamansky/.emacs.d/.cache/lsp/eclipse.jdt.ls/plugins/org.eclipse.equinox.launcher_1.5.800.v20200727-1323.jar")
;; (defun my-eglot-eclipse-jdt-contact (interactive)
;;   "Contact with the jdt server input INTERACTIVE."
;;   (let ((cp (getenv "CLASSPATH")))
;;     (setenv "CLASSPATH" (concat cp ":" my-eclipse-jdt-home))
;;     (unwind-protect (eglot--eclipse-jdt-contact nil)
;;       (setenv "CLASSPATH" cp))))
;; (setcdr (assq 'java-mode eglot-server-programs) #'my-eglot-eclipse-jdt-contact)
;; (add-hook 'java-mode-hook 'eglot-ensure)

(provide 'pkg-eglot)
