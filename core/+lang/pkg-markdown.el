;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	Markdown mode setting
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))


;; ;; markdown preview tool
;; ;; TODO: add a hook for livedown-preview for markdown-mode
;; (use-package emacs-livedown
;;   :ensure nil
;;   :after (markdown-mode)

;;   :quelpa (livedown :fetcher github :repo "shime/emacs-livedown")
;;   :init
;;   (setq
;;    livedown-autostart nil ; automatically open preview when opening markdown files
;;    livedown-open t        ; automatically open the browser window
;;    livedown-port 1337     ; port for livedown server
;;    livedown-browser nil)  ; browser to use
;;   :config
;;   (progn
;;     (define-key markdown-mode-map (kbd "M-p") 'livedown-preview)
;;     (define-key markdown-mode-map (kbd "M-k") 'livedown-kill)
;;   ))

(use-package markdown-toc)


(provide 'pkg-markdown)
