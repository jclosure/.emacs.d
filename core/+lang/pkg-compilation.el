
;; TODO: move the rest of compile-related and make stuff in here


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; TERMINAL COLORS
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; ref: https://github.com/atomontage/xterm-color
;; colorize compilation buffers
;; IMPORTANT: add modes to this variable if you experience weird ansi codes in *compilation* buffer
(setq compilation-buffer-colorization-exclude-modes '("Grep" "rg" "ag" "helm-ag"))
(use-package xterm-color
  :config
  (progn
    (setq compilation-environment '("TERM=xterm-256color"))

    (defun my/advice-compilation-filter (f proc string)
      (unless (member mode-name compilation-buffer-colorization-exclude-modes)
        (funcall f proc (xterm-color-filter string))))

    (advice-add 'compilation-filter :around #'my/advice-compilation-filter)))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; STICKY *COMPILATION* BUFFER/WINDOW CONTROL
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; buries *compilation* buffer if successful, else leave it in place
;; NOTE: causes undesirable behavior in some cases, where M-x compile is used
;; for running code, e.g. go-test
;; NOTE: possibly replaceable with better behavior by:
;; https://github.com/EricCrosson/bury-successful-compilation

;; TODO: change to set-default
(setq sticky-compilation-window t)

(defun my-when-compilation-succeeds (buff desc)
  (run-at-time "0.4 sec" nil
               (lambda (buff desc)
                 (select-window (get-buffer-window buff))
                 (unless sticky-compilation-window
                   (quit-window))
                 (message "No Compilation Errors."))
               buff
               desc)
  )

(defun my-when-compilation-fails (buff desc)
  (run-at-time "0.4 sec" nil
               (lambda (buff desc)
                 (select-window (get-buffer-window buff))
                 (switch-to-buffer buff)
                 (message "Found Compilation Errors!")
                 )
               buff
               desc)
  )

(setq compilation-finish-functions
      (lambda (buf desc)
        ;; (message "Buffer %s: %s" buf desc)
        (if (null (string-match ".*exited abnormally.*" desc))
            ;;no errors, make the compilation window go away in a few seconds
            (my-when-compilation-succeeds buf desc)
          ;; case where there was an error
          (my-when-compilation-fails buf desc))))

(provide 'pkg-compilation)
