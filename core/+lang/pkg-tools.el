;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;      Useful tools setup
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;; Code:

;; find out what current buffer depends on
(use-package elisp-depend
  :commands elisp-depend-print-dependencies)

;; vi-like tildes on unused file lines
(use-package vi-tilde-fringe
  :config
  (global-vi-tilde-fringe-mode))

;; flymake
(use-package flymake
  :init
  ;; Nope, I want my copies in the system temp dir.
  (setq flymake-run-in-place nil)
  ;; This lets me say where my temp dir is.
  ;; (setq temporary-file-directory "~/.emacs.d/tmp/")
  )
;; (use-package flymake-go)
(use-package flymake-easy)
(use-package flymake-ruby
  :config
  (add-hook 'ruby-mode-hook 'flymake-ruby-load))
(use-package flymake-yaml
  :config
  (add-hook 'yaml-mode-hook 'flymake-yaml-load))


;; YAML
(use-package yaml-mode
  :mode ("\\.yaml\\'" "\\.yml\\'")
  :custom-face
  (font-lock-variable-name-face ((t (:foreground "violet")))))

;;;; graphql-mode

(use-package graphql-mode
  :ensure t
  :mode (("\\.graphql\\'" . graphql-mode)))


;; CSV
(use-package csv-mode
  :mode "\\.csv\\'")

;; bison and flex file
(use-package bison-mode)

;; systemd format support
(use-package systemd)

;; PROTOBUF
(use-package protobuf-mode
  :mode "\\.proto$")

;; FLATBUFFERS
(use-package flatbuffers-mode)

;; XML
;; default indent to be 4
(setq nxml-child-indent 4 nxml-attribute-indent 4)

;;;; vlf
;; View Large Files
(use-package vlf
  :init
  (setq vlf-application 'dont-ask)   ;just do it
  (setq vlf-batch-size 8192))        ;a bit more text per batch please


(use-package which-key
  :diminish which-key-mode
  :hook (after-init . which-key-mode))

(use-package which-func
  :hook (prog-mode . which-function-mode)
  :init (setq which-func-non-auto-modes '(treemacs-mode)))

(provide 'pkg-tools)
