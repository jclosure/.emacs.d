
;; ELIXIR
(use-package elixir-mode
  :commands elixir-mode
  :mode
  (("\\.exs$" . elixir-mode)
   ("\\.ex$" . elixir-mode))
  :config
  (progn
    ;; TODO: reenable alchemist once elixir-lsp setup
    ;; currently disabled in order to use dumb-jump
    ;; (add-hook 'elixir-mode-hook 'alchemist-mode)
    ;; (add-hook 'elixir-mode-hook  'regtab-mode)

    ;; support functions stolen from spacemacs

    (defun spacemacs//elixir-enable-compilation-checking ()
      "Enable compile checking if `elixir-enable-compilation-checking' is non nil."
      (when (or elixir-enable-compilation-checking)
        (flycheck-mix-setup)
        ;; enable credo only if there are no compilation errors
        (flycheck-add-next-checker 'elixir-mix '(warning . elixir-credo))))

    (defun spacemacs/elixir-annotate-pry ()
      "Highlight breakpoint lines."
      (interactive)
      (highlight-lines-matching-regexp "require IEx; IEx.pry"))

    (defun spacemacs/elixir-toggle-breakpoint ()
      "Add a breakpoint line or clear it if line is already a breakpoint."
      (interactive)
      (let ((trace "require IEx; IEx.pry")
            (line (thing-at-point 'line)))
        (if (and line (string-match trace line))
            (kill-whole-line)
          (progn
            (back-to-indentation)
            (insert trace)
            (newline-and-indent)))))

    )

  ;; HACK: hardcoded forcing elixir to use dumb-jump
  (bind-keys :map elixir-mode-map
             ("M-." . 'dumb-jump-go))
  (bind-keys :map elixir-mode-map
             ("M-," . 'dumb-jump-back))

  )

;; (use-package alchemist
;;   :commands alchemist-mode
;;   :init
;;   (defun my-put-iex ()
;;     ;; Automatically add pry so debugging is easy
;;     (interactive)
;;     (insert "require IEx; IEx.pry"))
;;   :config
;;   ;; override iex key sequence with my version
;;   (bind-keys :map alchemist-mode-map
;;              ( "C-c a p i" . my-put-iex))

;;   ;; compiler/recompile this buffer, e.g. this module
;;   (bind-keys :map alchemist-mode-map
;;              ("C-c C-l" . (lambda () (interactive)
;;                             (save-buffer)
;;                             (alchemist-iex-compile-this-buffer))))

;;   ;; make elixir evalute a single line similar to elisp
;;   (bind-keys :map alchemist-mode-map
;;              ("C-x C-e" . alchemist-iex-send-current-line))

;;   ;; HACK: hardcoded  because alchemist doesn't work in monorepo
;;   ;; overriding jump keys for alchemist mode map to make jumping work in monorepos
;;   ;; using dumb-jump directly because smart-jump has alchemist jump registered inline
;;   ;; before dumb-jump
;;   (bind-keys :map alchemist-mode-map
;;              ("M-." . dumb-jump-go))
;;   (bind-keys :map alchemist-mode-map
;;              ("M-," . dumb-jump-back))
;;   )

;; (use-package elixir-yasnippets)

;; (use-package flymake-elixir
;;   :hook
;;   (elixir-mode-hook . flymake-elixir-load))

(provide 'pkg-elixir)
