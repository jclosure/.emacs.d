(use-package js2-mode
  :mode ("\\.js$" . js2-mode)
  :init
  (setq js2-strict-missing-semi-warning nil))

;; JSON
(use-package json-mode
  :mode "\\.json$")

;; show path to where point is in a json doc: C-c C-p
(use-package json-snatcher
  :commands (jsons-print-path))

;; Prettier for JS
(use-package prettier-js
  :ensure t)

(use-package xref-js2
  :config
  (add-hook 'js2-mode-hook
            (lambda ()
              (add-hook 'xref-backend-functions #'xref-js2-xref-backend nil t))))

;; REACT SUPPORT
(use-package rjsx-mode)

(use-package nodejs-repl
  :commands (nodejs-repl
             nodejs-repl-send-buffer
             nodejs-repl-switch-to-repl
             nodejs-repl-send-region
             nodejs-repl-send-last-expression
             nodejs-repl-execute
             nodejs-repl-load-file))

(use-package nvm
  :commands (nvm-use
             nvm-use-for))

(use-package add-node-modules-path
  :hook ((js2-mode . add-node-modules-path)
         (rjsx-mode . add-node-modules-path)
         (js-mode . add-node-modules-path)
         (json-mode . add-node-modules-path)))


;; ELASTICSEARCH
;; TODO: this is the author's example spools and keybindings
;; IMPORTANT: https://laptrinhx.com/an-emacs-major-mode-for-interacting-with-elasticsearch-3461331629/
(use-package es-mode
  :mode ("\\.es\\'" . es-mode)
  :bind (("C-c C-f" . json-pretty-print)))

(use-package ob-elasticsearch
  :ensure nil
  :straight nil
  :after (es-mode)
  :init
  (add-to-list 'org-babel-load-languages '(elasticsearch . t)))

;; JQ
;; requires jq binary to be installed and in $PATH
(use-package jq-mode
  :mode "\\.jq\\'")

(provide 'pkg-javascript)
