;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	C/C++ IDE Setup
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(add-hook 'c-mode-hook 'hs-minor-mode)
(add-hook 'c++-mode-hook 'hs-minor-mode)

(use-package cc-mode
  :ensure nil
  :straight nil
  :bind (:map c-mode-map
	      ("M-h" . helm-gtags-pop-stack)
	      ("M-l" . helm-gtags-find-tag))
  :config
  (progn
    (require 'compile)
    (c-toggle-auto-newline 1)))

(use-package disaster
  :commands (disaster))

;; clang-format can be triggered using C-c C-f
;; Install clang-format: $ apt-get install clang-format-3.9
;;                       $ ln -s `which clang-format-3.9` ~/bin/clang-format
;; Create clang-format file using google style
;; clang-format -style=google -dump-config > .clang-format
(use-package clang-format
  ;; :bind ("C-c C-f" . 'clang-format-region)
  )

(use-package cmake-mode
  :mode (("CMakeLists\\.txt\\'" . cmake-mode) ("\\.cmake\\'" . cmake-mode)))

(use-package company-c-headers)

(use-package realgud
  :commands (realgud:gdb
             realgud:ipdb
             realgud:pdb))


(use-package gdb-mi
  :ensure nil
  :straight nil
  :init
  (setq
   ;; use gdb-many-windows by default when `M-x gdb'
   gdb-many-windows t
   ;; Non-nil means display source file containing the main routine at startup
   gdb-show-main t))


(provide 'pkg-clang)
