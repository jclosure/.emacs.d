
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; GROOVY
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(use-package groovy-mode
  :mode (("/Jenkinsfile\\'" . groovy-mode)
         ("\\.groovy\\'" . groovy-mode)
         ("\\.gvy\\'" . groovy-mode)
         ("\\.gy\\'" . groovy-mode)
         ("\\.gsh\\'" . groovy-mode)))


(provide 'pkg-groovy)
