;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; SHELL SCRIPT LANGUAGE SUPPORT
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


;; company-shell
;; to provide company-shell-env only
(use-package company-shell
  :after (company))

;; shell scripting
(use-package sh-script
  :mode (("\\.alias\\'"       . shell-script-mode)
         ("\\..*sh\\'"        . shell-script-mode)
         ("\\.[a-zA-Z]+rc\\'" . shell-script-mode)
         ("crontab.*\\'"     . shell-script-mode))
  :init
  (defun my-shell-script-mode-hook ()
    "Hook for `sh-mode'"
    (set (make-local-variable 'company-backends)
         '((company-capf company-dabbrev-code company-shell-env company-yasnippet company-files))))
  :config
  (progn
    (add-hook 'sh-mode-hook 'my-shell-script-mode-hook)

    ))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; ENSURE SHELL SCRIPTS ARE AUTOMATICALLY MARKED EXUCUTABLE
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(use-package executable
  :ensure nil
  :hook
  ((after-save .
               executable-make-buffer-file-executable-if-script-p)))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; SHELL SYNTAX CHECKING
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; syntax checking in shell scripts
;; (use-package flymake-shell
;;   :hook (sh-set-shell-hook . flymake-shell-load))

(provide 'pkg-shell)
