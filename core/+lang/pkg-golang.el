;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	Go IDE Setup
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(use-package go-mode
  :bind (:map go-mode-map
	      ("M-l" . godef-jump)
	      ("M-h" . pop-tag-mark))
  :hook ((go-mode . lsp-deferred)
         ;; (before-save . lsp-format-buffer)
         ;; (before-save . lsp-organize-imports))
         )
  :config
  (progn
    (setq gofmt-command "goimports")
    (add-hook 'before-save-hook 'gofmt-before-save)
    ))

(use-package company-go
  :init
  (progn
    (add-hook 'go-mode-hook (lambda ()
                              (set (make-local-variable 'company-backends) '(company-go))
                              (company-mode)))
    (setq company-go-show-annotation t)
    (setq company-tooltip-limit 20) ; bigger popup window

    ;; Customize compile command to run go build
    (if (and compile-command (not (string-match "go" compile-command)))
        (setq compile-command
              "go build -v && go test -v && go vet"))
    )
  )

(use-package go-eldoc
  :config
  (progn
    (add-hook 'go-mode-hook 'go-eldoc-setup)
    ))

(use-package go-guru

  :hook (go-mode . go-guru-hl-identifier-mode))
(use-package go-rename
  :ensure t)

;; (use-package flycheck-gometalinter)

(use-package flycheck-gometalinter
  :ensure t
  :config
  (progn
    (flycheck-gometalinter-setup)))


(use-package go-playground
  :bind (:map go-mode-map
	      ("M-<RET>" . go-playground-exec)))


(use-package go-scratch
  :ensure t
  :commands go-scratch)


(use-package gorepl-mode
  :commands gorepl-run-load-current-file)

;; Dlv Debugger
;; (require 'go-dlv)

;; go get golang.org/x/tools/cmd/...
;; go get github.com/rogpeppe/godef
;; go get -u github.com/nsf/gocode
;; go get golang.org/x/tools/cmd/goimports



;; go hydra
(use-package use-package-hydra :ensure t)
(use-package hydra
  :ensure t
  :config
  (require 'hydra)
  ;;:commands (ace-flyspell-setup)
  :bind
  ;;("M-s" . hydra-go/body)
  :init
  (add-hook 'dap-stopped-hook
            (lambda (arg) (call-interactively #'hydra-go/body)))
  :hydra (hydra-go (:color pink :hint nil :foreign-keys run)
                   "
   _n_: Next       _c_: Continue _g_: goroutines      _i_: break log
   _s_: Step in    _o_: Step out _k_: break condition _h_: break hit condition
   _Q_: Disconnect _q_: quit     _l_: locals
   "
	                 ("n" dap-next)
	                 ("c" dap-continue)
	                 ("s" dap-step-in)
	                 ("l" dap-ui-locals)
	                 ("e" dap-eval-thing-at-point)
	                 ("h" dap-breakpoint-hit-condition)
	                 ("k" dap-breakpoint-condition)
	                 ("i" dap-breakpoint-log-message)
	                 ("q" nil "quit" :color blue)
	                 ("Q" dap-disconnect :color red)))

;; DAP
(use-package dap-mode
  ;;:custom
  ;; .emacs.d/.extension/vscode/ms-vscode.go/extension/out/src/debugAdapter/goDebug.js
  ;;(dap-go-debug-program `("node" "~/extension/out/src/debugAdapter/goDebug.js"))
  :config
  (dap-mode 1)
  (setq dap-print-io t)
  ;;(setq fit-window-to-buffer-horizontally t)
  ;;(setq window-resize-pixelwise t)
  (require 'dap-hydra)
  (require 'dap-mode)
  (require 'dap-ui)

  (require 'dap-go)		; download and expand vscode-go-extenstion to the =~/.extensions/go=
  (dap-go-setup)
  ;; (use-package dap-ui
  ;;   :ensure nil
  ;;   :config
  ;;   (dap-ui-mode 1)
  ;;   )

  ;; setup the debug template
  ;; (dap-register-debug-template
  ;;  "Launch Executable"
  ;;  (list :type "go"
  ;;        :request "launch"
  ;;        :name "Launch Executable"
  ;;        :mode "exec"
  ;;        :program nil
  ;;        :args "--help"
  ;;        :env nil
  ;;        :envFile nil))

  )



(provide 'pkg-golang)
