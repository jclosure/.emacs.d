
(use-package disaster
  :commands (disaster))

(use-package gdb-mi
  :ensure nil
  :init
  (setq
   ;; use gdb-many-windows by default when `Mx gdb '
   gdb-many-windows t
   ;; Non-nil means display source file containing the main routine at startup
   gdb-show-main t))



(with-eval-after-load 'lsp-mode
  (add-hook 'lsp-mode-hook #'lsp-enable-which-key-integration)

  (require 'dap-cpptools)

  (when (featurep 'helm)
    (use-package helm-lsp))

  (setq treemacs-space-between-root-nodes nil
        lsp-idle-delay 0.1)

  (add-hook 'c-mode-hook 'lsp)
  (add-hook 'c++-mode-hook 'lsp)


  ;; EXAMPLE OF DOING IT PROGRAMMATICALLY
  ;; (dap-register-debug-template
  ;;  "Debug Emacs"
  ;;  (list :type "cppdbg"
  ;;        :request "launch"
  ;;        :name "Debug Emacs"
  ;;        :program "${workspaceFolder}/src/emacs"
  ;;        :args '("-q")
  ;;        :stopAtentry nil
  ;;        :cwd "${workspaceFolder}"
  ;;        :environment '()
  ;;        :externalConsole nil
  ;;        :MIMode "gdb"))
  )


;; EXAMPLE OF: launch.json, which is put in root of project
;; {
;; "version": "0.2.0",
;; "configurations": [
;;                    {
;;                    "name": "Debug Emacs",
;;                    "type": "cppdbg",
;;                    "request": "launch",
;;                    "program": "${workspaceFolder}/src/emacs",
;;                    "args": ["-q"],
;;                    "stopAtEntry": false,
;;                    "cwd": "${workspaceFolder}",
;;                    "environment": [],
;;                    "externalConsole": false,
;;                    "MIMode": "gdb"
;;                    }
;;                    ]
;; }
(provide 'pkg-c++)
