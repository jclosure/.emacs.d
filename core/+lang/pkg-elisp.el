;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; EMACS LISP
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	Automatically compile Emacs Lisp libraries
;;	https://github.com/emacscollective/auto-compile
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(use-package auto-compile
  :init (setq load-prefer-newer t)
  :config
  (progn
    (auto-compile-on-load-mode)
    (auto-compile-on-save-mode)))


;; interactive macro expanding and pretty printing to show exacltly how what a macro expands to with each step
;; https://github.com/joddie/macrostep
(use-package macrostep
  :commands (macrostep-expand
             macrostep-mode))


(use-package emacs-lisp
  :ensure nil
  :straight nil
  :init
  (defun my-elisp-mode-hook ()
    "Hook for `emacs-lisp-mode'"
    (set (make-local-variable 'company-backends)
         '((company-capf company-dabbrev-code company-yasnippet company-files))))
  :mode ("\\.el$" . emacs-lisp-mode)
  :bind (:map emacs-lisp-mode-map
              ("C-c I" . describe-function)
              ("C-c S" . find-function-at-point)))
;; leave these outside of above
(add-hook 'emacs-lisp-mode-hook 'my-elisp-mode-hook)
(add-hook 'emacs-lisp-mode-hook 'company-mode)


(provide 'pkg-elisp)
