
;; NOTE: THESE KEYS WILL COLLIDE

(use-package ggtags
  :straight t
  :after (:any company)
  :diminish ggtags-mode
  :commands (ggtags-mode)
  :config
  (setq ggtags-update-on-save nil)
  (setq ggtags-use-idutils t)
  (setq ggtags-sort-by-nearness t)
  (unbind-key "M-<" ggtags-mode-map)
  (unbind-key "M->" ggtags-mode-map))

(use-package helm-gtags
  :straight t
  :after ggtags
  :commands (helm-gtags-select helm-gtags-find-tag)
  :config
  (setq helm-gtags-fuzzy-match t)
  (setq helm-gtags-preselect t)
  (setq helm-gtags-prefix-key "C-c g")
  (setq helm-gtags-path-style 'relative)

  (define-key helm-gtags-mode-map (kbd "M-.") 'helm-gtags-dwim)
  (define-key helm-gtags-mode-map (kbd "M-,") 'helm-gtags-pop-stack))



(provide 'pkg-gtags)
