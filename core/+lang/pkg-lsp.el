;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	Language server protocol Setup
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(use-package lsp-mode
  :ensure
  :commands lsp
  :config
  (progn
    ;; (lsp-enable-which-key-integration t)
    (add-hook 'lsp-mode-hook 'lsp-ui-mode)

    ;; set visual indicator of focus to just an underline
    (set-face-attribute 'lsp-face-highlight-textual nil
		                    :background nil
                        :foreground nil
                        :underline t
                        )))

(use-package lsp-ui
  :ensure
  :commands lsp-ui-mode
  :init
  ;; how to show/hide lsp-ui items: https://emacs-lsp.github.io/lsp-mode/tutorials/how-to-turn-off/
  (setq lsp-ui-sideline-show-code-actions nil
        lsp-ui-sideline-show-hover nil
        ;; lsp-eldoc-enable-hover nil
        lsp-signature-auto-activate nil
        ;; lsp-signature-doc-lines 1
        lsp-eldoc-render-all nil
        ;; lsp-signature-render-documentation nil

        ;; turn off showing eldoc in minibuffer
        lsp-ui-doc-use-childframe nil
        )
  :custom
  (lsp-ui-peek-always-show t)
  (lsp-ui-sideline-show-hover t)
  (lsp-ui-doc-enable nil)
  :config
  ;; possibly just move to custom.el
  (set-face-attribute 'lsp-face-highlight-textual nil
		                  :background nil
                      :foreground nil
                      :underline t))




(provide 'pkg-lsp)
