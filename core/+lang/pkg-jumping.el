;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	non-lexical jumping
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


;; ;; dumb-jump uses ag for jumps if avail, else grep
;; ;; must be latest ver for newer lang support

;; IMPORTANT:
;; IF YOU GET THE ERROR: "invalid function helm-build-sync-source",
;; run the following: M-x `elpa-recompile-all'
;; ref: https://github.com/syl20bnr/spacemacs/issues/12096

(use-package dumb-jump
  ;; smart-jump owns these..
  ;; :bind (("M-." . dumb-jump-go)
  ;;        ("M-," . dumb-jump-back))
  ;; force load
  ;; :after persp-mode
  :demand t
  :config
  (progn
    (defun dumb-jump--format-result (proj result)
      (let ((path (s-replace proj "" (plist-get result :path)))
            (line (plist-get result :line))
            (context (s-trim (plist-get result :context))))
        (format "%s:%s: %s"
                (if (projectile-project-p)
                    (progn
                      (replace-regexp-in-string "\\./" ""
                                                (replace-regexp-in-string "\\.\\./" "" path)))
                  path)
                line
                context)))

    ;; NOTE: overriding internal function in order to format to my liking
    ;; TODO: create a PR with pluggable formatting function for dumb-jump
    (setq
     ;; comment out to use popup (like company-mode) as selector
     dumb-jump-selector 'helm

     dumb-jump-aggressive nil

     ;; NOTE: I have modified the following to work for root build marker
     ;; set it to your needs or unset for defaults:
     ;; https://github.com/jacktasia/dumb-jump/blob/master/dumb-jump.el#L1450
     dumb-jump-project-denoters '("Jenkinsfile"))))


;; NOTE: make elisp jumps also work
;; TODO: keep an eye on this and make sure of the following:
;; no negative impact on performance with jump ops
;; no strange behaviors in different major modes
(use-package smart-jump
  :config
  (progn
    ;; go ahead and allow smart jump to be smart

    (defun smart-jump-find-references-with-helm-xref ()
      "Use `helm-xref-show-xrefs' to find references."
      (interactive)
      (if (featurep 'helm-xref)
          (helm-xref-show-xrefs
           (cond ((use-region-p)
                  (buffer-substring-no-properties (region-beginning)
                                                  (region-end)))
                 ((symbol-at-point)
                  (substring-no-properties
                   (symbol-name (symbol-at-point))))),
           nil ;; TODO: this will not do anything if there's no xref backend. put default as helm-projectile-ag
           )
        (message
         "Install the emacs package helm-xref to use\
 `smart-jump-simple-find-references-with-helm-xref'.")))

    ;; set smart-jump's final fallback to my own function
    (setq smart-jump-find-references-fallback-function
          'smart-jump-find-references-with-helm-xref)

    ;; disable smart-jump's final fallback
    ;; (setq smart-jump-find-references-fallback-function
    ;;       nil)

    ;; BEGIN REQUIREMENTS
    ;; IMPORTANT: all requirements need to be loaded before registration
    ;; (require 'elpy)
    (require 'xref-js2)
    (require 'rjsx-mode)
    ;; END REQUIREMENTS

    (smart-jump-setup-default-registers)

    ;; ;; overriding to force use of elpy rather than anaconda mode - see: smart-jump-python.el
    ;; (smart-jump-register :modes 'python-mode
    ;;                      :jump-fn 'elpy-goto-definition
    ;;                      :pop-fn 'xref-pop-marker-stack
    ;;                      ;; pending https://github.com/jorgenschaefer/elpy/issues/1082
    ;;                      :refs-fn 'xref-find-references
    ;;                      ;; :refs-fn 'helm-projectile-ag
    ;;                      :should-jump t
    ;;                      :heuristic 'point
    ;;                      :async 300
    ;;                      :order 0 ; lower is priority
    ;;                      )


    ;; overriding default register because NOT using alchemist-mode for this - see: smart-jump-elixir.el
    (smart-jump-register :modes 'elixir-mode
                         :jump-fn 'dumb-jump-go
                         :pop-fn 'dumb-jump-back
                         :refs-fn 'xref-find-references
                         ;; :refs-fn 'helm-projectile-ag
                         :should-jump 'xref-prompt-for-identifier
                         :heuristic 'point
                         :async 300
                         :order 0 ; lower is priority
                         )


    ;; IMPORTANT: make sure rust-analyzer is in your current $PATH
    (smart-jump-register :modes 'rust-mode
                         :jump-fn 'lsp-find-definition
                         :pop-fn 'pop-tag-mark
                         :refs-fn 'lsp-find-references
                         :should-jump (and (bound-and-true-p rustic-mode)
                                           (executable-find "rust-analyzer"))
                         :heuristic 'point)

    ;; hard override of smart-jump-references with default fn - these are mutually exclusive with above
    ;; (define-key python-mode-map (kbd "M-?") 'xref-find-references)
    ;; (define-key elixir-mode-map (kbd "M-?") 'xref-find-references)
    ;; (define-key js2-mode-map (kbd "M-?") 'xref-find-references)
    ;; (global-set-key (kbd "M-?")  'xref-find-references)

  ))



(provide 'pkg-jumping)
