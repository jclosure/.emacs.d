

(require 'pkg-compilation)

(require 'pkg-company)

(require 'pkg-lsp)
;; (require 'pkg-eglot)

(require 'pkg-english)
(require 'pkg-sql)
(require 'pkg-groovy)
(require 'pkg-elisp)
(require 'pkg-shell)
(require 'pkg-python)
(require 'pkg-golang)
(require 'pkg-clang)
(require 'pkg-markdown)
(require 'pkg-rust)
(require 'pkg-c++)
(require 'pkg-elixir)
(require 'pkg-javascript)
(require 'pkg-typescript)
(require 'pkg-web)

(require 'pkg-tools)
(require 'json)

;; code nav and introspection
(require 'pkg-jumping) ; IMPORTANT: deps need to be loadable already



(provide 'lang)
