;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; 	Web IDE Setup
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; Major mode for editing web templates
(use-package web-mode
  :mode (
         ("/\\(views\\|html\\|theme\\|templates\\)/.*\\.php\\'" . web-mode)
         ("\\.[agj]sp\\'" . web-mode)
         ("\\.as[cp]x\\'" . web-mode)
         ("\\.blade\\.php\\'" . web-mode)
         ("\\.djhtml\\'" . web-mode)
         ("\\.ejs\\'" . web-mode)
         ("\\.erb\\'" . web-mode)
         ("\\.html?\\'" . web-mode)
         ("\\.jsp\\'" . web-mode)
         ("\\.mustache\\'" . web-mode)
         ("\\.php\\'" . web-mode)
         ("\\.phtml\\'" . web-mode)
         ("\\.tpl\\.php\\'" . web-mode)
         ("\\.xml\\'" . web-mode)
         )
  :hook
  ((web-mode . company-mode))
  :config
  (progn

    ;; autosyntax closing/pairing
    (setq web-mode-enable-auto-closing t
          web-mode-enable-auto-pairing t
          web-mode-enable-auto-indentation t
          web-mode-enable-auto-opening t
          web-mode-enable-auto-quoting t
          web-mode-tag-auto-close-style 2

          web-mode-enable-auto-closing t
          web-mode-enable-auto-pairing t
          web-mode-enable-comment-keywords t
          web-mode-enable-current-element-highlight t
          web-mode-code-indent-offset 4
          web-mode-css-indent-offset 4
          web-mode-markup-indent-offset 4
          web-mode-block-padding 4
          web-mode-script-padding 4
          web-mode-style-padding 4
          )



    ))

(use-package emmet-mode
  :hook ((web-mode . emmet-mode)
         (css-mode . emmet-mode)))

(use-package web-beautify)
(use-package haml-mode)
(use-package php-mode)

;; REST
(use-package restclient
  :mode ("\\.http\\'" . restclient-mode)
  :config
  (use-package restclient-test
    :diminish
    :hook (restclient-mode . restclient-test-mode))

  (with-eval-after-load 'company
    (use-package company-restclient
      :defines company-backends
      :init (add-to-list 'company-backends 'company-restclient))))

(provide 'pkg-web)
