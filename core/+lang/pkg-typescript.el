
;; TYPESCRIPT

;; TypeScript completion
(use-package tide

  :ensure t
  :after (typescript-mode web-mode company flycheck)
  :hook ((typescript-mode . tide-setup)
	       (typescript-mode . tide-hl-identifier-mode)
	       (web-mode . (lambda ()
		                   (when (string-equal "tsx" (file-name-extension
						                                      buffer-file-name))
			                   (tide-setup))))
	       (web-mode . (lambda ()
		                   (when (string-equal "tsx" (file-name-extension
						                                      buffer-file-name))
			                   (tide-hl-identifier-mode)))))
  :config (flycheck-add-mode 'typescript-tslint 'web-mode))

;; TypeScript highlighting
(use-package typescript-mode

  :ensure t
  :mode (("\\.ts\\'" . typescript-mode))
  :custom
  (typescript-indent-level 2 "TypeScript indent size"))

(provide 'pkg-typescript)
