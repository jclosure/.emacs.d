
;; SQL
(use-package sql
  :init
  ;; (add-hook 'sql-mode-hook 'regtab-mode)
  (add-hook 'sql-interactive-mode-hook 'regtab-mode))

(use-package sqlup-mode
  :init
  (add-hook 'sql-mode-hook 'sqlup-mode)
  (add-hook 'sql-interactive-mode-hook 'sqlup-mode))

(use-package sql-indent

  :init
  (add-hook 'sql-mode-hook 'sqlind-minor-mode)
  (add-hook 'sql-interactive-mode-hook 'sqlind-minor-mode))

(use-package sqlformat
  ;; :custom (sqlformat-command 'sqlformat)
  :custom (sqlformat-command 'pgformatter) ; brew install pgformatter or apt-get install pgformatter
  )

(provide 'pkg-sql)
