;; (use-package company-box
;;   ;; :ensure nil
;;   ;; :quelpa (company-box :fetcher github :repo "sebastiencs/company-box")
;;   :hook (company-mode . company-box-mode))

;; company
;; TODO: try this: https://github.com/TommyX12/company-tabnine
(use-package company
  :after (general)
  :preface
  (defun company-mode-enable ()
    (company-mode  1))
  :custom
  (company-begin-commands '(self-insert-command))
  ;; (company-idle-delay .1)
  (company-minimum-prefix-length 2)
  ;; (company-show-numbers t)
  (company-tooltip-align-annotations 't)
  ;; (global-company-mode t)
  :hook ((prog-mode . company-mode-enable)
         (elisp-mode . company-mode-enable))
  ;; :bind
  ;; (:map company-active-map
  ;;       ("ESC" . company-abort)) ; side-effects on <up>/<down> in company-active-map breaks: https://emacs.stackexchange.com/questions/37659/oa-and-ob-inserted-by-emacs
  :general
  (:keymaps 'company-active-map
            "C-n" 'company-select-next
            "C-p" 'company-select-previous
            "C-d" 'company-show-doc-buffer)
  :config
  ;;; TODO: add these mode hooks for company instead of global
  ;; latex-mode-hook
  ;; html-mode-hook
  ;; emacs-lisp-mode-hook
  ;; prog-mode-hook
  ;; python-mode-hook
  (progn

    ;; if you have svg support, put icons in left margin
    (when (not *is-in-terminal*)
      (setq company-format-margin-function #'company-vscode-dark-icons-margin))

    ;; setup our backends
    (add-to-list 'company-backends 'company-capf)

    ;; cycle completion options with tab-and-go
    ;; (company-tng-configure-default)

    ;; allow tab cycling
    (eval-after-load 'company
      '(progn
         (define-key company-active-map (kbd "TAB") 'company-complete-common-or-cycle)
         (define-key company-active-map (kbd "<tab>") 'company-complete-common-or-cycle)))

    (eval-after-load 'company
      '(progn
         (define-key company-active-map (kbd "S-TAB") 'company-select-previous)
         (define-key company-active-map (kbd "<backtab>") 'company-select-previous)))


    ;; ^ wires these company-frontends and keys:
    ;; https://github.com/emacs-evil/evil-collection/issues/41#issuecomment-349500540

    (setq company-idle-delay nil
          company-minimum-prefix-length 0
          company-auto-complete t)

    ;; ;; swap search and filter shortcuts.
    ;; (define-key company-active-map "\C-s" 'company-filter-candidates)
    ;; (define-key company-active-map "\C-\M-s" 'company-search-candidates)


    ;; tab trigger approaches:

    ;; approach 1
    ;; use company provided indent-or-complete.  works well but is not smart
    ;; like approach 2, because expands on unwanted chars, e.g. (){}*, etc...
    ;; (global-set-key (kbd "TAB") #'company-indent-or-complete-common)

    ;; approach 2
    ;; ref: https://www.emacswiki.org/emacs/CompanyMode#toc6
    ;; using this approach for tab+complete control
    ;; it only completes if prior char is a _, -, or regular character class
    ;; including explicit .
    ;; also this allows yasnippet to preempt completion if there's a match.
    ;; the yasnippet expanse  may be black some completions, so it can be disabled if want.
    (progn

      (defun str-char-before-point ()
        (string (char-before (point))))
      (defun str-char-at-point ()
        (string (char-before (+ 1 (point)))))
      (defun str-char-after-point ()
        (string (char-after (point))))

      (defun generally-completable-char-before-point-p ()
        (string-match-p  "[\\-a-zA-Z0-9_\\.]" (str-char-before-point)))

      (defun generic-prog-mode-point-and-after-point-criteria-match-p ()
        "Sufficient for most programming languages, e.g. elisp, python, c"
        (or
         (at-end-of-line-text-p) ; point at the end of visual line
         (string-match-p  "[\s\t]+" (str-char-after-point)) ; space after point
         (string-match-p  "\)" (str-char-at-point)) ; point over )
         (string-match-p  "\(" (str-char-at-point)))) ; point over (

      ;; NOTE: different check-expansions can be created for different
      ;; language modes in order to handle different syntatic needs.
      ;; (setq print-maybe-on t)
      (defun check-expansion ()
        (save-excursion
          (if (and
               ;; character befor point must be
               (generally-completable-char-before-point-p)
               ;; and now what is point looking at and what is after that
               (generic-prog-mode-point-and-after-point-criteria-match-p))
              (print-maybe "match" t) ; return t
            (print-maybe "no match" nil) ; return nil
            )))

      (defun do-yas-expand ()
        (let ((yas/fallback-behavior 'return-nil))
          (yas/expand)))

      ;; NOTE: we are using several internal functions for this.
      ;; check to see if there's a better way to do this
      (defun in-live-snippet-p ()
        "Tells us if point is in a live snippet"
        (interactive)
        (if (and (bound-and-true-p yas/minor-mode) yas--active-field-overlay)
            (let* ((snippet (overlay-get yas--active-field-overlay 'yas--snippet)))
              (yas--snippet-live-p snippet))
          nil))

      ;; no yas expansion attempt included
      (defun my-company-complete-maybe ()
        "Checks if we're in a snippet, if so TAB advances through fields
         otherwise it completes or indents according to the result of check-expansion"
        (interactive)
        (if (in-live-snippet-p)
            (yas-next-field)
          (if (minibufferp)
              (minibuffer-complete)
            (if (check-expansion)
                (progn
                  ;; (message "found expansion") ; for debugging
                  (company-complete-common)
                  t)
              (progn
                ;; (message "no expansion found") ; for debugging
                nil
                )))))

(defun my-company-complete-or-indent-for-tab ()
  (interactive)
  (unless (my-company-complete-maybe)
    (indent-for-tab-command)))

      ;; ;; includes an attempt to yas expand
      ;; (defun my-tab-indent-or-company-complete ()
      ;;   (interactive)
      ;;   (if (minibufferp)
      ;;       (minibuffer-complete)
      ;;     (if (or (not yas/minor-mode)
      ;;             (null (do-yas-expand)))
      ;;         (if (check-expansion)
      ;;             (company-complete-common)
      ;;           (indent-for-tab-command)))))


      ;; TODO: promote to :keymaps at use-package level
      ;;;; we set the binding to our tab triggering fn
      ;; (global-set-key (kbd "TAB") 'my-tab-indent-or-company-complete)
      (general-define-key
       :keymaps 'prog-mode-map
       "TAB" 'my-company-complete-or-indent-for-tab
       "<tab>" 'my-company-complete-or-indent-for-tab
       ;; ...
       )

      ;;;; XOR
      ;;;; toggle company menu w/ C-TAB
      ;; helm buffers this globally
      ;; (my/global-map-and-set-key "C-TAB" 'company-complete-common)


      ;; allow C-TAB to close company menu
      (define-key company-active-map (kbd "C-TAB") #'company-abort)


      ;; use enter to complete explicitly and don't send a CR when in company-active mode
      (define-key company-active-map (kbd "RET") #'company-complete-selection))

    ;; setup with yas
    (require 'company-yasnippet)

    ;; ;; enable company mode everywhere, also serves to prestart it for prog-modes
    ;; (global-company-mode 1)
    ))


(use-package company-flx
  :demand t
  :config
  (company-flx-mode +1))


;; most frequently used to top
;; very heavy load ... defer
;; (use-package company-statistics
;;   :config
;;   (company-statistics-mode))





(provide 'pkg-company)
