
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	PIP SUPPORT
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(use-package pip-requirements
  :ensure t
  :mode (("requirements.*\\.txt" . pip-requirements-mode)))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	Python IDE Setup
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; NOTE: ensure pyenv installed
(use-package pyenv-mode
  :init
  (add-to-list 'exec-path "~/.pyenv/shims")
  (setenv "WORKON_HOME" "~/.pyenv/versions/")
  :config
  (pyenv-mode))


;; IPYTHON for interpreter
;; (use-package python
;;   :mode ("\\.py\\'" . python-mode)
;;   :config
;;   (progn
;;     ;; use ipython for interpreter if it's available
;;     (if (executable-find "ipython")
;;         (setq python-shell-interpreter "ipython"
;;               python-shell-interpreter-args "--simple-prompt -i")
;;       (setq setq python-shell-interpreter "python3"))))

;; Use Jupyter's IPython for REPL
(setq python-shell-interpreter "jupyter"
      python-shell-interpreter-args "console --simple-prompt"
      python-shell-prompt-detect-failure-warning nil)
(add-to-list 'python-shell-completion-native-disabled-interpreters
             "jupyter")

(use-package python
  :commands python-mode
  :interpreter ("python3" . python-mode)
  :hook
  (python-mode-hook . (lambda ()
                        (highlight-lines-matching-regexp "import ipdb")
                        (highlight-lines-matching-regexp "ipdb.set_trace()")
                        (highlight-lines-matching-regexp "import wdb")
                        (highlight-lines-matching-regexp "wdb.set_trace()")))
  ;; :custom
  ;; (python-environment-virtualenv (quote ("python3" "-m" "venv")))
  :init
  (progn
    "hydra for shifting python code left/right more easily"

    (defun tom/shift-left (start end &optional count)
      "Shift region left and activate hydra."
      (interactive
       (if mark-active
           (list (region-beginning) (region-end) current-prefix-arg)
         (list (line-beginning-position) (line-end-position) current-prefix-arg)))
      (python-indent-shift-left start end count)
      (tom/hydra-move-lines/body))

    (defun tom/shift-right (start end &optional count)
      "Shift region right and activate hydra."
      (interactive
       (if mark-active
           (list (region-beginning) (region-end) current-prefix-arg)
         (list (line-beginning-position) (line-end-position) current-prefix-arg)))
      (python-indent-shift-right start end count)
      (tom/hydra-move-lines/body))

    (defhydra tom/hydra-move-lines ()
      "Move one or multiple lines"
      ("<" python-indent-shift-left "left")
      (">" python-indent-shift-right "right")))

  :bind (:map python-mode-map
              ("C-c <" . tom/shift-left)
              ("C-c >" . tom/shift-right)))


;; (use-package auto-virtualenvwrapper
;;   :ensure t
;;   :commands auto-virtualenvwrapper-activate
;;   :init
;;   (add-hook 'python-mode-hook #'auto-virtualenvwrapper-activate)
;;   (add-hook 'window-configuration-change-hook #'auto-virtualenvwrapper-activate)
;;   (add-hook 'focus-in-hook #'auto-virtualenvwrapper-activate)
;;   :config
;;   (setq auto-virtualenvwrapper-verbose t))

;; ;; NOTE: It is very important that all libs that are
;; ;; accessible to jedi be either in the project dir or
;; ;; available on the PYTHONPATH.
;; (use-package company-jedi
;;   :ensure t
;;   :after python

;;   :bind (:map python-mode-map
;;          ;; ("M-." . jedi:goto-definition)
;;          ;; ("M-," . my-jedi:goto-definition-pop-marker)
;;          ;; ("M-/" . jedi:show-doc)
;;          ;; ("M-?" . helm-jedi-related-names)
;;          ; ("C-c C-d" . jedi:show-doc)
;;          :map comint-mode-map
;;          ("C-l" . comint-clear-buffer))
;;   :init
;;   (setq jedi:complete-on-dot t)
;;   (add-hook 'python-mode-hook (lambda () (add-to-list 'company-backends 'company-jedi)))
;;   :config
;;   (progn
;;     ;; NOTE: I'm defining my own ver of jedi:goto-definistion-pop-marker fn in oder to make
;;     ;; it fallback to use (xref-pop-marker-stack), so the helm-projectile-rg advice xref find behaves
;;     ;; the same in python with jedi: marker mgmnt functions
;;     (defun my-jedi:goto-definition-pop-marker ()
;;       "Goto the last point where `jedi:goto-definition' was called."
;;       (interactive)
;;       (if (ring-empty-p jedi:goto-definition--marker-ring)
;;           (xref-pop-marker-stack)
;;         ;; (error "Jedi marker ring is empty, can't pop")
;;         (let ((marker (ring-remove jedi:goto-definition--marker-ring 0)))
;;           (switch-to-buffer (or (marker-buffer marker)
;;                                 (error "Buffer has been deleted")))
;;           (goto-char (marker-position marker))
;;           ;; Cleanup the marker so as to avoid them piling up.
;;           (set-marker marker nil nil))))
;;
;;     ;; here's another approach, that does not use jedi's builtin marker-ring
;;     ;; https://txt.arboreus.com/2013/02/21/jedi.el-jump-to-definition-and-back.html
;;
;;     ;; center the jumped def into the middle of the screen using hook
;;     (defun my-jedi-goto-definition-center-in-window-hook ()
;;       (let ((recenter-positions '(middle)))
;;         (recenter-top-bottom)))
;;
;;     (add-hook 'jedi:goto-definition-hook
;;               'my-jedi-goto-definition-center-in-window-hook)
;;     )
;;      ;; in this case we're using jedi's internal marker ring for python
;;      ;; (defun my-python-push-marker-ring ()
;;      ;;     (jedi:goto-definition-push-marker))
;; )


;; lets you manage pip packages
(use-package pippel)

;; requires > python3.7
;; need black binary: pip install black
(use-package blacken
  :ensure t)

;; (use-package py-autopep8
;;   :hook (python-mode-hook . py-autopep8-enable-on-save))

(use-package jinja2-mode
  :mode (("\\.j2$" . jinja2-mode)))

(use-package yapfify
  :config
  (defun format-python()
    (when (eq major-mode 'python-mode)
      (yapfify-buffer)))

  (add-hook 'python-mode-hook 'yapf-mode)
  (add-hook 'before-save-hook #'format-python)
  )


;; (use-package elpy
;;   :commands (elpy-goto-definition ;; autoloaded by smart-jump
;;              elpy-goto-assignment
;;              elpy-goto-location)
;;   :config
;;   (progn
;;     ;; set of functions to discover project root used by elpy
;;     ;; elpy-project-root-finder-functions
;;     ))



;; EXPERIMENTAL

;; (lsp-register-client
;;  (make-lsp-client :new-connection (lsp-stdio-connection "pyls")
;; 		              :major-modes '(python-mode)
;; 		              :server-id 'pyls))


;; borrowed from: https://github.com/KaratasFurkan/.emacs.d#general
(defun fk/async-process (command &optional name filter)
  "Start an async process by running the COMMAND string with bash. Return the
process object for it.

NAME is name for the process. Default is \"async-process\".

FILTER is function that runs after the process is finished, its args should be
\"(process output)\". Default is just messages the output."
  (make-process
   :command `("bash" "-c" ,command)
   :name (if name name
           "async-process")
   :filter (if filter filter
             (lambda (process output) (message (s-trim output))))))

;; Examples:
;;
;; (fk/async-process "ls")
;;
;; (fk/async-process "ls" "my ls process"
;;                   (lambda (process output) (message "Output:\n\n%s" output)))
;;
;; (fk/async-process "unknown command")


;; (when (executable-find "pyright"))




(use-package lsp-pyright
  :after lsp-mode
  :hook ((python-mode . (lambda ()
                          (require 'lsp-pyright) (lsp-deferred))))
  :custom
  (lsp-pyright-auto-import-completions nil)
  (lsp-pyright-typechecking-mode "off")
  :config
  (progn
    (fk/async-process
     "npm outdated -g | grep pyright | wc -l" nil
     (lambda (process output)
       (pcase output
         ("0\n" (message "Pyright is up to date."))
         ("1\n" (message "A pyright update is available.")))))


    (unless (fboundp 'lsp-pyright-locate-python)
      (autoload #'lsp-pyright-locate-python "lsp-pyright" nil t))
    (unless (fboundp 'lsp-pyright-locate-venv)
      (autoload #'lsp-pyright-locate-venv "lsp-pyright" nil t))


    (defvar lsp-pyright-python-executable-cmd)
    (setq lsp-pyright-python-executable-cmd "python3")



    ;; Don't watch python site-path lib dirs and some others
    (setq lsp-file-watch-ignored-directories
          (append lsp-file-watch-ignored-directories
                  '(
                    "**/tmp"
                    "**/deps"
                    "**/Frameworks/Python.framework"
                    )))


    ))



(provide 'pkg-python)
