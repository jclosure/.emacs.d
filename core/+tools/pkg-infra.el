;; SSH
(use-package ssh-config-mode
  :mode ((".ssh/config\\'"       . ssh-config-mode)
         ("sshd?_config\\'"      . ssh-config-mode)
         ("known_hosts\\'"       . ssh-known-hosts-mode)
         ("authorized_keys\\'" . ssh-authorized-keys-mode)))

;; ANSIBLE
(use-package company-ansible
  :ensure t
  :after (company)
  :config
  (add-to-list 'company-backends 'company-ansible))

;; (use-package poly-ansible
;;   :ensure t)

;; DOCKER
; (use-package docker-tramp
;   :demand t
;   :ensure t)
; (require 'docker-tramp)
;
; (use-package docker
;   :ensure t)

(use-package dockerfile-mode
  :ensure t
  :mode "Dockerfile.*\\'")

;; NGNIX
(use-package nginx-mode)

;; TERRAFORM
(use-package terraform-mode
  :ensure t
  :init
  (setq terraform-indent-level 2))

(use-package company-terraform
  :ensure t
  :config
  (company-terraform-init))

;; GRAPHVIZ
(use-package graphviz-dot-mode
  :ensure t
  :mode ("\\.dot\\'" . graphviz-dot-mode)
  )

(provide 'pkg-infra)
