;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; EMACS WINDOW CONFIG, NAV, AND MANAGEMENT
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; The native border "consumes" a pixel of the fringe on righter-most splits,
;; `window-divider' does not. Available since Emacs 25.1.
(setq-default window-divider-default-places t
              window-divider-default-bottom-width 1
              window-divider-default-right-width 1)
(add-hook 'init-hook #'window-divider-mode)


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; WINDOW PLACEMENT
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; TODO: create more rules for shackle
;; (use-package shackle
;;   :custom
;;   (shackle-select-reused-windows t)
;;   :config
;;   (progn
;;     ;;(setq helm-display-function 'pop-to-buffer) ; make helm play nice
;;     ;;(setq shackle-rules '(("^\\Swoop\\b" :regexp t :align t :size 0.4)))

;;     ;; make magit always pop to the right at 40% of frame
;;     (setq shackle-rules
;;           '(("\\`\\*magit.*?\\*\\'" :regexp t :align top :size 0.9)
;;             ("\\magit:.*" :regexp t :align right :size 0.5)
;;             ("\\*.*Help.*\\*" :regexp t :align right :size 0.5)
;;             ("*git-gutter:diff*" :align right :size 0.5)
;;             ;; ("*Org Agenda*" :align right :size 0.5)
;;             )
;;           )
;;     ))



;; (use-package shackle
;;   :config
;;   (progn
;;     (setq shackle-lighter "")
;;     (setq shackle-select-reused-windows nil) ; default nil
;;     (setq shackle-default-alignment 'below) ; default below
;;     (setq shackle-default-size 0.4) ; default 0.5

;;     (setq shackle-rules
;;           ;; CONDITION(:regexp)            :select     :inhibit-window-quit   :size+:align|:other     :same|:popup
;;           '((compilation-mode              :select nil                                               )
;;             ("*undo-tree*"                                                    :size 0.25 :align right)
;;             ("*eshell*"                    :select t                          :other t               )
;;             ("*Shell Command Output*"      :select nil                                               )
;;             ("\\*Async Shell.*\\*" :regexp t :ignore t                                                 )
;;             (occur-mode                    :select nil                                   :align t    )
;;             ("*Help*"                      :select t   :inhibit-window-quit t :other t               )
;;             ("*Completions*"                                                  :size 0.3  :align t    )
;;             ("*Messages*"                  :select nil :inhibit-window-quit t :other t               )
;;             ("\\*[Wo]*Man.*\\*"    :regexp t :select t   :inhibit-window-quit t :other t               )
;;             ("\\*poporg.*\\*"      :regexp t :select t                          :other t               )
;;             ("\\`\\*helm.*?\\*\\'"   :regexp t                                    :size 0.3  :align t    )
;;             ("*Calendar*"                  :select t                          :size 0.3  :align below)
;;             ("*info*"                      :select t   :inhibit-window-quit t                         :same t)
;;             (magit-status-mode             :select t   :inhibit-window-quit t                         :same t)
;;             (magit-log-mode                :select t   :inhibit-window-quit t                         :same t)
;;             ))

;;     (shackle-mode 1)))


;; Elements of the `shackle-rules' alist:
;;
;; |-----------+------------------------+--------------------------------------------------|
;; | CONDITION | symbol                 | Major mode of the buffer to match                |
;; |           | string                 | Name of the buffer                               |
;; |           |                        | - which can be turned into regexp matching       |
;; |           |                        | by using the :regexp key with a value of t       |
;; |           |                        | in the key-value part                            |
;; |           | list of either         | a list groups either symbols or strings          |
;; |           | symbol or string       | (as described earlier) while requiring at        |
;; |           |                        | least one element to match                       |
;; |           | t                      | t as the fallback rule to follow when no         |
;; |           |                        | other match succeeds.                            |
;; |           |                        | If you set up a fallback rule, make sure         |
;; |           |                        | it's the last rule in shackle-rules,             |
;; |           |                        | otherwise it will always be used.                |
;; |-----------+------------------------+--------------------------------------------------|
;; | KEY-VALUE | :select t              | Select the popped up window. The                 |
;; |           |                        | `shackle-select-reused-windows' option makes     |
;; |           |                        | this the default for windows already             |
;; |           |                        | displaying the buffer.                           |
;; |-----------+------------------------+--------------------------------------------------|
;; |           | :inhibit-window-quit t | Special buffers usually have `q' bound to        |
;; |           |                        | `quit-window' which commonly buries the buffer   |
;; |           |                        | and deletes the window. This option inhibits the |
;; |           |                        | latter which is especially useful in combination |
;; |           |                        | with :same, but can also be used with other keys |
;; |           |                        | like :other as well.                             |
;; |-----------+------------------------+--------------------------------------------------|
;; |           | :ignore t              | Skip handling the display of the buffer in       |
;; |           |                        | question. Keep in mind that while this avoids    |
;; |           |                        | switching buffers, popping up windows and        |
;; |           |                        | displaying frames, it does not inhibit what may  |
;; |           |                        | have preceded this command, such as the          |
;; |           |                        | creation/update of the buffer to be displayed.   |
;; |-----------+------------------------+--------------------------------------------------|
;; |           | :same t                | Display buffer in the current window.            |
;; |           | :popup t               | Pop up a new window instead of displaying        |
;; |           | *mutually exclusive*   | the buffer in the current one.                   |
;; |-----------+------------------------+--------------------------------------------------|
;; |           | :other t               | Reuse the window `other-window' would select if  |
;; |           | *must not be used      | there's more than one window open, otherwise pop |
;; |           | with :align, :size*    | up a new window. When used in combination with   |
;; |           |                        | the :frame key, do the equivalent to             |
;; |           |                        | other-frame or a new frame                       |
;; |-----------+------------------------+--------------------------------------------------|
;; |           | :align                 | Align a new window at the respective side of     |
;; |           | 'above, 'below,        | the current frame or with the default alignment  |
;; |           | 'left, 'right,         | (customizable with `shackle-default-alignment')  |
;; |           | or t (default)         | by deleting every other window than the          |
;; |           |                        | currently selected one, then wait for the window |
;; |           |                        | to be "dealt" with. This can either happen by    |
;; |           |                        | burying its buffer with q or by deleting its     |
;; |           |                        | window with C-x 0.                               |
;; |           | :size                  | Aligned window use a default ratio of 0.5 to     |
;; |           | a floating point       | split up the original window in half             |
;; |           | value between 0 and 1  | (customizable with `shackle-default-size'), the  |
;; |           | is interpreted as a    | size can be changed on a per-case basis by       |
;; |           | ratio. An integer >=1  | providing a different floating point value like  |
;; |           | is interpreted as a    | 0.33 to make it occupy a third of the original   |
;; |           | number of lines.       | window's size.                                   |
;; |-----------+------------------------+--------------------------------------------------|
;; |           | :frame t               | Pop buffer to a frame instead of a window.       |
;; |-----------+------------------------+--------------------------------------------------|
;;
;; http://emacs.stackexchange.com/a/13687/115
;; Don't show Async Shell Command buffers





;; I always want "*Org Agenda*" on the side. I couldn't get
;; shackle to control the org-agenda window, so opting for this:
;; ref: https://emacs.stackexchange.com/a/2514/21777
(defadvice org-agenda (around split-vertically activate)
  (let ((split-width-threshold 80))  ; or whatever width makes sense for you
    ad-do-it))

(use-package popwin
  :hook after-init
  :config
  ;; M-x dired-jump-other-window
  ;; (push '(dired-mode :position top) popwin:special-display-config)


  ;; ;; M-x compile
  ;; (push '(compilation-mode :position bottom :noselect nil :height 10) popwin:special-display-config)
  ;; (push '(comint-mode :position bottom :noselect nil :height 10) popwin:special-display-config)
  ;; (push '("\\*term<.*>\\*" :regexp t :position bottom :select t :height 10) popwin:special-display-config)
  ;; (push '(term-mode :position :bottom :height 10 :stick t) popwin:special-display-config)

  ;; Console
   ;; (let ((rules '(
   ;;              ;; M-!
   ;;              ;; ("*Shell Command Output*" )

   ;;              ;; \*term<.*>\*
   ;;              ;; (term-mode :position bottom :select t :height 10)

   ;;               ("*shell*" :height 0.3)
   ;;               ("\\*ansi-term.*\\*" :regexp t :height 0.3)
   ;;               ("\\*terminal.*\\*" :regexp t :height 0.3)

   ;;               ))
   ;;   (mapcar* (lambda (c) (push c popwin:special-display-config)) rules))



   ;; (setq popwin:special-display-config rules)

  ;; popwin settings
  ;; (setq popwin:special-display-config '())
  (setq popwin:special-display-config
        '( ;;("*Help*" :height 0.4 :stick t)

          ;; TODO: try to enable. be sure there are no side-effects on helm or treemacs
          ;; ("\\*.*" :regexp t :position bottom :noselect t :height 0.3 :stick t)

          ;; Debug
          ("*Warnings*" :position bottom :height 0.3 )
          ("*Backtrace*" :position bottom :height 0.3 )
          ("*Messages*" :position bottom :height 0.3 )
          ("*Compile-Log*" :position bottom :height 0.3 )
          ("*Shell Command Output*" :position bottom :height 0.3 )
          (".*overtone.log" :regexp t :height 0.3)
          ("collected.org" :position top :height 15)
          (flycheck-error-list-mode :position bottom :height 0.3 :stick t)
          (compilation-mode :position bottom :height 0.3 :noselect t)
          ;; ;; Utils

          ;; IMPORTANT: popwin interferes with Treemacs when helm windows are called from it
          ;; ("helm" :regexp t :height 0.3)

          ("*Occur*" :position bottom :height 0.3)
          ("\\*Slime Description.*" :noselect t :regexp t :height 0.3)
          ("*undo-tree*" :width 0.3 :position right)
          ("*grep*" :position bottom :height 0.2 :stick t)
          ("*Completions*" :height 0.4)
          ("*compilation*" :height 0.4 :noselect t :stick t)
          ("*quickrun*" :height 0.3 :stick t)
          ;; ;; Magit/vc
          ;; (magit-status-mode :position bottom :noselect t :height 0.3 :stick t)
          ;; ("COMMIT_EDITMSG" :position bottom :noselect t :height 0.3 :stick t)
          ;; ("*magit-commit*" :position bottom :noselect t :height 0.3 :stick t)
          ;; ("\\*magit.*" :regexp t :position bottom :noselect t :height 0.3 :stick t)
          ;; ("*magit-diff*" :position bottom :noselect t :height 0.3)
          ;; ("*magit-edit-log*" :position bottom :noselect t :height 0.2)
          ;; ("*magit-process*" :position bottom :noselect t :height 0.2)
          ;; ("*vc-diff*" :position bottom :noselect t :height 0.2)
          ;; ("*vc-change-log*" :position bottom :noselect t :height 0.2)
          ;; ;; Navigator
          ;; ("*Ibuffer*" :position bottom :height 0.2)
          ;; ("*Ido Completions*" :noselect t :height 0.3)
          ;; ("*imenu-tree*" :position left :width 50 :stick t)

          ;; ;; (direx:direx-mode :position left :width 50 :dedicated t)

          ("*gists*" :height 0.3)
          ("*sldb.*":regexp t :height 0.3)
          ("*Gofmt Errors*" :noselect t)
          ("\\*godoc*" :regexp t :height 0.3)
          ("*nrepl-error*" :height 0.2 :stick t)
          ("*nrepl-doc*" :height 0.2 :stick t)
          ("*nrepl-src*" :height 0.2 :stick t)
          ("*Kill Ring*" :height 0.3)
          ("*project-status*" :noselect t)
          ("*Compile-Log" :height 0.2 :stick t)
          ("*pytest*" :noselect t)
          ;; Programing
          ("Django:" :regexp t :width 0.3 :position right)
          ("*Python*" :stick t)
          ("*jedi:doc*" :noselect t)
          ;; Console
          ("*shell*" :height 0.3)
          ("\\*ansi-term.*\\*" :regexp t :height 0.3)
          ("\\*terminal.*\\*" :regexp t :height 0.3)
          ("*MULTI-TERM-DEDICATED*" :position bottom :height 0.3 :stick t)
          (term-mode :position :bottom :height 10 :stick t)
          ;; Org/Organized
          (diary-fancy-display-mode :position left :width 50 :stick nil)
          (diary-mode :position bottom :height 15 :stick t)
          (calendar-mode :position bottom :height 15 :stick nil)
          (org-agenda-mode :position bottom :height 15 :stick t)
          ;; ("*Org Agenda.*\\*" :regexp t :position bottom :height 15 :stick t)
          )
        )

  (defun live-display-ansi ()
    (interactive)
    (popwin:display-buffer "*ansi-term*"))

  (defun popwin-term:ansi-term ()
    (interactive)
    (popwin:display-buffer-1
     (or (get-buffer-create "*ansi-term*")
         (save-window-excursion
           (ansi-term "/bin/bash")))
     :default-config-keywords '(:position :bottom :height 10 :stick t)))


  (defun popwin-term:multi-term ()
    (interactive)
    (popwin:display-buffer-1
     (or (get-buffer "*terminal*")
         (save-window-excursion
           (call-interactively 'multi-term)))
     :default-config-keywords '(:position :bottom :height 10 :stick t)))

  ;; ;; WIP - not working
  ;; (defun toggle-terminal ()
  ;;   (interactive)
  ;;   (let ((term-buf (get-buffer "*foo*")))
  ;;     (if term-buf

  ;;            (cond ((eq term-buf (window-buffer (selected-window)))
  ;;                   ;; (message "Visible and focused"))
  ;;                   (bury-buffer term-buf))
  ;;                  ((get-buffer-window term-buf)
  ;;                   ;; (message "Visible and unfocused"))
  ;;                   (bury-buffer term-buf))
  ;;                  (t
  ;;                   ;; (message "Not visible")
  ;;                   (popwin:display-buffer-1
  ;;                    term-buf
  ;;                    :default-config-keywords '(:position :bottom :height 10 :stick t)))
  ;;                  )

  ;;       (popwin:display-buffer-1
  ;;         (save-window-excursion
  ;;           (call-interactively 'multi-term)))
  ;;        :default-config-keywords '(:position :bottom :height 10 :stick t))))

  ;; (global-sqet-key (kbd "C-`") popwin:keymap)

  (popwin-mode 1)
  )

;; ;; Shell Pop
;; (use-package shell-pop
;;   :bind (("C-`" . (lambda ()
;;                     (interactive)
;;                     (if (fboundp 'vterm-posframe-toggle)
;;                         (vterm-posframe-toggle)
;;                       (shell-pop nil))))
;;          ([f9] . shell-pop))
;;   :init
;;   (setq shell-pop-window-size 30
;;         shell-pop-shell-type
;;         (cond ((fboundp 'vterm) '("vterm" "*vterm*" #'vterm))
;;               (sys/win32p '("eshell" "*eshell*" #'eshell))
;;               (t '("terminal" "*terminal*"
;;                    (lambda () (term shell-pop-term-shell)))))))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; WINDMOVE
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; using this for now
(windmove-default-keybindings)

;; (use-package windmove
;;   :ensure nil
;;   :straight nil
;;   :init
;;   (progn
;;    ;; NOTE: this is a mutually exclusive below alternative approaches, strong implementation.
;;    ;;  (defun ignore-error-wrapper (fn)
;;    ;;    "Funtion return new function that ignore errors.
;;    ;; The function wraps a function with `ignore-errors'"
;;    ;;    (let ((fn fn))
;;    ;;      (lambda ()
;;    ;;        (interactive)
;;    ;;        (ignore-errors
;;    ;;          (funcall fn)))))
;;    ;;  (when (fboundp 'windmove-default-keybindings)
;;    ;;    (progn
;;    ;;      (windmove-default-keybindings)
;;    ;;      (global-set-key [s-left] (ignore-error-wrapper 'windmove-left))
;;    ;;      (global-set-key [s-right] (ignore-error-wrapper 'windmove-right))
;;    ;;      (global-set-key [s-up] (ignore-error-wrapper 'windmove-up))
;;    ;;      (global-set-key [s-down] (ignore-error-wrapper 'windmove-down))))


;;     ;; NOTE: windmove conflicts with org-mode's defaults for the following:
;;     ;; ref: https://orgmode.org/manual/Conflicts.html

;;     ;; org-mode-map <S-down> -> (org-shiftdown &optional ARG)
;;     ;; org-mode-map <S-up>   -> (org-shiftup &optional ARG)
;;     ;; org-mode-map <S-left> -> (org-shiftleft &optional ARG)
;;     ;; org-mode-map <S-right> -> (org-shiftright &optional ARG)

;;     ;; instead use the other bindings in org-mode for these which are:
;;     ;; org-mode-map C-c <down>
;;     ;; org-mode-map C-c <up>
;;     ;; org-mode-map C-c <left>
;;     ;; org-mode-map C-c <right>

;;     ;; Make windmove work in Org mode:
;;     ;; (add-hook 'org-shiftup-final-hook 'windmove-up)
;;     ;; (add-hook 'org-shiftleft-final-hook 'windmove-left)
;;     ;; (add-hook 'org-shiftdown-final-hook 'windmove-down)
;;     ;; (add-hook 'org-shiftright-final-hook 'windmove-right)

;;     ;;; setting org-mode's shift-select behavior below

;;     ;; "Everywhere except timestamps
;;     ;; (setq org-support-shift-select 'always)

;;     ;; "When outside special context"
;;     ;; (setq org-support-shift-select t)

;;     ;; "Never"
;;     ;; (setq org-support-shift-select nil)
;;     ))


(with-eval-after-load 'hydra

  ;; resizing windows functions

  (defun hydra-move-splitter-left (arg)
    "Move window splitter left."
    (interactive "p")
    (if (let ((windmove-wrap-around))
          (windmove-find-other-window 'right))
        (shrink-window-horizontally arg)
      (enlarge-window-horizontally arg)))

  (defun hydra-move-splitter-right (arg)
    "Move window splitter right."
    (interactive "p")
    (if (let ((windmove-wrap-around))
          (windmove-find-other-window 'right))
        (enlarge-window-horizontally arg)
      (shrink-window-horizontally arg)))

  (defun hydra-move-splitter-up (arg)
    "Move window splitter up."
    (interactive "p")
    (if (let ((windmove-wrap-around))
          (windmove-find-other-window 'up))
        (enlarge-window arg)
      (shrink-window arg)))

  (defun hydra-move-splitter-down (arg)
    "Move window splitter down."
    (interactive "p")
    (if (let ((windmove-wrap-around))
          (windmove-find-other-window 'up))
        (shrink-window arg)
      (enlarge-window arg)))

  ;; window resizing bindings - vim style
  (defhydra hydra-splitter (global-map "C-M-s")
    "Splitter adjustment for current window"
    ("h" hydra-move-splitter-left "Shrink horizontally")
    ("j" hydra-move-splitter-down "Grow vertically")
    ("k" hydra-move-splitter-up "Shrink vertically")
    ("l" hydra-move-splitter-right "Grow horizontally"))

  ;; preferring basic C-x o right now
  ;; ;; better window management - holy mode style
  ;; (defhydra hydra-cycle-windows
  ;;   (global-map "C-x o")
  ;;   "Window Management"
  ;;   ("n" (other-window 1) "Next")
  ;;   ("p" (other-window -1) "Previous")
  ;;   ("]" enlarge-window-horizontally "Enlarge horizontal")
  ;;   ("[" shrink-window-horizontally "Shrink horizontal")
  ;;   ("=" enlarge-window "Enlarge vertival")
  ;;   ("-" shrink-window "Shrink vertical")
  ;;   ("b" balance-windows "Balance windows")
  ;;   ("c" delete-window "Close window")
  ;;   ("d" delete-other-windows "Maximize window")
  ;;   ("q" nil "quit"))
  )

;; TODO: reassign bindings?
;; ;; simple current window resizing
;; (use-package windresize
;;   :bind ("C-c r" . windresize))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; WINDSWAP
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; NOTE: same keys as windmove but with C modifier, e.g. C-S-<arrows>
;; NOTE: C-S will conflict with SHIFT held while jumping words for selection
(use-package windswap
  :config
  (progn
    (windswap-default-keybindings 'control 'shift)))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; SWITCH BUFFER IN WINDOW
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; TODO: have cycle within current group buffer tab group
;; NOTE: these bindins compete with  C-M-i and TAB for complete-at-point

;; option 1
;; (use-package buffer-flip
;;   :bind  (("M-<tab>" . buffer-flip)
;;           :map buffer-flip-map
;;           ( "M-<tab>" .   buffer-flip-forward)
;;           ( "M-S-<tab>" . buffer-flip-backward)
;;           ( "M-ESC" .     buffer-flip-abort))
;;   :config
;;   (setq buffer-flip-skip-patterns
;;         '("^\\*helm\\b"
;;           "^\\*swiper\\*$")))

;; option 2
;; (use-package nswbuff
;;   :after (projectile)
;;   :commands (nswbuff-switch-to-previous-buffer
;;              nswbuff-switch-to-next-buffer)
;;   :config
;;   (progn
;;     (my/global-map-and-set-key "C-TAB" 'nswbuff-switch-to-previous-buffer)
;;     (my/global-map-and-set-key "C-S-TAB" 'nswbuff-switch-to-next-buffer))
;;   :init
;;   (setq nswbuff-clear-delay 0 ; set this to 2 (seconds), if you want to see the status window
;;         nswbuff-recent-buffers-first t
;;         nswbuff-display-intermediate-buffers t
;;         nswbuff-exclude-buffer-regexps '("^ "
;;                                          "^\*.*\*"
;;                                          "^magit.*:.+")
;;         nswbuff-include-buffer-regexps '("^*Org Src")
;;         nswbuff-start-with-current-centered t
;;         nswbuff-buffer-list-function '(lambda ()
;;                                         (interactive)
;;                                         (if (projectile-project-p)
;;                                             (nswbuff-projectile-buffer-list)
;;                                           (buffer-list)))))



;; ACE
;; (use-package ace-window
;;   :ensure t
;;   :hook after-init
;;   :init (setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l)
;;               aw-char-position 'left
;;               aw-ignore-current nil
;;               aw-leading-char-style 'char
;;               aw-scope 'frame)
;;   :bind (("M-o" . ace-window)
;;          ("M-O" . ace-swap-window)))
(use-package ace-window
  :bind (("M-o" . shackra/other-window)
         ("M-O" . treemacs-select-window))
  :init
  (custom-set-faces
   '(aw-leading-char-face
     ((t (:inherit ace-jump-face-foreground :height 3.0)))))
  (defun --count-frames ()
    "Return the number of visible frames"
    (let* ((frames (if (daemonp) (butlast (visible-frame-list) 1) (visible-frame-list))))
      (length frames)))
  (defun shackra/other-window ()
    "Change the cursor's focus to another window"
    (interactive)
    (if (or (> (count-windows) 2) (> (--count-frames) 1))
        (ace-window 1)
      (ace-window 0)))
  :config
  (setf aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l)))

(provide 'pkg-emacs-windows)
