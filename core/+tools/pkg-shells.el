


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; TERMINAL - MULTI-TERM
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; zsh and multi-term: http://rawsyntax.com/blog/learn-emacs-zsh-and-multi-term/
(use-package multi-term
  ;; :bind (;; ("C-c t" . multi-term)
  ;;        ;; ("C-c \"" . multi-term-dedicated-toggle)
  ;;        ;; ("C-`" . multi-term-dedicated-toggle) ;; key probably won't work in non-gui
  ;;        )
  :config
  (setq multi-term-program (getenv "SHELL")
        multi-term-buffer-name "term"
        multi-term-dedicated-select-after-open-p t
        multi-term-dedicated-window-height 15)
  (setenv "ZSH_THEME" "simple")
  (setenv "TERM" "xterm-256color")
  (add-hook 'term-mode-hook
            (lambda ()
              ;; scroll an entire screen

              ;; page-UP/page-DOWN (prior/next) to scroll an entire screen
              (add-to-list 'term-bind-key-alist '("<next>" . scroll-up))
              (add-to-list 'term-bind-key-alist '("<prior>" . scroll-down))


              ;; scroll just 1 line

              ;; binding C-<up>/C-<down> to scroll 1 line in terminals
              (add-to-list 'term-bind-key-alist '("C-<down>" . scroll-up-line))
              (add-to-list 'term-bind-key-alist '("C-<up>". scroll-down-line))

              ;; also binding M-<up>/M-<down> (M == ESC in MacOS) to scroll 1 line in terminals
              (add-to-list 'term-bind-key-alist '("ESC <down>" . scroll-up-line))
              (add-to-list 'term-bind-key-alist '("ESC <up>" . scroll-down-line))

              ;; also binding M-<up>/M-<down> explicitly for tmux sessions to scroll 1 line
              (add-to-list 'term-bind-key-alist '("M-<down>" . scroll-up-line))
              (add-to-list 'term-bind-key-alist '("M-<up>". scroll-down-line))

              ;; TODO: in tmux mouse scrolling works as expected, get it working in native

              ;; conflict with yasnippet
              (yas-minor-mode -1)
              (company-mode -1)

	            ))
  (add-hook 'term-mode-hook
            (lambda ()
              (define-key term-raw-map (kbd "C-y") 'term-paste)))
  (add-hook 'term-mode-hook
            (lambda ()
              (setq term-buffer-maximum-size 10000))))

;; NOTE: this is a constant in the multi-term.el, how do I set it here?
;; (setq multi-term-dedicated-buffer-name "terminal")


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; ESHELL CONFIG
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(setq eshell-hist-ignoredups t)
(setq eshell-cmpl-cycle-completions nil)
(setq eshell-cmpl-ignore-case t)
(setq eshell-ask-to-save-history (quote always))
(setq eshell-prompt-regexp "^[^#$\n]*[#$] "
      eshell-prompt-function
      (lambda nil
        (concat
	       "[" (user-login-name) "@" (system-name) " "
	       (if (string= (eshell/pwd) (getenv "HOME"))
	           "~" (eshell/basename (eshell/pwd)))
	       "]"
	       (if (= (user-uid) 0) "# " "$ "))))
(add-hook 'eshell-mode-hook
          '(lambda ()
             (progn
               (define-key eshell-mode-map "\C-a" 'eshell-bol)
               (define-key eshell-mode-map "\C-r" 'counsel-esh-history)
               (define-key eshell-mode-map [up] 'previous-line)
               (define-key eshell-mode-map [down] 'next-line)
               )))

(lock-buffer)

;; Locked mode
(global-set-key (kbd "C-c C-l") 'locked-buffer-mode)

(define-minor-mode locked-buffer-mode
  "Make the current window always display this buffer."
  nil " locked" nil
  (set-window-dedicated-p (selected-window) locked-buffer-mode))



;; (use-package terminal-toggle
;;   :bind (("C-`" . terminal-toggle)) ;; key probably won't work in non-gui
;;   :init
;;   (setq
;;         terminal-toggle--term-command "ansi-term"
;;         terminal-toggle--term-shell "zsh"))


;; (use-package term-toggle
;;   :straight (emacs-term-toggle :type git :host github :repo "amno1/emacs-term-toggle")
;;   :init
;;   (setq term-toggle-no-confirm-exit t
;;         term-toggle-kill-buffer-on-term-exit t))






(provide 'pkg-shells)
