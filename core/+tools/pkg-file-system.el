;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; NEOTREE
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; FIXME: neotree vc or somthing in init is overriding color

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	Sidebar for emacs
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(use-package all-the-icons)
(use-package neotree
  :bind
  (("<f2>" . neotree-dir)
   ("<f8>" . neotree-toggle))
  :init
  (progn
    (defun neotree-project-dir ()
      "Open NeoTree using the git root."
      (interactive)
      (let ((project-dir (projectile-project-root))
            (file-name (buffer-file-name)))
        (neotree-toggle)
        (if project-dir
            (if (neo-global--window-exists-p)
                (progn
                  (neotree-dir project-dir)
                  (neotree-find file-name)))
          (neotree-toggle))))

    (setq neo-show-updir-line t)
    (setq neo-show-hidden-files t)
    ;; (setq neo-click-changes-root t)
    (setq neo-hide-cursor t)
    ;; every time when the neotree window is opened, it will try to find current
    ;; file and jump to node.
    (setq-default neo-smart-open t)

    (setq neo-smart-open t)
    (setq neo-theme (if (display-graphic-p) 'icons 'nerd)) ; 'classic, 'nerd, 'ascii, 'arrow

    ;; (setq-default neo-show-hidden-files nil)

    ;; set to desired width
    (setq neo-window-width 40)
    ;; (setq neo-window-fixed-size nil)

    ;; automatically indent to arrow of nerd theme
    (custom-set-variables
     '(neo-auto-indent-point t))

    ;; enable source control state tracking
    (progn
      (setq neo-vc-integration '(face char))

      ;; patch to fix vc integration
      (defun neo-vc-for-node (node)
        (let* ((backend (vc-backend node))
        	     (vc-state (when backend (vc-state node backend))))
          ;; (message "%s %s %s" node backend vc-state)
          ;; set faces for states
          (cons (cdr (assoc vc-state neo-vc-state-char-alist))
        	      (cl-case vc-state
        	        (up-to-date       neo-vc-up-to-date-face)
        	        (edited           neo-vc-edited-face)
        	        (needs-update     neo-vc-needs-update-face)
        	        (needs-merge      neo-vc-needs-merge-face)
        	        (unlocked-changes neo-vc-unlocked-changes-face)
        	        (added            neo-vc-added-face)
        	        (removed          neo-vc-removed-face)
        	        (conflict         neo-vc-conflict-face)
        	        (missing          neo-vc-missing-face)
        	        (ignored          neo-vc-ignored-face)
        	        (unregistered     neo-vc-unregistered-face)
        	        (user             neo-vc-user-face)
        	        (t                neo-vc-default-face))))))))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; TREEMACS
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(use-package treemacs
  :init
  (with-eval-after-load 'winum
    (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :config
  (progn
    (setq treemacs-be-evil                       nil
          treemacs-collapse-dirs                 (if (executable-find "python3") 3 0)
          treemacs-deferred-git-apply-delay      0.2
          treemacs-display-in-side-window        t ;; uses Emac's system of sidewindow show/hide if enabled
          treemacs-eldoc-display                 t
          treemacs-file-event-delay              1000
          treemacs-max-git-entries               10000
          treemacs-file-follow-delay             10 ;; seconds - slowed down
          treemacs-follow-after-init             t
          treemacs-follow-recenter-distance      0.5
          treemacs-git-command-pipe              ""
          treemacs-goto-tag-strategy             'refetch-index
          treemacs-indentation                   2
          treemacs-indentation-string            " "
          treemacs-is-never-other-window         nil
          treemacs-max-git-entries               5000
          treemacs-missing-project-action        'ask
          treemacs-no-png-images                 nil
          treemacs-no-delete-other-windows       t
          treemacs-project-follow-cleanup        nil
          treemacs-persist-file                  (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
          treemacs-recenter-distance             0.5
          treemacs-recenter-after-file-follow    'on-visibility
          treemacs-recenter-after-tag-follow     'always
          treemacs-recenter-after-project-jump   'always
          treemacs-recenter-after-project-expand 'always
          treemacs-show-cursor                   nil
          treemacs-show-hidden-files             t
          treemacs-silent-filewatch              t
          treemacs-silent-refresh                t
          treemacs-sorting                       'alphabetic-asc ; 'alphabetic-desc
          treemacs-space-between-root-nodes      t
          treemacs-tag-follow-cleanup            t
          treemacs-tag-follow-delay              0.2
          treemacs-width                         28)

    ;; The default width and height of the default treemacs icons is 22 pixels. If you are
    ;; using a Hi-DPI display, uncomment this to double the icon size.
    ;; (treemacs-resize-icons 16)

    ;; TODO: doesn't seem to support regex. check out.
    ;; (defvar treemacs-custom-dockerfile-icon (all-the-icons-icon-for-file "Dockerfile"))
    ;; (treemacs-define-custom-icon treemacs-custom-dockerfile-icon "Dockerfile.*\\'")


    ;; (treemacs-follow-mode t)
    (treemacs-follow-mode 'tag)
    (treemacs-filewatch-mode t)

    (setq treemacs-use-scope-type 'Perspectives
          treemacs-use-git-mode 'extended
          treemacs-lock-width nil)

    (treemacs-fringe-indicator-mode t)
    (pcase (cons (not (null (executable-find "git")))
                 (not (null (executable-find "python3"))))
      (`(t . t)
       (treemacs-git-mode 'deferred))
      (`(t . _)
       (treemacs-git-mode 'simple)))

    ;; define a function that can turn off fixed width on demand
    ;; FIXME: requires M-x which-key-mode (to be toggled off becuase of bug)
    ;; should be fixed here: https://github.com/Alexander-Miller/treemacs/issues/376
    ;; (defun treemacs-resize-window-unlock ()
    ;;   (interactive)
    ;;   (with-selected-window (treemacs-get-local-window)
    ;;     (progn
    ;;       ;; we have to disable/reenable which-key to do this trick.
    ;;       (if (and (bound-and-true-p which-key-mode) which-key-mode)
    ;;           (progn
    ;;             (which-key-mode -1)
    ;;             (setq treemacs--width-is-locked nil
    ;;                   window-size-fixed nil)
    ;;             (which-key-mode 1))
    ;;         (setq treemacs--width-is-locked nil
    ;;               window-size-fixed nil)
    ;;       )))
    ;; ;; now add the advice that unlocks the treemacs fixed window size
    ;; (advice-add 'treemacs :after (lambda (&optional _arg _pred) (treemacs-resize-window-unlock)))


    ;; advise treemacs from the outside to hide it's cursor
    ;; TODO: create a PR for hide/show cursor
    ;; 1. move this internally into treemacs-mode.el & read from 'treemacs-show-cursor
    ;; 2. make a toggle to show/hide cursor on demand from anywhere targeting the window
    ;; (progn
    ;;   (defun maybe-hide-treemacs-cursor (&optional _arg pred)
    ;;     (interactive)
    ;;     (move-beginning-of-line 1)
    ;;     (when (and (not treemacs-show-cursor)
    ;;                (string-match-p "\\*Treemacs-.+" (buffer-name)))
    ;;       (internal-show-cursor (get-buffer-window) nil)))

    ;;   (advice-add 'treemacs :after 'maybe-hide-treemacs-cursor))

    ;; allow normal page up/down behavior in treemacs window
    (define-key treemacs-mode-map (kbd "<prior>")   #'scroll-down-command)
    (define-key treemacs-mode-map (kbd "<next>")   #'scroll-up-command)

    ;; single click to expand/collapse
    ;; (define-key treemacs-mode-map [mouse-1] #'treemacs-single-click-expand-action)




    ;; set fonts in tui fallback icons to my liking
    ;; NOTE: these are originally defined in treemacs-icons.el
    (when *is-in-terminal*
      (treemacs-create-icon :file "dir-closed.png"  :extensions (dir-closed) :fallback (propertize "▸ " 'face 'treemacs-term-node-face))
      (treemacs-create-icon :file "dir-open.png"    :extensions (dir-open)   :fallback (propertize "▾ " 'face 'treemacs-term-node-face))
      (treemacs-create-icon :file "tags-leaf.png"   :extensions (tag-leaf)   :fallback (propertize "• " 'face 'font-lock-constant-face))
      (treemacs-create-icon :file "tags-open.png"   :extensions (tag-open)   :fallback (propertize "▸ " 'face 'font-lock-string-face))
      (treemacs-create-icon :file "tags-closed.png" :extensions (tag-closed) :fallback (propertize "▾ " 'face 'font-lock-string-face)))

    ;; setup any custom icons here
    (defvar treemacs-custom-dockerfile-icon (all-the-icons-icon-for-file "Dockerfile"))
    (treemacs-define-custom-icon treemacs-custom-dockerfile-icon
                                 "DockerfileLocal"
                                 "DockerfileLocalCloud"
                                 "DockerfileWorker"
                                 "DockerfileWorkerLocal"
                                 "DockerfileWorkerLocalCloud")) ;; wildcards don't work only explicit, usually only file extensions
  :bind
  (:map global-map
        ("C-x f"       . treemacs-select-window)
        ;; ([f9]        . treemacs)
        ("C-x t t"     . treemacs)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ("C-x t B"   . treemacs-bookmark)
        ("C-x t C-t" . treemacs-find-file)
        ("C-x t M-t" . treemacs-find-tag)))


(use-package treemacs-projectile
  :after (treemacs projectile))

;; TODO: renenable when this project support all-the-icons
;; (use-package treemacs-icons-dired
;;   :after (treemacs dired)
;;   :config (treemacs-icons-dired-mode))

(use-package treemacs-magit
  :after (treemacs magit))



;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; DIRED
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; NOTE: we use ranger in place of dired
(use-package dired
  :ensure nil
  :straight nil
  :config
  ;; dired - reuse current buffer by pressing 'a'
  (put 'dired-find-alternate-file 'disabled nil)

  ;; always delete and copy recursively
  (setq dired-recursive-deletes 'always)
  (setq dired-recursive-copies 'always)

  ;; if there is a dired buffer displayed in the next window, use its
  ;; current subdir, instead of the current subdir of this dired buffer
  (setq dired-dwim-target t)

  ;; enable some really cool extensions like C-x C-j (dired-jump)
  (require 'dired-x))


;; dired+ adds some features to standard dired (like reusing buffers)

(use-package dired+
  ;; :quelpa (dired+ :fetcher url :url "https://www.emacswiki.org/emacs/download/dired+.el")
  ;; :quelpa (dired+ :fetcher wiki)
  ;; :quelpa (dired+ :fetcher github :repo "emacsmirror/dired-plus")
  :straight (dired+ :type git :host github :repo "emacsmirror/dired-plus")
  :init
  (setq diredp-hide-details-initially-flag nil)
  (setq diredp-hide-details-propagate-flag nil)
  :config
  (diredp-toggle-find-file-reuse-dir 1)
  ;; This binding messes with the arrow keys, for some reason.
  (unbind-key "M-O" dired-mode-map))


;; ;;preview files in dired
;; (use-package peep-dired
;;   :disabled t
;;   ;; don't access `dired-mode-map' until `peep-dired' is loaded
;;   :bind (:map dired-mode-map
;;               ("P" . peep-dired))
;;   :config
;;   (setq peep-dired-enable-on-directories t
;;         peep-dired-cleanup-on-disable t))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; RANGER
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; ranger
;; NOTE: keep this section commented because causes weirdness with dired+
;; ;; https://github.com/ralesi/ranger.el#standard-ranger-bindings
;; (use-package ranger
;;   :config
;;   (progn
;;     (define-key ranger-normal-mode-map (kbd "+") #'dired-create-directory)

;;     ;;  (add-to-list 'ranger-prefer-regexp "^\\.DS_Store$")
;;     (defcustom ranger-hidden-filter
;;       "^\.|\.(?:pyc|pyo|DS_Store|bak|swp)$|^lost\+found$|^__(py)?cache__$"
;;       "Regexp of filenames to hide."
;;       :group 'ranger
;;       :type 'string)
;;     (setq ranger-max-preview-size 10)
;;     (setq ranger-excluded-extensions '("dat" "p" "iso" "pb" "fb"))
;;     (setq ranger-dont-show-binary t)
;;     (setq ranger-show-hidden t)
;;     (setq ranger-preview-file t)
;;     (setq ranger-dont-show-binary t)
;;     (setq ranger-show-literal nil)
;;     ;; (setq ranger-cleanup-on-disable t)
;;     (setq ranger-cleanup-eagerly t)
;;     (setq ranger-dont-show-binary t)

;;     ;; ;; default ranger over dired
;;     ;; (setq ranger-override-dired 'ranger)
;;     ;; (ranger-override-dired-mode t)

;;     ;; allow dired to stand without ranger
;;     (setq ranger-override-dired nil)
;;     (ranger-override-dired-mode nil)
;;     ))

(provide 'pkg-file-system)
