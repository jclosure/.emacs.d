;;; SYSTEM  CLIPBOARD  CONFIG

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; USE MACOS PASTBOARD
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; Use macOS clipboard
;; (use-package pbcopy
;;   ;; :hook (after-init . (turn-on-pbcopy))
;;   :if (eq system-type 'darwin)
;;   :config
;;   (turn-on-pbcopy))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; USE MACOS OR LINUX OR SSH-BRIDGED CLIPBOARD AND *copy-log* buffer
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; TODO: turn copy-log into a persisted file as option
;; TODO: implement case below for linux linux support, same as mac using xclip

;; prints copy/paste data into *Messages* buffer
(setq *debug-clipboard-io* t)

;; variables ref: https://with-emacs.com/posts/tutorials/almost-all-you-need-to-know-about-variables/
;; set this var upstream if you want to debug, it will override this default
(defvar *debug-clipboard-io* nil)

(defun write-to-clipboard-log-maybe (&rest args)
  (when *debug-clipboard-io*
    (apply #'write-to-clipboard-log args)))

(cl-defun write-to-clipboard-log (text &optional (header nil) &optional (footer nil))
  (interactive "sEnter the text: ")
  (let ((log-buffer-name "*copy-log*")
        (working-buffer (buffer-name)))
    (setq log-buffer (get-buffer-create log-buffer-name))
    (with-current-buffer log-buffer-name ; replace with the name of the buffer you want to append
      (goto-char (point-max))
      ;; maybe instead: (format-time-string "%Y-%m-%d %H:%M:%S")
      (let ((delimited (concat "--- " "%s" " in " working-buffer  ": "  (current-time-string)  " ---\n")))
        (when header
          (insert (propertize (format delimited header) 'face '(:foreground "purple" :weight bold))))
        (insert (format "%s\n" text))
        (when footer
          (insert (propertize (format delimited footer) 'face '(:inverse-video t))))))))

;; TODO: implement as a macro for better performance
(defun get-current-func-name ()
  "Get the symbol of the function this function is called from."
  ;; 5 is the magic number that makes us look
  ;; above this function
  (let* ((index 5)
         (frame (backtrace-frame index)))
    ;; from what I can tell, top level function call frames
    ;; start with t and the second value is the symbol of the function
    (while (not (equal t (first frame)))
      (setq frame (backtrace-frame (incf index))))
    (second frame)))

;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Clipboard.html
;; (setq save-interprogram-paste-before-kill t)

;; NOW WIRE THE CONFIGS UP BASED ON RUN ENVIRONMENT

;; NOTE: this assumes local macos not remotely run ssh, although does support tramp
(when *is-a-mac*
  (if *is-in-tmux*
      (progn
        (message "*********************** USING LOCAL MACOS TMUX CLIPBOARD ****************************")

        "When we are running under macos in tmux reattach-to-user-namespace.
         be sure to: brew install reattach-to-user-namespace"

        (defun copy-from-osx ()
          "Use OSX clipboard to paste. PULL"
          (let ((text (shell-command-to-string "reattach-to-user-namespace pbpaste")))
            (write-to-clipboard-log-maybe text (get-current-func-name))
            text))

        (defun paste-to-osx (text &optional push)
          "Add kill ring entries (TEXT) to OSX clipboard.  PUSH."
          (let ((process-connection-type nil))
            (let ((proc (start-process "pbcopy" "*Messages*" "reattach-to-user-namespace" "pbcopy")))
              (write-to-clipboard-log-maybe text (get-current-func-name))
              (process-send-string proc text)
              (process-send-eof proc))))

        (setq interprogram-cut-function 'paste-to-osx)
        (setq interprogram-paste-function 'copy-from-osx))


    (progn
      (message "*********************** USING LOCAL MACOS NATIVE CLIPBOARD ****************************")

      "When we are running in native macos"

      (defun copy-from-osx ()
        "Use OSX clipboard to paste. PULL"
        (let ((text (shell-command-to-string "pbpaste")))
          (write-to-clipboard-log-maybe text (get-current-func-name))
          text))

      (defun paste-to-osx (text &optional push)
        "Add kill ring entries (TEXT) to OSX clipboard.  PUSH."
        (let ((process-connection-type nil))
          (let ((proc (start-process "pbcopy" "*Messages*" "pbcopy")))
            (write-to-clipboard-log-maybe text (get-current-func-name))
            (process-send-string proc text)
            (process-send-eof proc))))

      (setq interprogram-cut-function 'paste-to-osx)
      (setq interprogram-paste-function 'copy-from-osx))))


;; NOTE: IMPORTANT!!! this assumes you are using ssh
;; with bi-directional clipboard port forwarding already
;; established. otherwise, you probably want to disable this.
;; a recipe for this on mac (and linux) can be found here:
;; https://gist.github.com/jclosure/19698429dda1105b8a93b0832c07ebc7
(when (and *is-a-linux* *is-in-terminal* *is-in-ssh*)
  (progn
    (message "*********************** USING REMOTE LINUX TERMINAL SSH CLIPBOARD ****************************")
    "We are running in a remote linux terminal via an ssh connection"

    (defun is-port-listening (host port)
      "Tests if a tcp port is open"
      (interactive)
      (string-match-p "succeeded!"
                      (shell-command-to-string (format "nc -zv %s %d" host port))))

    (defun copy-from-remote ()
      "Use netcat clipboard to paste. PULL"
      (let ((text (shell-command-to-string "nc localhost 2225")))
        (write-to-clipboard-log-maybe text (get-current-func-name))
        text))

    (defun paste-to-remote (text &optional push)
      "Add kill ring entries (TEXT) to netcat clipboard.  PUSH."
      (let ((process-connection-type nil))
        (let ((proc (start-process "remote-clipboard-copy" "*Messages*" "nc" "localhost" "2224")))
          (write-to-clipboard-log-maybe text (get-current-func-name))
          (process-send-string proc text)
          (process-send-eof proc))))

    ;; if the reverse forarding is done we'll use the ports
    (if (is-port-listening "localhost" 2225)
        (progn
          (message "remote clipboard listening")
          (setq interprogram-cut-function 'paste-to-remote)
          (setq interprogram-paste-function 'copy-from-remote))
      (message "remote clipboard is not listening. skipping..."))))

(provide 'pkg-clipboard)
