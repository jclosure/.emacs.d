(require 'pkg-packages)
(require 'pkg-file-system)
(require 'pkg-regex)
(require 'pkg-clipboard)
(require 'pkg-markers)
(require 'pkg-shells)
(require 'pkg-bin)
(require 'pkg-infra)
(require 'pkg-emacs-windows)
;; (require 'pkg-plantuml)

;; only load these manually when nec for debugging
(require 'pkg-debugging)

(require 'pkg-help)

(provide 'tools)
