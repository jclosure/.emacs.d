;; ;; recenters on M-,
;; ;; NOTE: C-l by default will toggle between middle, top, bottom

;; solution 1: after advise the xref-pop-marker-stack fn
;; (defun my-center-in-window ()
;;   (let ((recenter-positions '(middle)))
;;         (recenter-top-bottom)))
;; (advice-add 'xref-pop-marker-stack :after #'my-center-in-window)

;; solution 2: use the builtin hook xref-after-return-hook
(defun my-center-in-window-hook ()
  (let ((recenter-positions '(middle)))
    (recenter-top-bottom)))
(add-hook 'xref-after-return-hook 'my-center-in-window-hook)


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; POINT HISTORY: C-\
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(use-package popwin)

;; TODO: try to get these so not require demand

;; manual fetch: (quelpa '(point-history :fetcher github :repo "blue0513/point-history"))
(use-package point-history
  :straight (point-history :type git :host github :repo "blue0513/point-history")
  :init
  (setq point-history-save-timer 5
        point-history-ignore-buffer "^ \\*Minibuf\\|^ \\*point-history-show*"
        point-history-should-preview t)

  :config
  (progn

    ;; (add-hook 'after-init-hook (lambda ()
    (point-history-mode t)

    ;; global toggle
    ;; (global-set-key [(control ?\\)] 'point-history-show)

    ;; avoid tracking point in certain modes
    ;; (setq point-history-ignore-major-mode '(emacs-lisp-mode ruby-mode))

    ;; mode map key bindings
    (define-key point-history-show-mode-map (kbd "q") 'point-history-close)

    (define-key point-history-show-mode-map (kbd "n") 'point-history-next-line)
    (define-key point-history-show-mode-map (kbd "p") 'point-history-prev-line)
    ))


;; NOTE: requires point-history to be loaded by itself
;; manual fetch: (quelpa '(ivy-point-history :fetcher github :repo "SuzumiyaAoba/ivy-point-history"))
(use-package ivy-point-history
  :bind (("C-\\" . ivy-point-history))
  :straight (ivy-point-history :type git :host github :repo "SuzumiyaAoba/ivy-point-history"))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; CHANGE NAV
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; TODO: try idle-timer-based point-ring https://qiita.com/zk_phi/items/c145b7bd8077b8a0f537
;; XOR
;; use change-bashed point-ring w/ jump back/forward
;; navigate changes in buffer forward and backward
(use-package goto-chg
  :bind (("M--" . goto-last-change)
         ("M-=" . goto-last-change-reverse)))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; MARK RING NAV
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; NOTE: both helm and ivy have this already

;; ;; nav marks easily - need to map term chars below
;; (use-package backward-forward
;;   ;; :bind
;;   ;; ("C-," . backward-forward-previous-location)
;;   ;; ("C-." . backward-forward-next-location)
;;   :custom
;;   (mark-ring-max 60)
;;   :config
;;   (backward-forward-mode t))


(provide 'pkg-markers)
