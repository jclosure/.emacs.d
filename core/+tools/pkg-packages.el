;;; Code:

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; MACOS-SPECIFIC FEATURES
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; Reveal in finder for current bufferz
(use-package reveal-in-osx-finder
  :bind ("C-c z" . reveal-in-osx-finder)
  :if (eq system-type 'darwin))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; GENERATE QR CODES (gui only)
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; install qrencode
;; brew install qrencode
;; sudo apt-get install qrencode
(defun qr-code-region (start end)
  "Show a QR code of the region.
A simple way to transfer text to the phone..."
  (interactive "r")
  (let ((buf (get-buffer-create "*QR*"))
	      (inhibit-read-only t))
    (with-current-buffer buf
      (erase-buffer))
    (let ((coding-system-for-read 'raw-text))
      (shell-command-on-region start end "qrencode -o -" buf))
    (switch-to-buffer buf)
    (image-mode)))



;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; QUICK RUN ANY COMMAND IN ANY LANGUAGE
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; https://github.com/emacsorphanage/quickrun
(use-package quickrun
  :commands (quickrun
             quickrun-region
             quickrun-with-arg
             quickrun-shell
             quickrun-compile-only
             quickrun-replace-region))



;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; MOUSE CONFIG (move to system.el or input.el)
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(require 'mouse) ;; needed for iterm2 compatibility
(xterm-mouse-mode t)
(global-set-key (kbd "C-x m") 'xterm-mouse-mode)
;; mouse scrolling with xterm mouse
(global-set-key [mouse-4] '(lambda ()
                             (interactive)
                             (scroll-down 1)))
(global-set-key [mouse-5] '(lambda ()
                             (interactive)
                             (scroll-up 1)))
(setq mouse-sel-mode t)
(defun track-mouse (e))

;;	Disable the mouse in Emacs
;;	https://github.com/purcell/disable-mouse
;; (use-package disable-mouse
;;   :config (global-disable-mouse-mode))
;; MOUSE CONFIG (probably delete)

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; UNDO (see ergo-keys for bindings)
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(use-package undo-tree
  :defer 5
  :commands undo-tree-mode
  :diminish undo-tree-mode
  :config
  (progn
    (custom-set-variables
     '(undo-tree-visualizer-timestamps t)
     '(undo-tree-visualizer-diff t))
    (defalias 'redo 'undo-tree-redo)
    (global-undo-tree-mode 1)))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	Show key bind for currently entered incomplete command
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(use-package which-key
  :commands which-key-mode
  :config
  (progn
    ;; (which-key-mode 1)
    (which-key-setup-side-window-bottom)))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	Show recent file
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(use-package recentf
  :config
  (progn
    (setq recentf-max-saved-items 200
	  recentf-max-menu-items 15)
    (recentf-mode)
    ))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	Show line number of current coding window
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; (use-package linum
;;   :init
;;   (progn
;;     (global-linum-mode t)
;;     (setq linum-format "%4d  ")
;;     ))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	Auto pair when input
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; (use-package autopair
;;   :config (autopair-global-mode))



;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	PROJECT/PROJECTILE
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(use-package projectile
  ;; because tabbar loads after - speed up
  ;; :delight '(:eval (concat " " (projectile-project-name)))
  ;; :after persp-mode ; load only after persp-mode to handle race cond on bindings
  :demand t
  :config
  (progn

    (projectile-global-mode 1)

    ;; NOTE: moved to helm section, since controlling it there
    ;; (projectile-switch-project-action 'projectile-find-file)
    ;; (setq projectile-switch-project-action 'projectile-find-dir)

    ;; TODO: reorder these to not use eval for speed
    (with-eval-after-load 'ivy
      '(ivy-format-functions-alist))

    ;; (add-hook 'prog-mode-hook 'projectile-mode)
    (setq projectile-enable-caching t)
    ;; (setq projectile-completion-system 'ivy)

    ;; (setq projectile-switch-project-action 'neotree-projectile-action)
    ;; search superordinate dirs until root
    ;; (setq projectile-find-dir-includes-top-level t)
    (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

    ;; use ag instread of grep for helm-projectile-grep (REQUIRES AG)
    ;; (define-advice helm-projectile-grep (:override (&optional dir) ag)
    ;;   (helm-do-ag (or dir (projectile-project-root))))

    ;; globally ignore certain directory and file patterns

    (add-to-list 'projectile-globally-ignored-directories "**/_build/")
    (add-to-list 'projectile-globally-ignored-directories "**/deps/")
    (add-to-list 'projectile-globally-ignored-directories "**/node_modules/")
    (add-to-list 'projectile-globally-ignored-directories "**/thirdparty/")
    (add-to-list 'projectile-globally-ignored-directories "**/build/")
    (add-to-list 'projectile-globally-ignored-directories "**/log/")
    (add-to-list 'projectile-globally-ignored-directories "**/logs/")

    (add-to-list 'projectile-globally-ignored-files "*.min.js")

    ;; ignore certain directory and file patterns during grepping

    (add-to-list 'grep-find-ignored-directories "**/_build/")
    (add-to-list 'grep-find-ignored-directories "**/deps/")
    (add-to-list 'grep-find-ignored-directories "**/node_modules/")
    (add-to-list 'grep-find-ignored-directories "**/build/")
    (add-to-list 'grep-find-ignored-directories "**/thirdparty/")
    (add-to-list 'grep-find-ignored-directories "**/log/")
    (add-to-list 'grep-find-ignored-directories "**/logs/")
    (add-to-list 'grep-find-ignored-files "**/*.log")
    ))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	Git tool for emacs
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;; GIT

(use-package gitconfig-mode)
(use-package gitignore-mode)
(use-package gitattributes-mode)
(use-package git-link)

;; requires github acct
(use-package gist)

(use-package git-timemachine
  :bind ("M-g t" . git-timemachine-toggle))


;; Show a small popup with the blame for the current line only.
;; https://github.com/syohex/emacs-git-messenger
(use-package git-messenger
  ;; :bind ("C-x v p" . git-messenger:popup-message)
  :bind (:map vc-prefix-map
         ("p" . git-messenger:popup-message))
  :init
  (setq git-messenger:show-detail t
        git-messenger:use-magit-popup t)
  :config
  (progn
    (define-key git-messenger-map (kbd "RET") 'git-messenger:popup-close)))

(use-package magit-filenotify
  :after (magit)
  :config
  (progn
    (when (fboundp 'file-notify-add-watch)
      ;; NOTE: in the magit :config we disable this when killing magit buffers
      (add-hook 'magit-status-mode-hook 'magit-filenotify-mode))))


;; TODO: get gud by reading this: http://endlessparentheses.com/it-s-magit-and-you-re-the-magician.html
(use-package magit
  :bind ("C-x g" . magit-status)
  :config (progn
     (defun run-projectile-invalidate-cache (&rest _args)
       ;; We ignore the args to `magit-checkout'.
       (projectile-invalidate-cache nil))
     (advice-add 'magit-checkout
       :after #'run-projectile-invalidate-cache)
     (advice-add 'magit-branch-and-checkout ; This is `b c'.
       :after #'run-projectile-invalidate-cache)
     (setq magit-completing-read-function 'ivy-completing-read)

     ;; clean up all magit buffers after using it
     (defun my-magit-kill-buffers ()
       "Restore window configuration and kill all Magit buffers."
       (interactive)
       (let ((buffers (magit-mode-get-buffers)))
         (magit-filenotify-mode -1) ; turn off magit-filenotify before closing the buffer
         (magit-restore-window-configuration)
         (mapc #'kill-buffer buffers)))

     (bind-key "q" #'my-magit-kill-buffers magit-status-mode-map)

     ;; possible performance issues with this hook.
     ;; be on the lookout with magit for performance issues
     (add-hook 'after-save-hook 'magit-after-save-refresh-status t)

     ;; GERRIT SUPPORT
     ;; gerrit extension to push to frequently used refspec
     (defun magit-push-to-gerrit ()
       (interactive)
       (magit-push-refspecs "origin" "HEAD:refs/for/master" '()))

     ;; add a menu item transient to the push context menu
     (transient-append-suffix
      'magit-push
      "m"
      '("!" "Push to Gerrit" magit-push-to-gerrit))

     ;; UPDATE SUBMODULES SUPPORT
     ;; git submodule update --init --recursive
     (defun magit-submodule-update-all ()
       "Update all submodules"
       :description "Update All (git submodule update --init --recursive)"
       (interactive)
       (magit-with-toplevel
         (magit-run-git-async "submodule" "update" "--init" "--recursive")))

     ;; git submodule update -f --init --recursive
     (defun magit-submodule-update-all-force ()
       "Update all submodules"
       :description "Update All (git submodule update -f --init --recursive)"
       (interactive)
       (magit-with-toplevel
         (magit-run-git-async "submodule" "update" "-f" "--init" "--recursive")))

     ;; add a menu item transient to submodule context menu
     (transient-append-suffix
      'magit-submodule
      "u"
      '("U" "submodule update recursive" magit-submodule-update-all-force))

     ))


;; TODO: use a .dir-locals.el for forge-alist
;; docs: https://magit.vc/manual/forge/
;; ref: http://adhilton.pratt.duke.edu/sites/adhilton.pratt.duke.edu/files/u37/forge.pdf

(defcustom emacs-custom-gitlab-forge-fqdn (getenv "EMACS_CUSTOM_GITLAB_FORGE_FQDN")
  "My custom gitlab enterprise instance, e.g. \"gitlab.acme.com\""
  :type 'string)

(use-package forge
  :init (with-eval-after-load 'magit
          (require 'forge))
  :after magit
  :config
  ;; override the gitlab forge repo with a custom if avail
  (when emacs-custom-gitlab-forge-fqdn
    (add-to-list 'forge-alist
                 (list emacs-custom-gitlab-forge-fqdn
                  (format "%s/api/v4" emacs-custom-gitlab-forge-fqdn)
                   emacs-custom-gitlab-forge-fqdn
                   forge-gitlab-repository))))

(use-package git-gutter
  :hook after-init
  :init
  (setq use-custom-symbols nil)
  :config
  (progn

    (defun my-set-git-gutter-faces ()
      ;; (set-face-foreground 'git-gutter:modified "#AA00FF")
      (set-face-foreground 'git-gutter:modified "#D19A66")
      ;; (set-face-foreground 'git-gutter:added "#66CD00")
      ;; (set-face-foreground 'git-gutter:deleted "#F2473F")
      )

    (my-set-git-gutter-faces)
    (add-hook 'after-load-theme-hook 'my-set-git-gutter-faces)

    ;; ;; custom symbols for git status
    ;; (when use-custom-symbols
    ;;   (when (display-graphic-p)
    ;;     (custom-set-variables
    ;;      '(git-gutter:window-width 2)))
    ;;   (custom-set-variables
    ;;    '(git-gutter:modified-sign "☁")
    ;;    '(git-gutter:added-sign "☀")
    ;;    '(git-gutter:deleted-sign "☂")))

    (global-git-gutter-mode t)

    ;; (setq-default fringes-outside-margins t)
    ;; (setq indicate-empty-lines nil)
    ;; (setq git-gutter:lighter ""
    ;;       git-gutter:handled-backends '(git hg bzr svn))

    (custom-set-variables
     '(git-gutter:modified-sign "=")
     '(git-gutter:added-sign "+")
     '(git-gutter:deleted-sign "-")
    )

    ;; update view state when magit refreshes
    ;; e.g. stage/unstage
    (add-hook 'magit-post-refresh-hook
              #'git-gutter:update-all-windows)


    (with-eval-after-load 'hydra
      (defhydra hydra-git-gutter ()
	      "
Git gutter:
  _j_: next hunk        _s_tage hunk     _q_uit
  _k_: previous hunk    _r_evert hunk    _Q_uit and deactivate git-gutter
  ^ ^                   _p_opup hunk
  _h_: first hunk
  _l_: last hunk        set start _R_evision
"
	      ("j" git-gutter:next-hunk)
	      ("k" git-gutter:previous-hunk)
	      ("h" (progn (goto-char (point-min))
		                (git-gutter:next-hunk 1)))
	      ("l" (progn (goto-char (point-min))
		                (git-gutter:previous-hunk 1)))
	      ("s" git-gutter:stage-hunk)
	      ("r" git-gutter:revert-hunk)
	      ("p" git-gutter:popup-hunk)
	      ("R" git-gutter:set-start-revision)
	      ("q" nil :color blue)
	      ("Q" (progn (git-gutter-mode -1)
		                ;; git-gutter-fringe doesn't seem to
		                ;; clear the markup right away
		                (sit-for 0.1)
		                (git-gutter:clear))
	       :color blue)))
    ))


(when (display-graphic-p)
  (use-package git-gutter-fringe
    :after git-gutter
    :demand fringe-helper
    ;; :hook (after-load-theme . global-git-gutter-mode)
    ;; :hook (after-init . global-git-gutter-mode)
    :config
    (progn
      ;; purple/green/red scheme

      (defun my-set-git-gutter-fringe-faces ()
        ;; (set-face-foreground 'git-gutter:modified "#AA00FF")
        (set-face-foreground 'git-gutter-fr:modified "#D19A66")
        ;; (set-face-foreground 'git-gutter-fr:added "#66CD00")
        ;; (set-face-foreground 'git-gutter-fr:deleted "#F2473F")
        )

      (my-set-git-gutter-fringe-faces)
      (add-hook 'after-load-theme-hook 'my-set-git-gutter-fringe-faces)

      ;; STANDARD - DOUBLE WIDTH

      ;; (fringe-helper-define 'git-gutter-fr:modified nil
      ;;   "........"
      ;;   "XXXXXXXX"
      ;;   "XXXXXXXX"
      ;;   "........"
      ;;   "........"
      ;;   "XXXXXXXX"
      ;;   "XXXXXXXX"
      ;;   "........")

      ;; (fringe-helper-define 'git-gutter-fr:added nil
      ;;   "...XX..."
      ;;   "...XX..."
      ;;   "...XX..."
      ;;   "XXXXXXXX"
      ;;   "XXXXXXXX"
      ;;   "...XX..."
      ;;   "...XX..."
      ;;   "...XX...")

      ;; (fringe-helper-define 'git-gutter-fr:deleted nil
      ;;   "........"
      ;;   "........"
      ;;   "........"
      ;;   "XXXXXXXX"
      ;;   "XXXXXXXX"
      ;;   "........"
      ;;   "........"
      ;;   "........")


      ;; DOOM THIN SOLID COLOR - can create bitmap vectors in advance for faster loading, see: `fringe-helper-convert'
      ;; ref: https://gist.github.com/hlissner/f9da197d40d1a0415a66b0bef49696fc

      ;; Setting up the fringe

      (defconst doom-fringe-size '3 "Default fringe width")

      ;; switches order of fringe and margin
      (setq-default fringes-outside-margins t)

      ;; standardize fringe width
      (fringe-mode doom-fringe-size)
      (push `(left-fringe  . ,doom-fringe-size) default-frame-alist)
      (push `(right-fringe . ,doom-fringe-size) default-frame-alist)

      (define-fringe-bitmap 'git-gutter-fr:added
        [224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224]
        nil nil 'center)
      (define-fringe-bitmap 'git-gutter-fr:modified
        [224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224]
        nil nil 'center)
      (define-fringe-bitmap 'git-gutter-fr:deleted
        ;; [0 0 0 0 0 0 0 0 0 0 0 0 0 128 192 224 240 248]
        [224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224 224]
        nil nil 'center)

      )))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; GERRIT
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; NOTE: you must add a .dir-locals.el at your project root that sets the project's gerrit config
;; ((nil . gerrit-host . "gerrit.magicleap.com")
;;  (magit-mode . ((magit-gerrit-ssh-creds . "gerrit.magicleap.com")
;;                 (magit-gerrit-show-review-labels . t))))
(use-package magit-gerrit
  :demand t
  ;; :quelpa (magit-gerrit :fetcher github :repo "darcylee/magit-gerrit")
  :straight (magit-gerrit :type git :host github :repo "darcylee/magit-gerrit"))

;; (use-package gerrit
;;   :ensure t

;;   :custom
;;   (gerrit-host "gerrit.magicleap.com")  ;; is needed for REST API calls
;;   (gerrit-use-gitreview-interface nil)
;;   :config
;;   (progn
;;     (add-hook 'magit-status-sections-hook #'gerrit-magit-insert-status t)
;;     (global-set-key (kbd "C-x i") 'gerrit-upload-transient)
;;     (global-set-key (kbd "C-x o") 'gerrit-download))
;;   )


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; DIFFING
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; https://matthewbauer.us/bauer/
(use-package diff-hl
  :disabled
  :bind (:package diff-hl
                  :map diff-hl-mode-map
                  ("<left-fringe> <mouse-1>" . diff-hl-diff-goto-hunk))
  :hook ((prog-mode . diff-hl-mode)
         (vc-dir-mode . diff-hl-mode)
         (dired-mode . diff-hl-dir-mode)
         (magit-post-refresh . diff-hl-magit-post-refresh)
         (org-mode . diff-hl-mode)))


(use-package ediff-wind
  :ensure nil
  :straight nil
  :custom
  (ediff-split-window-function #'split-window-horizontally)
  (ediff-window-setup-function #'ediff-setup-windows-plain))


;; automatically detect merge conflict marks and enable the hydra
;; TODO: create a binding for smerge hydra
(use-package smerge-mode
  :preface
  (with-eval-after-load 'hydra
    (defhydra hydra-smerge
      (:color pink :hint nil :post (smerge-auto-leave))
      "
^Move^       ^Keep^               ^Diff^                 ^Other^
^^-----------^^-------------------^^---------------------^^-------
_n_ext       _b_ase               _<_: upper/base        _C_ombine
_p_rev       _u_pper              _=_: upper/lower       _r_esolve
^^           _l_ower              _>_: base/lower        _k_ill current
^^           _a_ll                _R_efine
^^           _RET_: current       _E_diff
"
      ("n" smerge-next)
      ("p" smerge-prev)
      ("b" smerge-keep-base)
      ("u" smerge-keep-upper)
      ("l" smerge-keep-lower)
      ("a" smerge-keep-all)
      ("RET" smerge-keep-current)
      ("\C-m" smerge-keep-current)
      ("<" smerge-diff-base-upper)
      ("=" smerge-diff-upper-lower)
      (">" smerge-diff-base-lower)
      ("R" smerge-refine)
      ("E" smerge-ediff)
      ("C" smerge-combine-with-next)
      ("r" smerge-resolve)
      ("k" smerge-kill-current)
      ("ZZ" (lambda ()
              (interactive)
              (save-buffer)
              (bury-buffer))
       "Save and bury buffer" :color blue)
      ("q" nil "cancel" :color blue)))
  :hook ((find-file . (lambda ()
                        (save-excursion
                          (goto-char (point-min))
                          (when (re-search-forward "^<<<<<<< " nil t)
                            (smerge-mode 1)))))
         (magit-diff-visit-file . (lambda ()
                                    (when smerge-mode
                                      (smerge-hydra/body))))))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; GREPPERY
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; requires silversearcher install: https://github.com/ggreer/the_silver_searcher#insta
(use-package ag
  ;; :ensure-system-package ag
  :init
  (setq ag-arguments (list "--word-regexp" "--smart-case")))

(use-package ripgrep
  ;; :ensure-system-package ripgrep
  )


(use-package rg
  ;; :ensure-system-package ripgrep
  :init
  (setq rg-show-header nil)
  )


;; requires ripgrep install: apt-get install ripgrep
(use-package deadgrep
  ;; :ensure-system-package ripgrep
  :bind ("<f5>" . deadgrep))

;; nec for grep nulear editing in ivy
;; https://github.com/mhayashi1120/Emacs-wgrep
(use-package wgrep
  :custom
  (wgrep-enable-key "e")
  (wgrep-auto-save-buffer t)
  (wgrep-change-readonly-file t))

;; nec for grep nulear editing in helm
(use-package wgrep-helm)

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; IVY & COUNSEL
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; ivy will use flx to sort results by relevance if it's added afterward
;; (use-package flx
;;   :after ivy)

;; (use-package flx-ido
;;   :after ivy)

;; (use-package ivy
;;   :diminish ivy-mode
;;   :config
;;   (progn
;;     ;; Create and delete a view
;;     ;; (global-set-key (kbd "C-c v") 'ivy-push-view)
;;     ;; (global-set-key (kbd "C-c V") 'ivy-pop-view)
;;     (ivy-mode 1))
;;   :init
;;   (progn
;;     ;; enable switching to the previously opened file buffers
;;     (setq ivy-use-virtual-buffers t)

;;     (setq ivy-display-style 'fancy
;;           ivy-count-format "(%d/%d) " ; this is what you want
;;           ivy-initial-inputs-alist nil ; remove initial ^ input.
;;           ivy-extra-directories nil) ; remove . and .. directory.

;;     (setq ivy-virtual-abbreviate 'abbreviate)
;;     (setq ivy-count-format "(%d/%d) ")
;;     (setq ivy-use-selectable-prompt t)))


;; ;; requires rg and fzf
;; (use-package counsel
;;   :demand
;;   :init
;;   (setq ivy-use-virtual-buffers t
;;         ;; ivy-re-builders-alist
;;         ;; '((counsel-git-grep . ivy--regex-plus)
;;         ;;   (counsel-rg . ivy--regex-plus)
;;         ;;   (swiper . ivy--regex-plus)
;;         ;;   (swiper-all . ivy--regex-plus)
;;         ;;   (t . ivy--regex-fuzzy))
;;         )
;;   :config
;;   (add-to-list 'ivy-ignore-buffers "\\`\\*remind-bindings\\*")
;;   (ivy-mode 1)
;;   (counsel-mode 1)
;;   :bind
;;   (("M-x" . 'counsel-M-x)
;;    ;; ("C-x C-f" . counsel-find-file) ;; this gets used by ivy in my config
;;    ("C-c E" . counsel-flycheck)
;;    ;; ("C-c f" . counsel-fzf)
;;    ("C-c g" . counsel-git)
;;    ("C-c j" . counsel-git-grep)
;;    ("C-c L" . counsel-locate)
;;    ("C-c o" . counsel-outline)
;;    ;; ("C-c r" . counsel-rg)
;;    ("C-c R" . counsel-register)
;;    ("C-c T" . counsel-load-theme)))


;; currrently currently xrefs are handled by helm-xref
;; (use-package ivy-xref
;;   :init
;;   (setq xref-show-xrefs-function #'ivy-xref-show-xrefs))


;; show ivy at position of point instead of minibuffer
;; (use-package ivy-posframe
;;   :init
;;   (setq ivy-posframe-display-functions-alist
;;         '((t . ivy-posframe-display-at-frame-center)))
;;   :config
;;   (ivy-posframe-mode 1))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; SWIPER (disabled for helm-swoop instead)
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; (use-package swiper
;;   :config
;;   (progn
;;     ;; default to thing-at-point when swiping
;;     ;; https://github.com/abo-abo/swiper/issues/1068
;;     (defun ivy-with-thing-at-point (cmd)
;;       (let ((ivy-initial-inputs-alist
;;              (list
;;               (cons cmd (thing-at-point 'symbol)))))
;;         (funcall cmd)))

;;     (defun swiper-thing-at-point ()
;;       (interactive)
;;       (ivy-with-thing-at-point 'swiper))

;;     ;; NOTE: swap with helm-swoop/helm-resume commands by commenting out these and commenting in helm's
;;     ;; (global-set-key "\C-s" 'swiper-thing-at-point)
;;     ;; ;; using ivy-resume, which will bring up last swipe session
;;     ;; (global-set-key (kbd "C-r") 'ivy-resume)

;;     (ivy-mode 1))

;;   ;; extend swiper to insert word/symbol at point
;;   (define-key swiper-map (kbd "C-.")
;;     (lambda () (interactive) (insert (format "\\<%s\\>" (with-ivy-window (thing-at-point 'symbol))))))
;;   (define-key swiper-map (kbd "M-.")
;;     (lambda () (interactive) (insert (format "\\<%s\\>" (with-ivy-window (thing-at-point 'word)))))))

;; (use-package swiper-helm

;;   :bind ("C-s" . swiper-helm))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; HELM
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(use-package helm
  ;; :after (helm-swoop)
  :init
  (progn
    (require 'helm)
    (require 'helm-config)
    (require 'helm-mode)
    (require 'helm-buffers)
    (require 'helm-for-files)
    (require 'helm-x-files)
    (require 'helm-ring)

    ;; (setq helm-autoresize-mode 1)

    ;; these vars allow helm-swoop to not take entire window when used with neotree or treemacs
    (setq helm-swoop-split-with-multiple-windows t
          helm-swoop-split-direction 'split-window-vertically
          helm-swoop-split-window-function 'helm-default-display-buffer)

    ;; allow fuzzy matching on these modes
    (setq helm-M-x-fuzzy-match                  t
          helm-bookmark-show-location           t
          helm-buffers-fuzzy-matching           t
          helm-completion-in-region-fuzzy-match t
          helm-file-cache-fuzzy-match           t
          helm-imenu-fuzzy-match                t
          helm-mode-fuzzy-match                 t
          helm-locate-fuzzy-match               t
          helm-quick-update                     t
          helm-recentf-fuzzy-match              t
          helm-semantic-fuzzy-match             t))
  :config
  (progn
    ;; open helm buffer inside current window, not occupy whole other window
    (setq helm-split-window-in-side-p t)

    ;; set helm buffer filename column width wide enough to see long filenames without ...
    ;; use nil to use the longest buffername found or something like 40 to constrain
    (setq helm-buffer-max-length nil)
    (defun spacemacs//helm-hide-minibuffer-maybe ()
      (when (with-helm-buffer helm-echo-input-in-header-line)
	(let ((ov (make-overlay (point-min) (point-max) nil nil t)))
	  (overlay-put ov 'window (selected-window))
	  (overlay-put ov 'face
		       (let ((bg-color (face-background 'default nil)))
			 `(:background ,bg-color :foreground ,bg-color)))
	  (setq-local cursor-type nil))))

    ;; set helm height
    (add-hook 'helm-minibuffer-set-up-hook
	      'spacemacs//helm-hide-minibuffer-maybe)
    (setq helm-autoresize-max-height 0)
    (setq helm-autoresize-min-height 40)
    (helm-autoresize-mode 1)


    ;; (require 'helm-config)
    (setq helm-candidate-number-limit 100)
    ;; From https://gist.github.com/antifuchs/9238468
    (setq helm-idle-delay 0.0 ; update fast sources immediately (doesn't).
          helm-input-idle-delay 0.01  ; this actually updates things
                                        ; reeeelatively quickly.
          helm-yas-display-key-on-candidate t
          helm-quick-update t
          helm-M-x-requires-pattern nil
          helm-ff-skip-boring-files t)

    ;; what sources will we show in helm-mini
    (setq helm-mini-default-sources
	  '(helm-source-buffers-list
	    ;; helm-source-bookmarks
	    helm-source-recentf
	    helm-source-buffer-not-found))

    ;; suppress headerline
    (setq helm-display-header-line nil) ;; t by default
    (setq helm-echo-input-in-header-line t)

    ;; suppress modline while in helm
    (progn
      ;; 1. Collect bottom buffers
      (defvar bottom-buffers nil
    	"List of bottom buffers before helm session.
         Its element is a pair of `buffer-name' and `mode-line-format'.")

      (defun bottom-buffers-init ()
    	(when bottom-buffers
    	  (bottom-buffers-show-mode-line))
    	(setq bottom-buffers
    	      (cl-loop for w in (window-list)
    		       when (window-at-side-p w 'bottom)
    		       collect (with-current-buffer (window-buffer w)
    				 (cons (buffer-name) mode-line-format)))))

      (add-hook 'helm-before-initialize-hook #'bottom-buffers-init)
      ;; 2. Hide mode line
      (defun bottom-buffers-hide-mode-line ()
    	(mapc (lambda (elt)
    		(with-current-buffer (car elt)
    		  (setq-local mode-line-format nil)))
    	      bottom-buffers))

      (add-hook 'helm-after-initialize-hook #'bottom-buffers-hide-mode-line)

      ;; 3. Restore mode line
      (defun bottom-buffers-show-mode-line ()
    	  (when bottom-buffers
    	    (mapc (lambda (elt)
                  (when (get-buffer (car elt))
                    (with-current-buffer (car elt)
    		              (setq-local mode-line-format (cdr elt)))))
    		        bottom-buffers)
    	    (setq bottom-buffers nil)))

      (add-hook 'helm-exit-minibuffer-hook #'bottom-buffers-show-mode-line)

      (defun helm-keyboard-quit-advice (orig-func &rest args)
    	(bottom-buffers-show-mode-line)
    	(apply orig-func args))

      (advice-add 'helm-keyboard-quit :around #'helm-keyboard-quit-advice))


    ;; make TAB behave like completion key for helm by
    ;; defining reversed keymaps for tab while in helm
    (define-key helm-map (kbd "TAB") #'helm-execute-persistent-action)
    (define-key helm-map (kbd "<tab>") #'helm-execute-persistent-action)
    (define-key helm-map (kbd "C-SPC") #'helm-select-action)

    ;; define a wrapper to use so helm can be project aware when finding files
    (defun smart-helm-for-files ()
      "Call `helm-projectile' if `projectile-project-p', otherwise fallback to `helm-for-files'."
      (interactive)
      (if (projectile-project-p)
          (helm-projectile-find-file-dwim)
        (helm-find-files)))

    ;; define the preferred order files are shown in
    (setq helm-for-files-preferred-list
          '(
            helm-source-projectile-files-list
            ;; helm-source-projectile-buffers-list
            helm-source-projectile-recentf-list
            helm-source-buffers-list
            helm-source-recentf
            helm-source-projectile-directories-list
            helm-source-projectile-projects
            ;; helm-source-bookmarks
            ;; helm-source-file-cache
            helm-source-files-in-current-dir
            helm-source-moccur
            helm-source-locate
            ))

    ;; use helm to find files for projects instead
    (define-key global-map [remap projectile-find-file] 'helm-projectile-find-file-dwim)

    ;; use helm-projectile when switching into a projectile project
    (setq projectile-switch-project-action 'helm-projectile)

    ;; helm view toggle functions

    (defun my-toggle-helm (helm-view-fn &optional use-fast-selection)
      "Toggles any view from helm. The passed callback must be interactive."
      (if helm-alive-p
          (if use-fast-selection
              ;; select without explicit RET
              (progn
                ;; makes it sticky, only enter of C-g will quit
                ;; (setq quit-flag t)
                ;; allows toggle but selection can only be done via RET
                ;; (setq helm-quit t)

                ;; these make it fully dynamic
                (helm-kill-async-processes)
                (helm-restore-position-on-quit)
                (helm-exit-minibuffer))
            ;; standard selection behavior
            ;; same as setting helm-quit t
            (helm-keyboard-quit))
          (funcall helm-view-fn)))


    ;; NOTE: this fn ensures that helm doesn't change the cursor when we exit
    (defun helm-keyboard-quit-advice (orig-func &rest args)
      "Reset the cursor-type back to what it was after quitting helm"
      (interactive)
      (deferred:$
        (deferred:wait 3) ; msec
        (deferred:nextc it
          (lambda (x)
            (when (boundp 'cursor-type)
              (setq cursor-type cursor-type)))))
      (apply orig-func args))

    ;; advising all these functions ensures we cover all cases
    (advice-add 'helm-cleanup :around #'helm-keyboard-quit-advice)
    (advice-add 'keyboard-escape-quit :around #'helm-keyboard-quit-advice)
    (advice-add 'keyboard-quit :around #'helm-keyboard-quit-advice)



    (defun my-toggle-helm-buffers-list ()
      "Toggles helm-buffers-list"
      (interactive)
      (my-toggle-helm 'helm-buffers-list nil))

    ;; NOTE: using C-TAB for helm-buffers for now
    (if *is-in-terminal*
        (my/global-map-and-set-key "C-TAB" 'my-toggle-helm-buffers-list)
      (progn (define-key helm-map (kbd "C-<tab>") 'my-toggle-helm-buffers-list)
       (bind-key* (kbd "C-<tab>") 'my-toggle-helm-buffers-list)))

    ;; NOTE: this will surely interfere with anything evil
    ;; allow ESC to exit helm in these specific helm-related maps
    ;; (add-hook 'helm-after-initialize-hook
    ;;           (lambda()
    ;;             ;; TODO: [WIP] replace the commands below with a nicer code and define list
    ;;             ;; (let ((targeted-helm-maps (list(
    ;;             ;;                                 helm-ag-map
    ;;             ;;                                 helm-rg-map
    ;;             ;;                                 helm-grep-map
    ;;             ;;                                 ))))
    ;;             ;;   (dolist (m targeted-helm-maps)
    ;;             ;;     (define-key m (kbd "ESC") 'helm-keyboard-quit)))
    ;;             (define-key helm-ag-map (kbd "ESC") 'helm-keyboard-quit)
    ;;             (define-key helm-rg-map (kbd "ESC") 'helm-keyboard-quit)
    ;;             (define-key helm-grep-map (kbd "ESC") 'helm-keyboard-quit)
    ;;             (define-key helm-buffer-map (kbd "ESC") 'helm-keyboard-quit)
    ;;             (define-key helm-M-x-map (kbd "ESC") 'helm-keyboard-quit)
    ;;             (define-key helm-find-files-map (kbd "ESC") 'helm-keyboard-quit)
    ;;             (define-key helm-map (kbd "ESC") 'helm-keyboard-quit)))

    ;; Note: THIS IS WHAT ALLOWS HELM TO OVERRIDE IVY FOR FIND-FILE
    ;; it must be loaded after (ivy-mode 1)
    (helm-mode 1)

    )


  :bind (("C-c h" . helm-mini)
         ("C-h a" . helm-apropos)
         ("C-x C-f" . helm-find-files) ;; this gets used by ivy in my config
         ;; ("C-x C-f" . smart-helm-for-files) ;; use find files in project if in project context
         ("C-x c m" . helm-mini)
         ("C-x c o" . helm-imenu)

         ;; handle dired use case
         ;; ("C-x C-d" . ranger)
         ("C-x C-d" . dired)

         ("C-x C-b" . helm-buffers-list)
         ;;("C-x b" . helm-buffers-list) ;; belongs to ivy
         ("M-y" . helm-show-kill-ring)
         ("M-x" . helm-M-x)
         ("M-s o" . helm-occur)

         ;; NOTE: swap with swiper/ivy-resume commands by commenting out these and commenting in swiper's
         ;; swoop pre input: https://github.com/emacsorphanage/helm-swoop#configure-pre-input-search-query
         ("C-s" . helm-swoop-without-pre-input)
         ;; ("C-S-s" . helm-swoop)
         ("C-r" . helm-resume)

         ("C-c y" . helm-yas-complete)
         ("C-c Y" . helm-yas-create-snippet-on-region)
         ("C-h SPC" . helm-all-mark-rings)
         ("C-x C-r"   . helm-recentf)))


;; (use-package helm-posframe
;;   ;; :disabled t
;;   :after helm
;;   :demand t
;;   :if (and (window-system) (version<= "26.1" emacs-version))
;;   :config
;;   (setq helm-posframe-poshandler 'posframe-poshandler-frame-center
;;         helm-posframe-height 20
;;         helm-posframe-width (round (* (frame-width) 0.75))
;;         helm-posframe-parameters '((internal-border-width . 10)))
;;   (helm-posframe-enable)
;;   (helm-posframe-disable)
;;   ;; :hook (helm-org-rifle-after-command . helm-posframe-cleanup)
;;   )

(use-package helm-descbinds

  :commands helm-descbinds
  :config
  (global-set-key (kbd "C-h b") 'helm-descbinds))

(use-package helm-make
  :bind
  (("C-c K" . helm-make)))

;; (use-package helm-flx
;;   :config
;;   (helm-flx-mode +1))

;; DOESN'T WORK - NO CANDIDATES
;; (use-package helm-fuzzier
;;   :config
;;   (helm-fuzzier-mode 1))

;; (use-package helm-fuz
;;   :config
;;   (helm-fuz-mode 1))



;; causes xref-find-references and xref-find-definitions to use helm
;; force load!
(use-package helm-xref

  :config
  (progn

    (if (>= emacs-major-version 27)
        (progn
          (setq xref-show-definitions-function #'helm-xref-show-defs-27)
          (setq xref-show-xrefs-function 'helm-xref-show-xrefs-27)) ; NOTE: this fn name may change soon
      (progn
        (setq xref-show-xrefs-function 'helm-xref-show-xrefs)
        (setq xref-show-definitions-function 'helm-xref-show-xrefs)))

    ;; (setq helm-xref-candidate-formatting-function 'helm-xref-format-candidate-long)

    (defun helm-xref-format-candidate-project (file line summary)
      "Build short form of candidate format with FILE, LINE, and SUMMARY."
      (concat
       (propertize

        ;; strategy 1: strip off prefix from root to project root
        (if (projectile-project-p)
            (replace-regexp-in-string (regexp-quote (projectile-project-root)) ""
                                      file)
          file)

        ;; strategy 2: infer based on prior project file, e.g. Jenkinsfile, .gitignore
        ;; (let ((fname (buffer-file-name)))
        ;;   (file-relative-name fname (locate-dominating-file fname "README.md")))

        'font-lock-face 'helm-xref-file-name)
       (when (string= "integer" (type-of line))
         (concat
          ":"
          (propertize (int-to-string line)
                      'font-lock-face 'helm-xref-line-number)))
       ":"
       summary))

    (setq helm-xref-candidate-formatting-function 'helm-xref-format-candidate-project)

    ))


(use-package helm-swoop

  :config
  (progn
    ;; jump back to place we were before swooping
    ;; (global-set-key (kbd "M-i") 'helm-swoop-back-to-last-point)

    ;; NOTE: we are setting a mark in the xref ring from swooping
    ;; if this behavior becomes problematic, just disable this line

    ;; get a marker ref for current point
    ;; (xref-push-marker-stack (copy-marker (point)))

    (add-hook 'helm-swoop-before-goto-line-action-hook 'xref-push-marker-stack)
))

;; IMPORTANT: ag doesn't properly respect negated ignores in .gitignore (e.g. foo/* && !foo/bar)
(use-package helm-ag
  :after (helm)
  :config
  (progn
    (setq
     helm-ag-use-agignore t
     ;; fixex ansi codes in helm-do-ag output
     ;; man: https://manpages.ubuntu.com/manpages/xenial/man1/ag.1.html
     helm-ag-base-command "ag --nocolor --nogroup"

     helm-ag-insert-at-point 'symbol
     helm-ag-ignore-buffer-patterns '("\\.log\\'" "\\.pem\\'"))

    ;; ignore .gitignore, .hgignore, and svn:ignore, but still take .agignore into account
    ;; (setq helm-ag-command-option " -U")
    ))


;; using my modified version that shows files relative to projectile project
(use-package helm-rg
  :after (helm)
  :ensure nil
 ;; :quelpa (helm-rg :fetcher github :repo "jclosure/helm-rg")
  :config
  ;; flip the polarity of both of these to use file headers on separate lines
  (setq helm-rg-include-file-on-every-match-line t
        helm-rg-prepend-file-name-line-at-top-of-matches nil)
  )

;; C-c p h to switch to it
(use-package helm-projectile

  :after (projectile helm)
  :config
  (progn
    (setq projectile-completion-system 'helm)

    ;; NOTE: this causes all C-c p s [r, g, s] commands to use helm
    (helm-projectile-on)))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; JUMP XREF FIND REFERENCES SPEC AND ADVICE
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; NOTE: THESE ARE PROJECT-ORIENTED OVERRIDABLE BACKENDS TO: xref-find-references
;; IMPORTANT: projectile's config fn must already be run for other apps to use these

;; set from: http://bryan-murdock.blogspot.com/2018/03/fixing-xref-find-references.html
;; to lookup references of words with word boundary in projectile projects.
;; this should be replaced if it causes problems with lsp or other xref-find-references
;; packages!!!
;; M-? is bound by default to xref-find-references

;; ootb ag for projects
;;(define-key global-map [remap xref-find-references] 'ag-project)

;; best helm-ag for projects
;; (define-key global-map [remap xref-find-references] 'helm-ag-project-root)

;; second best helm-ag for projects
;; (define-key global-map [remap xref-find-references] 'helm-projectile-ag) ; using ag because has wgrep support ootb

;; preferring helm-projectile-rg
;; rip grep honors .gitignores and is faster than ag.
;; (define-key global-map [remap xref-find-references] 'helm-projectile-rg)

;; ;; EAGER mark insertion - jump back from xref-finds with M-,
(defun my-xref-find-add-mark-before-advice (&rest args)
  "Advice for `xref-find-references' or `xref-find-definitions' that adds a mark first."

  ;; (cond ((eq major-mode 'python-mode)
  ;;        ;; use python-mode specific marker ring fn
  ;;        (my-python-push-marker-ring))
  ;;       ;; otherwise use default global marker ring
  ;;       ;; TODO: look into using a specific one for elixir
  ;;       (t (xref-push-marker-stack (point-marker))))

  ;; deprecated shows using a generic approach to ring insertion
  ;; (ring-insert find-tag-marker-ring (point-marker))
  (xref-push-marker-stack (point-marker))
  )


;; ;; NOTE: apply this advice to the remapped xref-find-references fn above
(advice-add 'helm-projectile-rg :before #'my-xref-find-add-mark-before-advice)
;; (advice-add 'helm-rg :before #'my-xref-find-add-mark-before-advice)

;; TODO: investigate LAZY mark insertion: https://github.com/emacs-helm/helm/issues/1927

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	Auto complete when coding
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; (use-package auto-complete
;;   ;; :bind ("<TAB>" . auto-complete)
;;   :init
;;   (progn
;;     (ac-config-default)
;;     ;; (global-auto-complete-mode t)
;;     (global-unset-key (kbd "TAB"))
;;     (ac-set-trigger-key "TAB")
;;     (setq ac-use-menu-map t)
;;     ))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	Quickly jumps between other symbols found at point in Emacs.
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; (use-package smartscan

;;   :config (smartscan-mode t))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	Snippet tool for emacs
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(use-package yasnippet
  :diminish yas-minor-mode
  :config
  (progn
    (yas-global-mode)
    (add-hook 'hippie-expand-try-functions-list 'yas-hippie-try-expand)
    (setq yas-key-syntaxes '("w_" "w_." "^ "))
    ;; (setq yas-installed-snippets-dir "~/elisp/yasnippet-snippets")
    (setq yas-expand-only-for-last-commands nil)
    (yas-global-mode 1)
    ;; (bind-key "\t" 'hippie-expand yas-minor-mode-map)
    (add-to-list 'yas-prompt-functions 'shk-yas/helm-prompt)))


(dolist (command '(yank yank-pop))
  (eval
   `(defadvice ,command (after indent-region activate)
      (and (not current-prefix-arg)
	   (member major-mode
		   '(emacs-lisp-mode
		     lisp-mode
		     clojure-mode
		     scheme-mode
		     haskell-mode
		     ruby-mode
		     rspec-mode
		     python-mode
		     c-mode
		     c++-mode
		     objc-mode
		     latex-mode
		     js-mode
		     plain-tex-mode))
	   (let ((mark-even-if-inactive transient-mark-mode))
	     (indent-region (region-beginning) (region-end) nil))))))

(defun shk-yas/helm-prompt (prompt choices &optional display-fn)
  "Use helm to select a snippet. Put this into `yas-prompt-functions.'"
  (interactive)
  (setq display-fn (or display-fn 'identity))
  (if (require 'helm-config)
      (let (tmpsource cands result rmap)
        (setq cands (mapcar (lambda (x) (funcall display-fn x)) choices))
        (setq rmap (mapcar (lambda (x) (cons (funcall display-fn x) x)) choices))
        (setq tmpsource
              (list
               (cons 'name prompt)
               (cons 'candidates cands)
               '(action . (("Expand" . (lambda (selection) selection))))
               ))
        (setq result (helm-other-buffer '(tmpsource) "*helm-select-yasnippet"))
        (if (null result)
            (signal 'quit "user quit!")
          (cdr (assoc result rmap))))
    nil))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	 ABBREV MODE
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(use-package abbrev
  :ensure nil
  :straight nil
  :config
  (setq save-abbrevs 'silently)
  (setq-default abbrev-mode t))



;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	Fixme-mode
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; (add-to-list 'load-path (expand-file-name "~/.emacs.d/vendors/"))
;; (require 'fixme-mode)
;; (fixme-mode 1)

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; 	SLIME mode
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; (use-package slime
;;   :config
;;   (progn
;;     (setq inferior-lisp-program "/usr/local/bin/sbcl")
;;     (setq slime-contribs '(slime-fancy))
;;     ))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; 	HELM-Dash
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; (use-package helm-dash
;;   :config
;;   (progn
;;     ;; (setq helm-dash-browser-func 'google-chrome)
;;     (setq helm-dash-docsets-path (expand-file-name "~/.emacs.d/docsets"))

;;     (helm-dash-activate-docset "Go")
;;     (helm-dash-activate-docset "Python 3")
;;     (helm-dash-activate-docset "CMake")
;;     (helm-dash-activate-docset "Bash")
;;     (helm-dash-activate-docset "Django")
;;     (helm-dash-activate-docset "Redis")
;;     (helm-dash-activate-docset "Emacs Lisp")
;;     ))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; 	Rainbow mode
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; (use-package rainbow-mode
;;   :config
;;   (progn
;;     (defun pkg-enable-rainbow ()
;;       (rainbow-mode t))
;;     (add-hook 'prog-mode-hook 'pkg-enable-rainbow)
;; ))

;; (use-package rainbow-delimiters
;;   :config
;;   (progn
;;     (defun pkg-enable-rainbow-delimiters ()
;;       (rainbow-delimiters-mode t))
;;     (add-hook 'prog-mode-hook 'pkg-enable-rainbow-delimiters)))



;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	ACTIVE BUFFER AND POINT TRACKING
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; (use-package beacon
;;   :diminish beacon-mode
;;   :hook after-init
;;   :config
;;   (beacon-mode 1)
;;   (setq beacon-dont-blink-commands nil) ;; always blink
;;   ;; (setq beacon-lighter '"Λ") -
;;   (add-to-list 'beacon-dont-blink-major-modes 'man-mode)
;;   (add-to-list 'beacon-dont-blink-major-modes 'shell-mode))


;; C-v/M-v visual helper for paging
;; (use-package scrollkeeper

;;   :ensure nil
;;   :quelpa (scrollkeeper :fetcher github :repo "alphapapa/scrollkeeper")
;;   :general ([remap scroll-up-command] #'scrollkeeper-contents-up
;;             [remap scroll-down-command] #'scrollkeeper-contents-down))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; 	Kubernetes tools
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(use-package kubernetes
  :ensure t
  :commands (kubernetes-overview))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; 	Plantuml mode
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; (require 'plantuml-mode)


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; 	Fiplr - Find in Project for Emacs
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(use-package fiplr)

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; 	GENERATE PASSWORDS
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(use-package password-generator
  :ensure t)

(provide 'pkg-packages)
