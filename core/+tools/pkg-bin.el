

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; OPEN ELF FILES
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(use-package elf-mode
  :magic ("ELF" . elf-mode))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; OPEN HEX FILES
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(use-package hexl-mode
  :ensure nil
  :straight nil
  :mode (("\\.bin\\'" . hexl-mode)
         ("\\.a\\'" . hexl-mode)
         ("\\.o\\'" . hexl-mode)
         ("\\.so\\'" . hexl-mode)
         ("\\.so.*\\'" . hexl-mode)
         ("\\.exe\\'" . hexl-mode)
         ("\\.dll\\'" . hexl-mode)))

(provide 'pkg-bin)
