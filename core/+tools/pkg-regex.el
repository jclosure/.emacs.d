;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; REGEX AND REGEX RELATED
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; NOTE: possibly this belongs in pkg-editing


(use-package anzu
  :config
  (progn
    (global-anzu-mode +1)
    (global-set-key (kbd "M-%") 'anzu-query-replace)
    ;; FIXME: C-M only works in gui
    (global-set-key (kbd "C-M-%") 'anzu-query-replace-regexp)

    (custom-set-variables
     '(anzu-mode-lighter "")
     '(anzu-deactivate-region t)
     '(anzu-search-threshold 1000)
     '(anzu-replace-threshold 50)
     '(anzu-replace-to-string-separator " => "))

    (define-key isearch-mode-map [remap isearch-query-replace]  #'anzu-isearch-query-replace)
    (define-key isearch-mode-map [remap isearch-query-replace-regexp] #'anzu-isearch-query-replace-regexp)
    ))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; REGEX TOOLS
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(use-package regex-tool
  :commands regex-tool
  ;; :custom
  ;; (regex-tool-backend 'perl)
  )

;; (use-package pcre2el)

;; (use-package visual-regexp
;;     :bind (("C-M-r" . vr/isearch-backward)
;;          ("C-M-s" . vr/isearch-forward)
;;          ("C-M-%" . vr/query-replace)))

;; ;; usage: https://github.com/benma/visual-regexp-steroids.el/
;; (use-package visual-regexp-steroids
;;   :bind (("C-M-r" . vr/isearch-backward)
;;          ("C-M-s" . vr/isearch-forward)
;;          ("C-M-%" . vr/query-replace)))


(provide 'pkg-regex)
