
(message "**************** LOADED PACKAGE HELP *****************")

;; HELP CUSTOMIZATION

(use-package help-fns+
  ;; :quelpa (help-fns+ :fetcher url :url "https://www.emacswiki.org/emacs/download/help-fns+.el")
  ;; :quelpa (help-fns+ :fetcher wiki)
  ;; :quelpa (help-fns+ :fetcher github :repo "emacsmirror/help-fns-plus")
  :straight (help-fns+ :type git :host github :repo "emacsmirror/help-fns-plus"))


(use-package abbrev
  :ensure nil
  :straight nil
  :config
  (setq save-abbrevs 'silently)
  (setq-default abbrev-mode t))


(use-package helpful
  :hook after-init
  :bind
  (("C-h f" . helpful-callable)
   ("C-h v" . helpful-variable)
   ("C-h k" . helpful-key)
   ("C-c C-d" . helpful-at-point)
   ("C-h F" . helpful-function)
   ("C-h C" . helpful-command)))

;; helpful examples added to elisp help system
(use-package elisp-demos
  :hook ((help-mode . (lambda () (require 'elisp-demos)))
         (helpful-mode . (lambda () (require 'elisp-demos))))
  :config
  (advice-add 'describe-function-1 :after #'elisp-demos-advice-describe-function-1)
  (advice-add 'helpful-update :after #'elisp-demos-advice-helpful-update))


;; https://github.com/flycheck/flycheck-pos-tip
(use-package flycheck-pos-tip
  :after flycheck
  :config
  (flycheck-pos-tip-mode))


(provide 'pkg-help)
