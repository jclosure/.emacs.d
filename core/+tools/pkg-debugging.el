;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;  See keys used in a buffer in realtime.
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; M-x clm/toggle-command-log-buffer
(use-package command-log-mode
  :commands (command-log-mode))

;; NOTE: need to show the command-log-buffer ^ and toggle on
;; M-x command-log-mode


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	See internal functions fire in realtime
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(defun connect-to-explain-pause-socket ()
  "TOP FOR EMACS

Get a live log of running commands with explain-pause-mode
There is also a explain-pause-top fn to discover
heavy fns running in emacs.

Usage:

Monitor all functions run in emacs from outside shell w/:

step 1: in a separate terminal shell:
socat -u UNIX-RECV:/tmp/explain-pause.socket STDOUT | grep -v tabbar-ruler-mouse-movement

step 2:
(connect-to-explain-pause-socket)
"
  (useq-package explain-pause-mode
                :demand t
                ;; :quelpa (explain-pause-mode :fetcher github :repo "lastquestion/explain-pause-mode")
                :straight (explain-pause-mode :type git :host github :repo "lastquestion/explain-pause-mode")
                )

  (defun start-explain-pause-logging ()
    (explain-pause-log-to-socket "/tmp/explain-pause.socket")
    (explain-pause-mode t))

  (start-explain-pause-logging))


(provide 'pkg-debugging)
