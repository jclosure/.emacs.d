;;; Commentary:
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; desktops/perspectives settings
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;; -*- lexical-binding: t; -*-
;;; Code:

;; NOTE: all of these sections should be considered mutually exclusive, so only enable one of them

;; NOTE: there are 2 competing and incompatible perspective libraries right now:
;; 1. persp-mode.el - https://github.com/Bad-ptr/persp-mode.el
;; 1. perspective.el - https://github.com/nex3/perspective-el

;; This file has support for both.  Make sure to comment in/out the full sections accordingly
;; when swtich out impls!!!

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; DESKTOP
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; (use-package desktop
;;   :hook
;;   (after-init . desktop-read)
;;   (after-init . desktop-save-mode))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; EYEBROWSE
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; ;; remap these config keys
;; (use-package eyebrowse
;;   :bind
;;   ("<f5>" . eyebrowse-switch-to-window-config-1)
;;   ("<f6>" . eyebrowse-switch-to-window-config-2)
;;   ("<f7>" . eyebrowse-switch-to-window-config-3)
;;   ("<f8>" . eyebrowse-switch-to-window-config-4)
;;   :hook
;;   (after-init . eyebrowse-mode)
;;   :custom
;;   (eyebrowse-new-workspace t))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; PERSP-MODE
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(use-package persp-mode-projectile-bridge
  :after (persp-mode)
  :config
  (with-eval-after-load "persp-mode-projectile-bridge-autoloads"
    (add-hook 'persp-mode-projectile-bridge-mode-hook
              #'(lambda ()
                  (if persp-mode-projectile-bridge-mode
                      (persp-mode-projectile-bridge-find-perspectives-for-all-buffers)
                    (persp-mode-projectile-bridge-kill-perspectives))))
    (add-hook 'after-init-hook
              #'(lambda ()
                  (persp-mode-projectile-bridge-mode 1))
              t)))


;; NOTE: demanded in order to recover last perspective on init
;; TODO: try using transients for perspective management: https://pastebin.com/ykPpPXqw
;; NOTE: the default bindings for persp-mode which use C-c p as a prefix, are actually taken by projectile in this config
(use-package persp-mode
  :demand t ; explicit defer is the only way I can get this to reliably load perspectives at start
  :init
  (setq-default
   persp-auto-resume-time 0.1)

  :config
  (progn
    (message "******************* PERSP-MODE **********************")
    (persp-mode 1)
    (setq-default
     persp-auto-save-fname ".persp"
     persp-auto-save-num-of-backups 1
     persp-autokill-buffer-on-remove nil)


    ;; register the copy-log with persp-mode by name
    (setq persp-save-buffer-functions (cons
                                       (lambda (b)
                                         (with-current-buffer b
                                           (when (string= (buffer-name) "*copy-log*")
                                             `(def-copy-log-buffer ,(buffer-name)))))
                                       persp-save-buffer-functions))


    ;; filter out buffers we don't want picked up
    (add-hook 'persp-common-buffer-filter-functions
              ;; there is also `persp-add-buffer-on-after-change-major-mode-filter-functions'
              #'(lambda (b)
                  (or
                   ;; filter out ephemeral buffers
                   (string-prefix-p "*" (buffer-name b))

                   ;; filter out magit buffers
                   (string-prefix-p "magit" (buffer-name b))

                   ;; filter out dired-based buffers (included ranger)
                   (with-current-buffer b
                     (derived-mode-p 'dired-mode)))))
    ))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; PERSPECTIVE-EL
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; NOTE: perspective.el is a more active project. It breaks persstent-scratch, which is important to me.
;; this happens because they abuse scratch by pointing unresolvable/saved files to it on load.
;; these projects will likely merge in the future. If they do, delete the persp-mode sedtion above.

;; (use-package perspective

;;   :preface
;;   (progn
;;     (defun cm/persp-neo ()
;;       "If NeoTree buffer is showing NeoTree follow the perspective"
;;       (interactive)
;;       (when (get-buffer " *NeoTree*")
;;         (let ((cw (selected-window))
;;               (path (buffer-file-name))) ;; save current window and buffer
;;           (progn
;;             (when (and (fboundp 'projectile-project-p)
;;                        (projectile-project-p)
;;                        (fboundp 'projectile-project-root))
;;               (neotree-dir (projectile-project-root)))
;;             (neotree-find path))
;;           (select-window cw)))))
;;   :hook
;;   ((after-init . persp-mode)
;;    (persp-switch . cm/persp-neo))
;;   :init
;;   (progn
;;     (setq persp-state-default-file "~/.emacs.d/var/perspective-default")
;;     (add-hook 'kill-emacs-hook #'persp-state-save)
;;     (add-hook 'after-init-hook  #'(lambda () (persp-state-load persp-state-default-file)))))

;; (use-package persp-projectile
;;   :after (perspective)
;;   :bind
;;   ("C-c x" . hydra-persp/body)
;;   :config
;;   (defhydra hydra-persp (:columns 4
;;                                   :color blue)
;;     "Perspective"
;;     ("a" persp-add-buffer "Add Buffer")
;;     ("i" persp-import "Import")
;;     ("c" persp-kill "Close")
;;     ("n" persp-next "Next")
;;     ("p" persp-prev "Prev")
;;     ("k" persp-remove-buffer "Kill Buffer")
;;     ("r" persp-rename "Rename")
;;     ("A" persp-set-buffer "Set Buffer")
;;     ("s" persp-switch "Switch")
;;     ("C-x" persp-switch-last "Switch Last")
;;     ("b" persp-switch-to-buffer "Switch to Buffer")
;;     ("P" projectile-persp-switch-project "Switch Project")
;;     ("q" nil "Quit")))


(provide 'pkg-perspectives)
