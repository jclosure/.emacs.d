
;; the scratch buffer will persist between runs
(use-package persistent-scratch
  :demand t
  :config
  (progn

    (setq scratch-buffers '("*scratch*" "*copy-log*"))

    ;; prevent killing of scratch buffers
    (deferred:$
      (deferred:wait 5000) ; msec
      (deferred:nextc it
        (lambda (x)
          (mapcar (lambda (name)
                    (when (get-buffer name)
                      (prevent-kill-buffer name)))
                  scratch-buffers))))


    ;; override from persistent-scratch.el
    ;; add more scratch buffers to be persisted
    (defun persistent-scratch-default-scratch-buffer-p ()
      "Return non-nil iff the current buffer's name explicitly matches an element in scratch-buffers."
      (member (buffer-name) scratch-buffers))


    ;; (setq scratch-buffer-prefixes '("*scratch" "*copy-log*"))
    ;; ;; override from persistent-scratch.el
    ;; ;; add more scratch buffers to be persisted
    ;; (defun persistent-scratch-default-scratch-buffer-p ()
    ;;   "Return non-nil iff the current buffer's name matches an element in scratch-buffer-prefixes."
    ;;   (when (seq-filter (lambda (prefix) (string-prefix-p prefix (buffer-name)))
    ;;                     scratch-buffer-prefixes))
    ;;   (buffer-name))

    (persistent-scratch-setup-default)
    (persistent-scratch-autosave-mode)))



;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; keep a live running log of all keystrokes and fns they run
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(use-package interaction-log
  :init
  (progn
    (defun open-interaction-log ()
      (interactive)
      (display-buffer ilog-buffer-name))
    (defun toggle-interaction-logging ()
      (interactive)
      (if  interaction-log-mode
            (interaction-log-mode -1)
          (interaction-log-mode +1))
      (when interaction-log-mode
        (open-interaction-log))))
  :bind (("C-h C-l" . 'toggle-interaction-logging)))


(provide 'pkg-persistent-scratch)
