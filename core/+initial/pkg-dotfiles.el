;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; THIS FILE IS DEDICATED TO BOOTSTRAPPING THE DOTFILES SETUP
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; KNOWN-HOSTS FILE SETUP
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


(defun ensure-known-hosts (fqdn)
  "Add a fqdn's to ssh key to ~/.ssh/known_hosts if not already there.
Example:
(ensure-known-hosts \"gitlab.com\")
"
  (progn
    (let
	      ((marker-file  (format "%sssh-key-known-%s" user-emacs-directory fqdn)))
	    (when (not (file-exists-p marker-file))
	      (let
	          ((result (string-trim
			                (shell-command-to-string
			                 (format "ssh-keygen -l -F %s || echo " fqdn)))))
	        (when (string-empty-p result)
	          (shell-command-to-string
	           (format
		          "ssh-keyscan -t rsa %s  >> ~/.ssh/known_hosts && touch %s" fqdn marker-file ))))))))

;; ;; TODO: find a better way to do this
;; (ensure-known-hosts "gitlab.com")
;; (ensure-known-hosts "github.com")


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; .ZSHRC SETUP
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; ;; TODO: come up with a better solution for doing this that does not use el-get
;; (setq *zshrc-includes-file* (format "%sel-get/dotfiles/zsh/.zshrc-includes" user-emacs-directory))
;; (setq *zshrc-iterm-file* (format "%sel-get/dotfiles/zsh/.zshrc-iterm" user-emacs-directory))

;; ;; (setq *zshrc-file* (cond (*is-a-mac* (format "%sel-get/dotfiles/macos/.zshrc" user-emacs-directory))
;; ;;       (*is-a-linux* (format "%sel-get/dotfiles/linux/dotfiles/.zshrc" user-emacs-directory))))

;; (when (not (file-exists-p "~/.zshrc-local"))
;;   (progn
;;     (shell-command "touch ~/.zshrc-local")
;;     (shell-command
;;      (format "echo 'source %s' >> ~/.zshrc" *zshrc-includes-file*))
;;     (shell-command
;;      (format "echo 'source ~/.zshrc-local' >> ~/.zshrc" *zshrc-includes-file*))
;;     (shell-command
;;      (format "echo 'source %s' >> ~/.zshrc" *zshrc-iterm-file*))))

(provide 'pkg-dotfiles)
