;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	PACKAGE SETUP AND USE-PACKAGE INITIALIZATION
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; NOTE: CREATE MIRROR OF PACKAGES WITH: https://github.com/redguardtoo/elpa-mirror

(setq use-package-always-ensure t)
;; (setq use-package-always-defer 1)
;; (setq use-package-always-defer t)
(setq use-package-enable-imenu-support t)
(setq use-package-minimum-reported-time 1)
;; enable to see loading stats
(setq use-package-verbose t)
(setq use-package-compute-statistics t)






;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	BOOSTRAP USE-PACKAGE WITH STRAIGHT
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(progn ; `straight.el'
  (defvar bootstrap-version)
  (let ((bootstrap-file
         (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
        (bootstrap-version 5))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer
          (url-retrieve-synchronously
           "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
           'silent 'inhibit-cookies)
        (goto-char (point-max))
        (eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage))

  (setq straight-use-package-by-default t)
  (straight-use-package 'use-package))

;; ;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; ;;	BOOTSTRAP PACKAGE.EL AND DEFINE PACKAGE ARCHIVE SOURCES
;; ;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; (progn ; `package.el'
;;   (require 'package)
;;   (setq package-enable-at-startup nil)
;;   ;; (add-to-list 'package-archives '("elpy" . "http://jorgenschaefer.github.io/packages/"))
;;   (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
;;   (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
;;   (add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
;;   (package-initialize))

;; (progn ; `use-package'
;;   (setq use-package-always-ensure t)
;;   ;; NOTE: has major impact on speed of loading

;;   ;; (setq use-package-always-defer 1)
;;   ;; (setq use-package-always-defer t)

;;   (setq use-package-enable-imenu-support t)
;;   (setq use-package-minimum-reported-time 1)

;;   ;; enable to see loading stats
;;   (setq use-package-verbose t)
;;   (setq use-package-compute-statistics t)
;;   )

;; ;; (require 'use-package)

;; ;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; ;;	USE-PACKAGE WITH QUELPA-USE-PACKAGE
;; ;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; (unless (package-installed-p 'use-package)
;;   (package-refresh-contents)
;;   (package-install 'quelpa)
;;   (package-install 'use-package)
;;   (package-install 'quelpa-use-package))

;; ;; don't update quelpa melpa on init (for speed)
;; (defvar quelpa-update-melpa-p nil)
;; ;; (setq use-package-ensure-function 'quelpa) ;; https://github.com/quelpa/quelpa-use-package
;; (require 'quelpa-use-package)

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	ENSURE KEYCHORDS
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(use-package use-package-chords
  :demand t
  :config (key-chord-mode 1))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; PACKAGE ENRICHMENT AND HELPERS
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(use-package paradox
  :config (paradox-enable))

(use-package try)

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	FEATURE LOADING, SEQUENCING, AND CONTROL MACROS
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; REF: https://www.emacswiki.org/emacs/LibraryDependencies

(defmacro when-feature-loaded (feature &rest body)
  "When FEATURE is loaded, evaluate and execute BODY."
  `(when (featurep ,feature) ,@body))

(defmacro unless-feature-loaded (feature &rest body)
  "Unless FEATURE is loaded, evaluate and execute BODY."
  `(unless (featurep ,feature) ,@body))

(defmacro when-feature-loadable (feature &rest body)
  "If FEATURE has been or can be loaded, evaluate and execute BODY."
  `(when (require ,feature nil t) ,@body))

(defmacro unless-feature-loadable (feature &rest body)
  "If cannot be loaded, evaluate and execute BODY."
  `(unless (require ,feature nil t) ,@body))



(provide 'pkg-repo-init)
