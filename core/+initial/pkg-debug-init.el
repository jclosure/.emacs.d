;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	INIT DEBUGGING
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; ;; use to find out which pkg, feature, code loaded another and when during init
;; ;; comment in/out to use/unuse:
;; ;; use with --debug-init
;; (eval-after-load "aggressive-indent"
;;   '(debug))

;; read this anytime you need to debug a function
;; https://endlessparentheses.com/debugging-emacs-lisp-part-1-earn-your-independence.html

;; turn on when doing dev on startup
;; it's better here to just use emacs -nw --debug-init
;; (setq debug-on-error t)

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	STARTUP PROFILING
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; usage: https://github.com/jschaf/esup#usage
;; M-x esup
(use-package esup)

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	TIMING
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; use a hook so the message doesn't get clobbered by other messages.
(add-hook 'emacs-startup-hook
	        (lambda ()
	          (message "Emacs ready in %s with %d garbage collections."
		                 (format "%.2f seconds"
			                       (float-time
			                        (time-subtract after-init-time before-init-time)))
		                 gcs-done)))


(provide 'pkg-debug-init)
