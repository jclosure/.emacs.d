;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; ELFEED SUPPORT
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(defun start-elfeed (&optional frame)
  "This function will start elfeed and update it.  I think it needs FRAME Passed to it."
  (if (not (get-buffer "*elfeed-search*"))
      (progn
        (elfeed)
        (while (not (get-buffer "*elfeed-search*")))
        (switch-to-buffer "*elfeed-search*"))))


;; register feeds and tag them
(setq elfeed-feeds (quote
                    (("https://www.reddit.com/r/linux.rss" reddit linux)
                     ("https://www.reddit.com/r/commandline.rss" reddit linux)
                     ("https://www.reddit.com/r/emacs.rss" reddit linux)
                     ("https://www.gamingonlinux.com/article_rss.php" gaming linux)
                     ("https://hackaday.com/blog/feed/" hackaday linux)
                     ("https://opensource.com/feed" opensource linux)
                     ("https://linux.softpedia.com/backend.xml" softpedia linux)
                     ("https://itsfoss.com/feed/" itsfoss linux)
                     ("https://www.zdnet.com/topic/linux/rss.xml" zdnet linux)
                     ("https://www.phoronix.com/rss.php" phoronix linux)
                     ("http://feeds.feedburner.com/d0od" omgubuntu linux)
                     ("https://www.computerworld.com/index.rss" computerworld linux)
                     ("https://www.networkworld.com/category/linux/index.rss" networkworld linux)
                     ("https://www.techrepublic.com/rssfeeds/topic/open-source/" techrepublic linux)
                     ("https://betanews.com/feed" betanews linux)
                     ("http://lxer.com/module/newswire/headlines.rss" lxer linux)
                     ("https://distrowatch.com/news/dwd.xml" distrowatch linux))))

(use-package elfeed
  :commands (elfeed)
  :config (require 'elfeed)
  ;; use eww as the default browser for all of Emacs
  ;; (setq browse-url-browser-function 'eww-browse-url)
  )

(use-package elfeed-protocol
  :hook after-init
  :after elfeed
  :config
  (elfeed-protocol-enable))

(use-package password-store
  :hook after-init
  :after elfeed-protocol)

(use-package elfeed-goodies
  :demand t
  :after elfeed
  :config
  (require 'elfeed-goodies)
  (elfeed-goodies/setup)
  (setq elfeed-goodies/entry-pane-position 'bottom
        elfeed-goodies/entry-pane-size 0.5
        elfeed-goodies/switch-to-entry nil)
  (start-elfeed))

;; (use-package elfeed-org
;;   :demand t
;;   :after org
;;   :config
;;   (elfeed-org)
;;   (setq rmh-elfeed-org-files
;;         (list (concat org-directory "/feeds.org"))))


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;; NOTE: See https://github.com/skeeto/elfeed/issues/293

;; (defvar ap/elfeed-update-complete-hook nil
;;   "Functions called with no arguments when `elfeed-update' is finished.")

;; (defvar ap/elfeed-updates-in-progress 0
;;   "Number of feed updates in-progress.")

;; (defvar ap/elfeed-search-update-filter nil
;;   "The filter when `elfeed-update' is called.")

;; (defun ap/elfeed-update-complete-hook (&rest ignore)
;;   "When update queue is empty, run `ap/elfeed-update-complete-hook' functions."
;;   (when (= 0 ap/elfeed-updates-in-progress)
;;     (run-hooks 'ap/elfeed-update-complete-hook)))

;; (add-hook 'elfeed-update-hooks #'ap/elfeed-update-complete-hook)

;; (defun ap/elfeed-update-message-completed (&rest _ignore)
;;   (message "Feeds updated"))

;; (add-hook 'ap/elfeed-update-complete-hook #'ap/elfeed-update-message-completed)

;; (defun ap/elfeed-search-update-restore-filter (&rest ignore)
;;   "Restore filter after feeds update."
;;   (when ap/elfeed-search-update-filter
;;     (elfeed-search-set-filter ap/elfeed-search-update-filter)
;;     (setq ap/elfeed-search-update-filter nil)))

;; (add-hook 'ap/elfeed-update-complete-hook #'ap/elfeed-search-update-restore-filter)

;; (defun ap/elfeed-search-update-save-filter (&rest ignore)
;;   "Save and change the filter while updating."
;;   (setq ap/elfeed-search-update-filter elfeed-search-filter)
;;   (setq elfeed-search-filter "#0"))

;; ;; NOTE: It would be better if this hook were run before starting the feed updates, but in
;; ;; `elfeed-update', it happens afterward.
;; (add-hook 'elfeed-update-init-hooks #'ap/elfeed-search-update-save-filter)

;; (defun ap/elfeed-update-counter-inc (&rest ignore)
;;   (cl-incf ap/elfeed-updates-in-progress))

;; (advice-add #'elfeed-update-feed :before #'ap/elfeed-update-counter-inc)

;; (defun ap/elfeed-update-counter-dec (&rest ignore)
;;   (cl-decf ap/elfeed-updates-in-progress)
;;   (when (< ap/elfeed-updates-in-progress 0)
;;     ;; Just in case
;;     (setq ap/elfeed-updates-in-progress 0)))

;; (add-hook 'elfeed-update-hooks #'ap/elfeed-update-counter-dec)

(provide 'pkg-feeds)
