;; (require 'pkg-org-mode)
;; (require 'pkg-publish)

;; TODO: consider using smartparens and disabling electric-pair-mode
(add-hook
 'org-mode-hook
 (lambda ()
   (setq-local
    ;; disable <> auto pairing in electric-pair-mode for org-mode
    electric-pair-inhibit-predicate
    `(lambda (c)
       (if (char-equal c ?<) t (,electric-pair-inhibit-predicate c)))
    ;; scale latex fonts to twice the size
    org-format-latex-options (plist-put org-format-latex-options :scale 2.0)
    )))

;; read live - http://emacslife.com/baby-steps-org.html
;; https://orgmode.org/manual/Effort-Estimates.html
;; https://github.com/pokeefe/Settings/blob/master/emacs-settings/.emacs.d/modules/init-org.el
;; https://webcache.googleusercontent.com/search?q=cache:Trsx3j6ie1EJ:https://ppalazon.github.io/2018/04/11/my-org-mode-configuration/+&cd=6&hl=en&ct=clnk&gl=us

;; This library provides tools to manipulate durations.  A duration
;; can have multiple formats:
;;
;;   - 3:12
;;   - 1:23:45
;;   - 1y 3d 3h 4min
;;   - 3d 13:35
;;   - 2.35h

;; TODO: make this a customization option
;; (setq org-directory "~/Google Drive File Stream/My Drive/Org")
;; (setq org-directory (expand-file-name "org" user-emacs-directory))
(setq org-directory (or (getenv "EMACS_ORG_DIR")
                        (expand-file-name "org" "~")))

;; disables subscripts as _, instead use _{foo}
(setq org-use-sub-superscripts '{})


;; TODO: convert to use-package
(with-eval-after-load "org"
  ;; bind missing keys
  (org-defkey org-mode-map "\C-c\C-x\C-r" 'org-clock-report)

  ;; setup org-crypt
  (require 'org-crypt)
  (require 'epa-file)
  (epa-file-enable)
  (org-crypt-use-before-save-magic)
  (setq org-tags-exclude-from-inheritance (quote ("crypt")))
  ;; GPG key to use for encryption - set to nil for symmetric
  ;; (setq org-crypt-key nil)
  ;; IMPORTANT: this key will timeout soon!
  ;; TODO: set this globally?

  ;; set from env var: EMACS_DEFAULT_GPG_KEY
  (setq org-crypt-key emacs-default-gpg-key)

  ;; show links as verbose
  (setq org-descriptive-links t)

  ;; disable autosave in org files
  (defun autosave-disable ()
    (auto-save-mode -1))
  (add-hook 'org-mode-hook #'autosave-disable)

  ;; Enable clock persistence
  (setq org-clock-persist 'history)
  (org-clock-persistence-insinuate)
  (setq org-clock-persist-file (concat user-emacs-directory "var/org-lock-save.el"))

  (if (file-exists-p org-clock-persist-file)
      ;; (setq org-clock-persist 'history)
      (org-clock-persistence-insinuate)
    (shell-command (concat "touch " org-clock-persist-file)))

  ;; Resume clocking task on clock-in if the clock is open
  (setq org-clock-in-resume t)
  ;; Do not prompt to resume an active clock, just resume it
  (setq org-clock-persist-query-resume nil)
  ;; Change tasks to whatever when clocking in
  (setq org-clock-in-switch-to-state "NEXT")
  ;; Save clock data and state changes and notes in the LOGBOOK drawer
  (setq org-clock-into-drawer t)
  ;; Sometimes I change tasks I'm clocking quickly - this removes clocked tasks
  ;; with 0:00 duration
  (setq org-clock-out-remove-zero-time-clocks t)
  ;; Clock out when moving task to a done state
  (setq org-clock-out-when-done t)
  ;; Enable auto clock resolution for finding open clocks
  (setq org-clock-auto-clock-resolution (quote when-no-clock-is-running))
  ;; Include current clocking task in clock reports
  (setq org-clock-report-include-clocking-task t)
  ;; use pretty things for the clocktable
  (setq org-pretty-entities t)

  ;; probably use org-journal instead
  ;; (setq org-agenda-include-diary t)


  ;; org related variables
  (setq org-agenda-span '30
        org-agenda-restore-windows-after-quit t
        org-agenda-show-all-dates t
        org-agenda-skip-deadline-if-done t
        org-agenda-skip-scheduled-if-done t
        org-agenda-start-on-weekday t
        org-deadline-warning-days 3
        org-fast-tag-selection-single-key nil
        org-reverse-note-order nil
        org-tags-match-list-sublevels nil
        org-use-fast-todo-selection t
        org-use-tag-inheritance nil)


  (setq
   ;; globally indent all headlines
   org-startup-indented t
   ;; initial folding paradigm
   org-startup-folded 'content
   )

  ;; Log the time when a TODO item was finished
  (setq org-log-done 'time)

  ;; Specify global tags with fast tag selection
  (setq org-tag-alist '((:startgroup . nil) ("@office" . ?o) ("@home" . ?h) (:endgroup . nil)
                        ("computer" . ?c) ("reading" . ?r) ("shopping" . ?g) ("jobs" . ?w) ("research" . ?r)))


  ;; Effort and global properties
  (setq org-global-properties '(("Effort_ALL". "0 0:10 0:20 0:30 1:00 2:00 3:00 4:00 6:00 8:00")))

  ;; Set global Column View format
  (setq org-columns-default-format '"%38ITEM(Details) %TAGS(Context) %7TODO(To Do) %5Effort(Time){:} %6CLOCKSUM(Clock)")

  ;; Setup custom todo keywords with shortcuts
  (setq org-todo-keywords
        '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)" "DELEGATED(D)")
          (sequence "REPORT(r)" "BUG(b)" "KNOWNCAUSE(k)" "VERIFY(v)" "|" "FIXED(f)")
          (sequence "|" "CANCELED(c)")))


  ;; ;; Customizea org-clocktable-defaults
  (setq org-agenda-clockreport-parameter-plist '(:link t :maxlevel 5))
  (setq org-clock-clocktable-default-properties '(:maxlevel 5 :scope subtree))



  (setq holiday-bahai-holidays nil
        ;; holiday-christian-holidays nil
        holiday-hebrew-holidays nil
        holiday-islamic-holidays nil
        holiday-oriental-holidays nil)

  ;; If idle for more than 15 minutes, resolve the things by asking what to do
  ;; with the clock time
  (setq org-clock-idle-time 15)

  (setq org-time-stamp-rounding-minutes (quote (0 5)))

  ;; ;; org-capture
  ;; ;; FIXME: doesn't work.  stringp error.  Get capture working from anywhere to specific files.
  ;; (progn
  ;;   (setq org-default-notes-file (concat org-directory "/notes.org"))
  ;;   (define-key global-map "\C-cc" 'org-capture)

  ;;   (setq org-capture-templates
  ;;         (quote (("t" "todo" entry (file (concat org-directory "/todo.org.gpg"))
  ;;                  "* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)
  ;;                 ("n" "note" entry (file (concat org-directory "/notes.org"))
  ;;                  "* %? :NOTE:\n%U\n%a\n" :clock-in t :clock-resume t)
  ;;                 ("j" "Journal" entry (file+datetree (concat org-directory "/diary.org"))
  ;;                  "* %?\n%U\n" :clock-in t :clock-resume t)
  ;;                 ))))

  )



  ;; GPG key to use for encryption
  ;; Either the Key ID or set to nil to use symmetric encryption.

  ;; TODO: set this on a org-mode-hook so it's buffer local
  ;; (setq auto-save-default nil)

  ;; Auto-saving does not cooperate with org-crypt.el: so you need
  ;; to turn it off if you plan to use org-crypt.el quite often.
  ;; Otherwise, you'll get an (annoying) message each time you
  ;; start Org.

  ;; To turn it off only locally, you can insert this:
  ;;
  ;; # -*- buffer-auto-save-file-name: nil; -*-)


;; (use-package org-super-agenda
;;   :after org-agenda
;;   ;; :demand t
;;   :hook org-mode
;;   :init
;;   (setq org-super-agenda-groups
;;         '((:name "Today"
;; 				         :time-grid t
;; 				         :scheduled today)
;; 			    (:name "Due today"
;; 				         :deadline today)
;; 			    (:name "Important"
;; 				         :priority "A")
;; 			    (:name "Overdue"
;; 				         :deadline past)
;; 			    (:name "Due soon"
;; 				         :deadline future)
;; 			    (:name "Waiting"
;; 			           :todo "WAIT")))
;;   :config
;;   (org-super-agenda-mode 1)
;;   ;; Agenda clock report parameters
;; )


;; set directories for agenda files (used for refile vars below)
(setq org-agenda-files `(,(concat org-directory "/todo.org.gpg")
                         ,(concat org-directory "/capture")
                         ,(concat org-directory "/work")
                         ,(concat org-directory "/notes")
                         ,(concat org-directory "/study")
                         ))


;; (use-package org-agenda
;;   :ensure nil
;;   :bind ("C-c a" . org-agenda)
;;   :init
;;   ;; agenda clock report format
;;   (setq org-agenda-clockreport-parameter-plist
;;         '(:link t :maxlevel 6 :fileskip0 t :compact t :narrow 60 :score 0))
;;   :config
;;   ;; allow gpg files into agenda
;;   (unless (string-match-p "\\.gpg" org-agenda-file-regexp)
;;     (setq org-agenda-file-regexp
;;           (replace-regexp-in-string "\\\\\\.org" "\\\\.org\\\\(\\\\.gpg\\\\)?"
;;                                     org-agenda-file-regexp)))



;;   ;; TODO: add file permanently to agenda

;;   ;; ADD A CLOCK PROGRAMMATICALLY TO AGENDA
;;   ;; (defun own-clock-report ()
;;   ;;   "function to insert clocktable"
;;   ;;   (goto-char (point-max))
;;   ;;   (let ((org-agenda-files (org-agenda-files nil 'ifmode))
;;   ;;         ;; the above line is to ensure the restricted range!
;;   ;;         (p (copy-sequence org-agenda-clockreport-parameter-plist))
;;   ;;         tbl)
;;   ;;     (setq p (org-plist-delete p :block))
;;   ;;     (setq p (plist-put p :tstart "<today>"))
;;   ;;     (setq p (plist-put p :tend "<now>"))
;;   ;;     (setq p (plist-put p :scope 'agenda))
;;   ;;     (setq tbl (apply 'org-clock-get-clocktable p))
;;   ;;     (open-line 1)
;;   ;;     (insert tbl)))

;;   ;; (add-hook 'org-agenda-hook #'own-clock-report)
;;   ;; (add-hook 'org-agenda-finalize-hook #'own-clock-report)
;;  )



;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	ORG-BABEL
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(setq org-babel-python-command "python3")

;; org bable playground
;; (require 'ob-C)
;; (require 'ob-js)
;; (require 'ob-python)
;; (require 'ob-shell)
;; (require 'ob-java)
;; ;;(require 'ob-scala)
;; (require 'ob-plantuml)
;; (require 'ob-R)
;; (require 'ob-redis)



;; disable security confirmations
(setq org-confirm-babel-evaluate nil)

;; NOTE THIS REPLACES 'org
(use-package org
  :after jupyter
  :straight (:includes (org-plus-contrib))
  :config
  (progn
    (org-babel-do-load-languages
     'org-babel-load-languages
     '((plantuml . t)
       (C . t)
       (R . t)
       (js . t)
       (python . t)
       (shell . t)
       (java . t)
       (sql . t)
       (jupyter . t)
       ;; (jq . t)
       ;; (redis . t)
       ;; (scala . t))
       ))
    ;; (require 'org-eldoc)
    (require 'org-tempo)
    ;; (straight-use-package '(org-plus-contrib :includes (org)))

    ;; wrap text at column 80 with M-q
    (add-hook 'org-mode-hook (lambda nil
                               (auto-fill-mode 1)
                               (set-fill-column 80)))
    ))


;; for org-babel
;; TODO: try latest ein instead
(use-package jupyter
  :demand  t
  ;; :build (:not native-compile)
  :straight (:no-byte-compile t :no-native-compile t)
  :init
  (setq jupyter-repl-echo-eval-p t)
  :config
  ;; alias python to jupyter-python
  ;; NOTE: must be  AFTER org-babel-do-load-languages
  (org-babel-jupyter-override-src-block "python"))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	ROAM
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; (use-package gkroam
;;   :ensure t
;;   :init
;;   (setq gkroam-root-dir "~/gkroam/org/"
;;         gkroam-pub-dir "~/gkroam/site/")
;;   :bind
;;   (("C-c r G" . gkroam-update-all)
;;    ("C-c r g" . gkroam-update)
;;    ("C-c r d" . gkroam-daily)
;;    ("C-c r f" . gkroam-find)
;;    ("C-c r c" . gkroam-capture)
;;    ("C-c r e" . gkroam-link-edit)
;;    ("C-c r n" . gkroam-smart-new)
;;    ("C-c r i" . gkroam-insert)
;;    ("C-c r I" . gkroam-index)
;;    ("C-c r p" . gkroam-preview)
;;    ("C-c r v" . gkroam-preview-current)
;;    ("C-c r t" . gkroam-toggle-brackets))
;;   :config
;;   ;; when this minor mode is on, show and hide brackets dynamically.
;;   ;; Here we turn it off.
;;   (gkroam-dynamic-brackets-mode -1))


(use-package org-roam
  :ensure t
  :init
  (setq org-roam-v2-ack t
        org-roam-verbose nil
        ;; Make org-roam buffer sticky; i.e. don't replace it when opening a
        ;; file with an *-other-window command.
        org-roam-buffer-window-parameters '((no-delete-other-windows . t)))
  :custom
  (org-roam-directory (expand-file-name "notes" org-directory))
  (org-roam-completion-everywhere t)
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert)
         :map org-mode-map
         ("C-M-i"    . completion-at-point))
  :config
  (org-roam-setup))


;; start with: M-x org-roam-ui-mode RET
;; http://127.0.0.1:35901/
(use-package org-roam-ui
  :straight
  (:host github :repo "org-roam/org-roam-ui" :branch "main" :files ("*.el" "out"))
  :after org-roam
  :hook (org-roam . org-roam-ui-mode))

;; ;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; ;;	REFILING CONFIG
;; ;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; ;; ref: https://blog.aaronbieber.com/2017/03/19/organizing-notes-with-refile.html
;; ;; use C-c C-w to trigger refile


(defvar my-org-refile-targets nil
  "List of refile targets for Org-remember.
   See `org-refile-targets'.")

(defvar my-org-refile-dir-excludes  "^[#\\.].*$")
(defvar my-org-refile-file-excludes "^[#\\.].*$")


(defun my-find-org-refile-targets
    (&optional recurse dirs file-excludes dir-excludes)
  "Fill the variable `my-org-refile-targets'.
Optional parameters:
  recurse        If `t', scan the directory recusively.
  dirs           A list of directories to scan for *.org files.
  file-excludes  Regular expression. If a filename matches this regular
expression,
                 do not add it to `my-org-refile-targets'.
  dir-excludes   Regular expression. If a directory name matches this
regular expression,
                 do not add it to `my-org-refile-targets'."
  (let ((targets (or dirs (list org-directory)))
        (fex (or file-excludes  "^[#\\.].*$"))
        (dex (or dir-excludes  "^[#\\.].*$"))
        path)
    (dolist (dir targets)
      (if (file-directory-p dir)
          (let ((all (directory-files dir nil "^[^#\\.].*$")))
            (dolist (f all)
              (setq path (concat (file-name-as-directory dir) f))
              (cond
               ((file-directory-p path)
                (if (and recurse (not (string-match dex f)))
                      (my-find-org-refile-targets t (list path) fex
dex)))
               ((and (string-match "^[^#\\.].*\\.org$" f) (not
(string-match fex f)))
                (setq my-org-refile-targets (append (list path)
my-org-refile-targets))))))
        (message "Not a directory: %s" path))
      )))



(defun my-add-to-org-refile-targets ( recurse dirs )
  "Add a directory to org-refile targets recursively."
  (interactive "P\nDdirectory: ")
  (my-find-org-refile-targets
   (if recurse t nil)
   (list dirs)
   my-org-refile-file-excludes
   my-org-refile-dir-excludes)
  (message "org-refile-targets: \n%s" my-org-refile-targets))




(setq org-refile-targets '(
                           ("~/org" :maxlevel . 3 )
                           ))
(setq org-refile-use-outline-path 'file)
;; (setq org-outline-path-complete-in-steps nil)
;; (setq org-refile-allow-creating-parent-nodes 'confirm)


(use-package helm-org-rifle)

;; (setq org-refile-targets
;;       '((nil :maxlevel . 3)
;;         (("todo.org"
;;           "done.org") :maxlevel . 3)))


;; ;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; ;; CONFLICTS WITH ORG
;; ;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; ;; ref: https://orgmode.org/manual/Conflicts.html
;; ;; Make windmove work in Org mode:

;; (add-hook 'org-shiftup-final-hook 'windmove-up)
;; (add-hook 'org-shiftleft-final-hook 'windmove-left)
;; (add-hook 'org-shiftdown-final-hook 'windmove-down)
;; (add-hook 'org-shiftright-final-hook 'windmove-right)

;; LEVERAGE THE POWER OF org-replace-disputed-keys here
;; (setq org-support-shift-select t
;;       org-replace-disputed-keys t)
;; (setq org-disputed-keys
;;       '(
;;         ([(shift left)]          . [(meta -)])         ; change status (todo/closed/done)
;;         ([(shift right)]         . [(meta =)])         ;
;;         ([(shift up)]            . [(control meta -)]) ; change priority
;;         ([(shift down)]          . [(control meta =)]) ;
;;         ([(control shift right)] . [(meta +)])         ; status of group
;;         ([(control shift left)]  . [(meta _)])         ;
;;         ([(control shift up)]    . [(control meta +)]) ; change clock logs
;;         ([(control shift down)]  . [(control meta _)]) ;
;;         ))

;; NOTE: handled in core file pkg-emacs-windows.el

;; INCLUDE OTHER LOCAL PACKAGES
(require 'pkg-org-journal)


;; #+SETUPFILE: template.org
(use-package autoinsert
  :after (org yasnippet)
  :init
  ;; Don't want to be prompted before insertion:
  (setq auto-insert-query nil)

  (setq auto-insert-directory (concat user-emacs-directory "insert"))
  (add-hook 'find-file-hook 'auto-insert)
  (auto-insert-mode 1)

  :config
  ;; there  are  2 ways  to register auto-insert rules

  ;; 1. for use with use static templates only
  ;; (define-auto-insert "\\.org?$" "template.org")

  ;; 2. use templates with variables
  (defun my/autoinsert-yas-expand ()
    "Replace text in yasnippet template."
    (yas-expand-snippet (buffer-string) (point-min) (point-max)))


  (add-to-list 'auto-insert-alist '(("\\.py\\'" . "Python souce code header") .
                                    ["template.py" my/autoinsert-yas-expand]))

  (add-to-list 'auto-insert-alist '(("\\.\\([Hh]\\|hh\\|hpp\\)\\'" . "C / C++ header") .
                                    ["template.h" my/autoinsert-yas-expand]))

  (add-to-list 'auto-insert-alist '(("\\.\\([Cc]\\|cc\\|cpp\\)\\'" . "C / C++ program") .
                                    ["template.c" my/autoinsert-yas-expand]))

  (add-to-list 'auto-insert-alist '(("\\.el\\'" . "Emacs Lisp package") .
                                    ["template.el" my/autoinsert-yas-expand]))


  )



(provide 'orgs)
