

(use-package org-journal
  :after org
  :init
  ;; Change default prefix key; needs to be set before loading org-journal
  (setq org-journal-prefix-key "C-c j ")
  :config
  (setq org-journal-dir (concat org-directory "/journal/")
        org-journal-date-format "%A, %d %B %Y"))


(provide 'pkg-org-journal)
