;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	ORG-SUPER-AGENDA CONFIG
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; TODO: try this config: https://www.youtube.com/watch?v=hZpDdbflt8c
(use-package org-super-agenda
  :after org-agenda
  :init
  (setq org-super-agenda-groups
        '((:name "Today"
				         :time-grid t
				         :scheduled today)
			    (:name "Due today"
				         :deadline today)
			    (:name "Important"
				         :priority "A")
			    (:name "Overdue"
				         :deadline past)
			    (:name "Due soon"
				         :deadline future)
			    (:name "Waiting"
			           :todo "WAIT")))
  :config
  (org-super-agenda-mode 1))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	Org TODO keywords
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(setq org-todo-keywords
      '((sequence "TODO(t)" "INPROGRESS(i)" "WAITING(w)" "REVIEW(r)" "|" "DONE(d)" "CANCELLED(c)")))

(setq org-todo-keyword-faces
      '(("TODO" . org-warning)
	("INPROGRESS" . "yellow")
	("WAITING" . "purple")
	("REVIEW" . "orange")
	("DONE" . "green")
	("CANCELLED" .  "red")))

;; close todo with note
(setq org-log-done 'note)
(setq org-startup-truncated t)

(setq org-log-into-drawer t)
(setq org-agenda-custom-commands
      '(("b" "Blog idea" tags-todo "BLOG")
	("s" "Someday" todo "SOMEDAY")
	("S" "Started" todo "STARTED")
	("w" "Waiting" todo "WAITING")
	("d" . " Task arrangement ")
	("da" " Important and urgent tasks " tags-todo "+PRIORITY=\"A\"")
	("db" " Important and not urgent tasks " tags-todo "+PRIORITY=\"B\"")
	("dc" " Unimportant and urgent tasks " tags-todo "+PRIORITY=\"C\"")
	("p" . " Project arrangement ")
	("W" "Weekly Review" tags-todo "PROJECT")))

;; add the org-pomodoro-mode-line to the global-mode-string
(unless global-mode-string (setq global-mode-string '("")))
(unless (memq 'org-pomodoro-mode-line global-mode-string)
  (setq global-mode-string (append global-mode-string
                                   '(org-pomodoro-mode-line))))

(add-hook 'org-mode-hook '(lambda () (setq fill-column 80)))
(add-hook 'org-mode-hook 'turn-on-auto-fill)

;; auto toggle todo status
(defun org-summary-todo (n-done n-not-done)
  "Switch entry to DONE when all subentries are done, to TODO otherwise."
  (let (org-log-done org-log-states)   ; turn off logging
    (org-todo (if (= n-not-done 0) "DONE" "TODO"))))
(add-hook 'org-after-todo-statistics-hook 'org-summary-todo)

;; pomodoro tech setting
(use-package org-pomodoro
  :ensure t
  :commands (org-pomodoro)
  :config
  (setq alert-user-configuration (quote ((((:category . "org-pomodoro")) libnotify nil))))
  (setq org-pomodoro-length 25)
  (setq org-pomodoro-short-break-length 5)
  (setq org-pomodoro-long-break-length 15)
  (setq org-pomodoro-format " ⏰  %s ")
  (setq org-pomodoro-long-break-format "  ☕☕☕  %s ")
  (setq org-pomodoro-short-break-format "  ☕  %s ")
  (setq org-pomodoro-ticking-sound-p nil)
  )

;; change a good appearance of todo items
(use-package org-bullets
  :config
  (progn
    (setq org-bullets-bullet-list '("☯" "✿" "✚" "◉" "❀"))
    (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
    ))

(use-package org-alert
  
  :config
  (progn
    (setq alert-default-style 'libnotify)
    ))

;; org files include in agenda view

(defvar org-icloud-path (concat (expand-file-name "~") "/Documents/"))
(defvar ep-work-org (concat org-icloud-path "work.org"))
(defvar ep-learning-org (concat org-icloud-path "learning.org"))
(defvar ep-personal-org (concat org-icloud-path "personal.org"))
(defvar ep-inbox-org (concat org-icloud-path "inbox.org"))
(defvar ep-habit-org (concat org-icloud-path "habit.org"))



(defun touch-file (fname)
  "Takes a symbol, converts to string, and touches it"
    (unless (file-exists-p fname)
      (shell-command (concat "touch " fname))))

(touch-file  ep-work-org)
(touch-file  ep-learning-org)
(touch-file  ep-personal-org)
(touch-file  ep-inbox-org)
(touch-file  ep-habit-org)

(setq org-agenda-files (list ep-work-org
			     ep-learning-org
			     ep-personal-org
			     ep-inbox-org
			     ep-habit-org))
(setq org-archive-location (concat  (concat (expand-file-name "~") "/Documents/Archive/Orgs/") "%s_archive::"))

;; org caputer settings
(setq org-default-notes-file ep-inbox-org)
(setq org-startup-with-inline-images t)
(define-key global-map "\C-cc" 'org-capture)

(setq org-capture-templates
      '(("t" "Todo" entry (file+headline ep-inbox-org "To do")
         "* TODO %?\n %i\n")))

(setq org-refile-targets
      '((ep-learning-org :maxlevel . 1)
        (ep-work-org :maxlevel . 1)
	(ep-personal-org :maxlevel . 1)
	(ep-habit-org :maxlevel . 1)))

;; open agenda view when emacs startup
(defun agenda-view()
  (interactive)
  (progn
    (kill-current-buffer)
    (org-agenda 'a "a")
    (delete-other-windows)))

;; open agenda view every hour
;; (run-at-time "0 sec" 3600 'agenda-view)

;; org bable playground
(require 'ob-C)
(require 'ob-js)
(require 'ob-python)
(require 'ob-shell)
(require 'ob-java)
;;(require 'ob-scala)
(require 'ob-plantuml)
(require 'ob-R)
;; (require 'ob-redis)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((plantuml . t)
   (C . t)
   (R . t)
   (js . t)
   (python . t)
   (shell . t)
   (java . t)
   (elasticsearch . t)
   ;; (redis . t)
   ;; (scala . t))
 ))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	Org-roam Settings
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(setq org-return-follows-link t)

(use-package org-roam
  :demand t
  :hook ((after-init . org-roam-mode)
         (org-mode . my/org-roam-setup))
  :init
  (progn
    (setq org-roam-directory "~/org-roam")
    (setq org-roam-db-update-method 'immediate)
    (make-directory org-roam-directory t))
  :config
  (progn
    (defun my/org-roam-setup ()
      "Initialize an org-roam buffer."
      (when (and (buffer-file-name) (string-prefix-p org-roam-directory (buffer-file-name)))
        ;; Recommended by org-roam manual:
        (setq-local company-backends (cons 'company-capf company-backends))
        (company-mode)))))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	Org Latex Settings
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


;; make all latex exports and previews font twice as big as normal
(require 'org)
(add-hook 'org-mode-hook #'(lambda ()
                             (setq org-format-latex-options (plist-put org-format-latex-options :scale 2.0))))


(provide 'pkg-org-mode)
