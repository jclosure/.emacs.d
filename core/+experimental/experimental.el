;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; SUBLIMITY SCROLL SUPPORT
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; (use-package sublimity
;;   ;; :hook after-init
;;   :demand t
;;   :config
;;   (progn
;;     (require 'sublimity)
;;     (require 'sublimity-scroll)
;;     ;; (require 'sublimity-map) ;; experimental
;;     (require 'sublimity-attractive)
;;     (sublimity-mode 1)
;;     (setq scroll-conservatively 101) ; as long as it's > 100
;;     (setq scroll-preserve-screen-position t)))


(use-package hide-mode-line
  :hook after-init
  :config
  (add-hook 'elfeed-show-mode-hook #'hide-mode-line-mode))

(setq enable-experimental-packages t)

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; TMUX SUPPORT
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(use-package emamux
  :general
  (:prefix "<f12>" ; TODO: make a non fn key leader
           "n" 'emamux:new-window
           "s" 'emamux:send-region
           "r" 'emamux:run-command))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; RESTART EMACS
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; Sometimes the best fix is turning it off and on again
(use-package restart-emacs
  :bind (("C-x M-r" . restart-emacs))
  :init
  (setq restart-emacs-restore-frames t))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; IRC SUPPORT
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(use-package erc
  :init (progn
          (setq erc-nick "jclosure"
                erc-autojoin-channels-alist '(("freenode.net" . ("#emacs"))))))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; TORRENT SUPPORT
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(use-package transmission
  :config (progn
            (defun transmission-add-magnet (url)
              "Like `transmission-add', but with no file completion."
              (interactive "sMagnet url: ")
              (transmission-add url))))




;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; LATEX SUPPORT
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; TODO: put this into a latex support pkg file
(when *is-a-mac*
  (setenv "PATH" (concat "/Library/TeX/texbin:" (getenv "PATH")))
  (setq exec-path (append '("/Library/TeX/texbin") exec-path)))

(use-package pdf-tools
  :hook after-init
  :config
  (pdf-loader-install)
  (setq pdf-view-use-scaling t
        pdf-view-use-imagemagick nil))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; EXPERIMENTAL PACKAGES AND CONFIG ONLY
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; (add-hook 'text-mode-hook 'block-indent-mode)
;; (add-hook 'elixir-mode-hook
;;           (lambda ()
;;             ;; (electric-indent-mode -1)
;;             (setq indent-region-function 'classic-indent-right-region-ignored)
;;             (setq indent-line-function 'classic-indent-right)))



;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; IPYTHON SUPPORT
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; (use-package ob-ipython
;;   :config
;;   ;; for now I am disabling elpy only ob-ipython minor mode
;;   ;; what we should actually do, is just to ensure that
;;   ;; ob-ipython's company backend comes before elpy's (TODO)
;;   (add-hook 'ob-ipython-mode-hookp
;;             (lambda ()
;;               ;; (elpy-mode 0)
;;               (company-mode 1)))
;;   (add-to-list 'company-backends 'company-ob-ipython)
;;   (add-to-list 'org-latex-minted-langs '(ipython "python")))

;; ;; manual fetch: (quelpa '(emacs-ipython-notebook :fetcher github :repo "millejoh/emacs-ipython-notebook"))
;; (use-package ein
;;   :demand t
;;   ;; :quelpa (ein :fetcher github :repo "millejoh/emacs-ipython-notebook")
;;   :commands ein:notebooklist-open
;;   :init
;;   (progn
;;     (setq ein:use-auto-complete t)
;;     (setq ein:use-smartrep t)
;;     (with-eval-after-load 'ein-notebooklist
;;       ;; removing keybindings

;;       (define-key ein:notebooklist-mode-map (kbd "M-p") nil)
;;       (define-key ein:notebooklist-mode-map (bd "<M-up>") nil)
;;       (define-key ein:notebooklist-mode-map (kbd "<M-down>") nil)
;;       ;; changing keybinding
;;       (define-key ein:notebooklist-mode-map (kbd "C-s") 'ein:notebook-save-notebook-command)
;;       (define-key ein:notebooklist-mode-map (kbd "<M-S-up>") 'ein:worksheet-move-cell-up)
;;       (define-key ein:notebooklist-mode-map (kbd "<M-S-down>") 'ein:worksheet-move-cell-down))))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; COLORIZE COMPILATION BUFFERS
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


;; (defun imalison:colorize-compilation-buffer ()
;;   (let ((was-read-only buffer-read-only))
;;     (unwind-protect
;;         (progn
;;           (when was-read-only
;;             (read-only-mode -1))
;;           (ansi-color-apply-on-region (point-min) (point-max)))
;;       (when was-read-only
;;         (read-only-mode +1)))))

;; (add-hook 'compilation-filter-hook 'imalison:colorize-compilation-buffer)



;; ;; compilation buffers should be colored
;; ;; NOTE: test this fully
;; (use-package ansi-color
;;   :init
;;   (setq compilation-scroll-output 't)   ; yes plz

;;   (defun smf/colorize-compilation ()
;;     "Colorize from `compilation-filter-start' to `point'."
;;     (let ((inhibit-read-only t))
;;       (ansi-color-apply-on-region
;;        compilation-filter-start (point))))

;;   (add-hook 'compilation-filter-hook 'smf/colorize-compilation))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; RANDOM
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


;; nice, but collides with helm-swoop C-s and C-r
;; (use-package ctrlf
;;   :config
;;   (ctrlf-mode 1))

(provide 'experimental)
