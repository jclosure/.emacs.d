;;; ELGET SETUP

(use-package el-get
  :demand t
  :config
  (progn
    (add-to-list 'el-get-recipe-path (expand-file-name "el-get-user/recipes" user-emacs-directory))

    ;; NOTE: currently using quelpa for emacswiki packages
    ;; do emacswiki init if this is the first time
    (unless (file-directory-p el-get-recipe-path-emacswiki)
      (progn
        (el-get-emacswiki-refresh el-get-recipe-path-emacswiki t)
        (el-get-emacswiki-build-local-recipes)))))


(use-package use-package-el-get
  :demand t)


;;; CUSTOM THEME OVERRIDES


;;; ELGET SETUP


;; (el-get-bundle dotfiles
;;   :url "https://gitlab.com/jclosure/dotfiles.git"
;;   :feature "init-dotfiles"
;;   :submodule nil
;;   ;;(load-file "~/.emacs.d/el-get/dotfiles/init-dotfiles.elc")
;;   )


;; ;; https://oli.me.uk/2015/03/03/making-el-get-sync-like-vim-plug/
;; ;; requires: ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts
;; (el-get-bundle dotfiles
;;   :type git
;;   :url "git@gitlab.com:jclosure/dotfiles.git"
;;   :features (init-dotfiles dotfiles general)
;;   :compile ("*\\.el$" "helpers/")
;;   :load-path ("./site-lisp" "./site-lisp/helpers"))


;; GERRIT-SUPPORT
;; TODO: get this working -> https://github.com/terranpro/magit-gerrit
;; https://emacs.stackexchange.com/questions/19672/magit-gerrit-push-to-other-branch
;; NOTE: default keys R-P collide with magit rename
;; (use-package magit-gerrit)
;; TODO: switch to gerrit management. integrate into flow. run: M-x magit-gerrit-dispatch
;; the melpa package is from the github repo above it uses magit-popup which is deprecated.
;; instead use -> https://github.com/darcylee/magit-gerrit which supports the new  transient
;; (el-get-bundle magit-gerrit :bundle-async t :type github :pkgname "darcylee/magit-gerrit")

;; TODO: when this becomes a melpa pkg, switch to use-package
(el-get-bundle helm-icons :bundle-async t :type github :pkgname "yyoncho/helm-icons")

(provide 'pkg-elget)
