(require 'pkg-appearance)
(require 'pkg-environment)

(require 'pkg-site-lisp)

;; TODO: move elget stuff into use-package from github
;; (require 'pkg-elget)

(require 'pkg-editing)

;; sanity layers
(require 'pkg-sane-movement)
(require 'pkg-sane-mouse)

(require 'pkg-secrets)
(require 'pkg-cursor)

(provide 'conf)
