;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; GPG ENCRYPTED SECRETS FILES
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; IMPORTANT: MAKE SURE TO SET THIS IN THE ENV!
(defcustom emacs-default-gpg-key (getenv "EMACS_DEFAULT_GPG_KEY")
  "My default gpg-key."
  :type 'string)

(defcustom emacs-secrets-dir (or (getenv "EMACS_SECRETS_DIR")
                                 (expand-file-name "org/secrets" "~"))
  "My secrets files directory."
  :type 'string)



;; load gpg files
;; requires that the exec path include gpg for both shell and elisp to resolve gpg
;; Example:
;; (use-package exec-path-from-shell
;;   :demand t
;;   :if (memq window-system '(mac x w32 ns))
;;   :config
;;   (exec-path-from-shell-initialize))
(defun load-dir-files (base)
    (dolist (f (directory-files base))
      (let ((name (concat base "/" f)))
        (when (not (file-directory-p name))
        (load-library name)))))

(defun load-gpg-secrets-files ()
  "Load all secrets files."
  (interactive)
  ;;(require 'epa-file)
  ;; (custom-set-variables '(epg-gpg-program  "/usr/local/bin/gpg"))
  ;; (add-to-list 'exec-path "/usr/local/bin")
  ;; (epa-file-enable)
  (if (and (file-directory-p emacs-secrets-dir)
               (eq 0 (shell-command (format "gpg --list-key %s" emacs-default-gpg-key))))
      (load-dir-files emacs-secrets-dir)
    (message "NO SECRETS LOADED! ENSURE SHELL VARS SET.")))

(add-hook 'emacs-startup-hook 'load-gpg-secrets-files)

(provide 'pkg-secrets)
