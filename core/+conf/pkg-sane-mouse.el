;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	SANE MOUSE BEHAVIORS
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; ;; https://github.com/purcell/disable-mouse
;; (use-package disable-mouse)

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	DOUBLE CLICK
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; here we make double clicking the mouse select word on boundary
;; this override subword-mode's highlighting

(defun get-word-boundary ()
  "Return the boundary of the current word.
 The return value is of the form: (cons pos1 pos2).
 "
  (save-excursion
    (let (p1 p2)
      (progn
        (skip-chars-backward "-A-Za-z0-9_.") ;; here you can choose which symbols to use
        (setq p1 (point))
        (skip-chars-forward "-A-Za-z0-9_.") ;; put the same here
        (setq p2 (point)))
      (cons p1 p2)
      ))
  )
(defun select-word ()
  "Mark the url under cursor."
  (interactive)
  ;;  (require 'thingatpt)
  (let (bds)
    (setq bds (get-word-boundary))

    (set-mark (car bds))
    (goto-char (cdr bds))
    )
  )
(global-set-key [double-mouse-1] 'select-word)

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	TRIPLE CLICK
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; ;; implement our own markline by borrowing from vt-mouse.el
;; (defun vt-mouse-mark-line (click)
;;   "Mark the line clicked in."
;;   (interactive "@e")
;;   (mouse-set-point click)
;;   (end-of-line)
;;   (set-mark (1+ (point))))
;; (global-set-key [triple-mouse-1] 'vt-mouse-mark-line)

;; NOTE: mark-defun is normally bound to C-M-h
;; (global-set-key [triple-mouse-1] 'mark-defun)

(provide 'pkg-sane-mouse)
