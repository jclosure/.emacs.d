
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; EDITING ENHANCEMENTS THROUGH HOOKS AND ADVICE
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; cleanup whitespace at the end of line-bs before save
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; open up a file on a specific line
;; (advice-add 'find-file :around #'find-file--line-number)

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; COMMENTING IN/OUT CODE BLOCKS
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(defun smf/comment-dwim-line (&optional arg)
  "comment the line if no region selected"
  (interactive "*P")
  (comment-normalize-vars)
  (if (and (not (region-active-p)) (not (looking-at "[ \t]*$")))
      (comment-or-uncomment-region (line-beginning-position) (line-end-position))
    (comment-dwim arg)))

(global-set-key (kbd "M-;") #'smf/comment-dwim-line)


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; INDENTATION CONTROL
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; block-style indention mode
(use-package block-indent
  :ensure nil
  ;; :load-path "~/projects/jclosure/block-indent"
 ;; :quelpa (block-indent :fetcher github :repo "jclosure/block-indent")
  :straight (block-indent :type git :host github :repo "jclosure/block-indent")
  :bind (:map block-indent-mode-map
              ("TAB" . block-indent-complete-or-indent)
              ("<tab>" . block-indent-complete-or-indent))
  :init
  (defun block-indent-complete-or-indent ()
    (interactive)
    (if (bound-and-true-p company-mode)
        (unless (my-company-complete-maybe)
          (block-indent-right))
      (block-indent-right)))
  )


;; atom/sublime style indentation
(use-package regtab
  :ensure nil
  ;; :load-path "~/projects/jclosure/regtab"
  ;; :quelpa (regtab :fetcher github :repo "jclosure/regtab")
  :straight (regtab :type git :host github :repo "zorgnax/regtab"
                    :fork (:host github :repo "jclosure/regtab"))
  :bind (:map regtab-mode-map
              ("TAB" . regtab-complete-or-indent)
              ("<tab>" . regtab-complete-or-indent))
  :init
  (defun regtab-complete-or-indent ()
    (interactive)
    (if (bound-and-true-p company-mode)
      (unless (my-company-complete-maybe)
        (regtab-indent))
      (regtab-indent))))

;; a minor mode that guesses the indentation offset originally used for
;; creating source code files and transparently adjusts the corresponding
;; settings in Emacs, making it more convenient to edit foreign files
;; https://github.com/jscheid/dtrt-indent
(use-package dtrt-indent
  :ensure t
  :hook ((sh-mode . dtrt-indent-mode)))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	CODE/TEXT FOLDING
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; (use-package vimish-fold
;;   :config
;;   (progn
;;     (vimish-fold-global-mode 1)))

;; (use-package origami
;;   :commands (origami-toggle-node))


(use-package yafolding
  :bind ("M-'" . yafolding-toggle-element)
  :hook ((prog-mode . yafolding-mode)
         (json-mode . yafolding-mode)
         (conf-mode . yafolding-mode)
         (text-mode . yafolding-mode)
         (nxml-mode . yafolding-mode)
         (yaml-mode . yafolding-mode)))



;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; REGION SELECTION AND EXPANSION/CONTRACTION
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; supress the warning that expand-region has been overwritten
;; ref: https://andrewjamesjohnson.com/suppressing-ad-handle-definition-warnings-in-emacs/
(setq ad-redefinition-action 'accept)

;; contract with C-=, =, =, -, -
;; ^=    ^[[61;5u
(use-package expand-region
  :commands er/expand-region
  :config (setq expand-region-contract-fast-key "-")
  :bind (("C-= =" . er/expand-region)))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; WRAP REGION
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; customize it: https://github.com/kaushalmodi/.emacs.d/blob/master/setup-files/setup-wrap-region.el
(use-package wrap-region
  :ensure t
  :hook ((prog-mode . wrap-region-mode))
  ;; :config
  ;; (wrap-region-add-wrappers
  ;;  '(("*" "*" nil org-mode)
  ;;    ("~" "~" nil org-mode)
  ;;    ("/" "/" nil org-mode)
  ;;    ("=" "=" "+" org-mode)
  ;;    ("_" "_" nil org-mode)
  ;;    ("$" "$" nil (org-mode latex-mode))))
  ;; (add-hook 'org-mode-hook 'wrap-region-mode)
  ;; (add-hook 'latex-mode-hook 'wrap-region-mode)
  )


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; IEDIT - edit multiple things that match highlighted area or thing-at-point
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; NOTE: for emacs in iterm2 add this escape sequence keyboard binding - ^; => [59;5u
(use-package iedit
  :commands (iedit-mode)
  :bind* (("C-;" . iedit-mode))
  ;; set the face manually since my current theme doesn't show well
  :custom-face
  (iedit-occurrence-face ((t (:background "magenta" :foreground "black")))))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; CURSOR JUMPING TO FRONT/BACK OF LINE (intelligently)
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; move where i mean
(use-package mwim
  :bind
  ("C-a" . mwim-beginning-of-code-or-line)
  ("C-e" . mwim-end-of-code-or-line))

;; ;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; ;; DRAGGABLE REGIONS
;; ;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; ;; IMPORTANT: causes issues with <up/down> region selection and M-<right/left>
;; (use-package drag-stuff

;;   :config
;;   (progn
;;     (drag-stuff-global-mode t)
;;     ;; NOTE: camps on M-<arrow keys>
;;     (drag-stuff-define-keys)
;;     (dolist (mode '(org-mode rebase-mode))
;;       (add-to-list 'drag-stuff-except-modes mode))))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; WHITESPACE
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(use-package ws-butler
  :hook (prog-mode . ws-butler-mode)
  :diminish ws-butler-mode)

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; SUBWORD - within word navigation
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(use-package subword
  :init
  (progn
    (global-subword-mode)))

(use-package volatile-highlights
  :hook
  (after-init . volatile-highlights-mode))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; MULTIPLE CURSORS
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; TODO: convert to general key bindings on use-package
(use-package multiple-cursors

  :config
  (progn

    ;; TODO: try using mouse to place arbitrary cursors
    ;; https://stackoverflow.com/questions/39882624/setting-arbitrary-cursor-positions-with-multiple-cursors-in-emacs
    ;; (global-set-key (kbd "C-S-<mouse-1>") 'mc/add-cursor-on-click)

    ;; From active region to multiple cursors:
    (general-define-key
     :prefix "C-c m"
     "r" 'set-rectangular-region-anchor
     "c" 'mc/edit-lines
     "e" 'mc/edit-ends-of-lines
     "a" 'mc/edit-beginnings-of-lines))
  )



;; simpler usage of mc
;; (use-package multiple-cursors
;;   :bind
;;   (("C-c n" . mc/mark-next-like-this)
;;    ("C-c p" . mc/mark-previous-like-this)))

;; TODO: rework this.  get a better solution for MC
;; (use-package multiple-cursors
;;   :functions hydra-multiple-cursors
;;   :bind (("M-u" . hydra-multiple-cursors/body)
;;          ;; we are overriding the mail default creation command here
;;          ("C-x m" . mc/edit-lines)
;;          ;; use the mouse to place a cursor - only works in gui bode because C-S
;;          ("C-S-<mouse-1>" . mc/add-cursor-on-click))
;;   :config
;;   (with-eval-after-load 'hydra
;;     ;; https://ladicle.com/post/config/
;;     (defhydra hydra-multiple-cursors (:color pink :hint nil)
;;       "
;;                                                                         ╔════════╗
;;     Point^^^^^^             Misc^^            Insert                            ║ Cursor ║
;;   ──────────────────────────────────────────────────────────────────────╨────────╜
;;      _k_    _K_    _M-k_    [_l_] edit lines  [_a_] letters
;;      ^↑^    ^↑^     ^↑^     [_m_] mark all    [_n_] numbers
;;     mark^^ skip^^^ un-mk^   [_s_] search
;;      ^↓^    ^↓^     ^↓^
;;      _j_    _J_    _M-j_
;;   ╭──────────────────────────────────────────────────────────────────────────────╯
;;                            [_q_]: quit, [Click]: point
;; "
;;       ("l" mc/edit-lines :exit t)
;;       ("m" mc/mark-all-like-this :exit t)
;;       ("j" mc/mark-next-like-this)
;;       ("J" mc/skip-to-next-like-this)
;;       ("M-j" mc/unmark-next-like-this)
;;       ("k" mc/mark-previous-like-this)
;;       ("K" mc/skip-to-previous-like-this)
;;       ("M-k" mc/unmark-previous-like-this)
;;       ("s" mc/mark-all-in-region-regexp :exit t)
;;       ("a" mc/insert-letters :exit t)
;;       ("n" mc/insert-numbers :exit t)
;;       ("<mouse-1>" mc/add-cursor-on-click)
;;       ;; Help with click recognition in this hydra
;;       ("<down-mouse-1>" ignore)
;;       ("<drag-mouse-1>" ignore)
;;       ("q" nil))))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; SMARTER CUT/COPY (thanks to xah)
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(defun xah-cut-line-or-region ()
  "Cut current line, or text selection.
When `universal-argument' is called first, cut whole buffer (respects `narrow-to-region').

URL `http://ergoemacs.org/emacs/emacs_copy_cut_current_line.html'
Version 2015-06-10"
  (interactive)
  (if current-prefix-arg
      (progn ; not using kill-region because we don't want to include previous kill
        (kill-new (buffer-string))
        (delete-region (point-min) (point-max)))
    (progn (if (use-region-p)
               (kill-region (region-beginning) (region-end) t)
             (kill-region (line-beginning-position) (line-beginning-position 2))))))


(defun xah-copy-line-or-region ()
  "Copy current line, or text selection.
When called repeatedly, append copy subsequent lines.
When `universal-argument' is called first, copy whole buffer (respects `narrow-to-region').

URL `http://ergoemacs.org/emacs/emacs_copy_cut_current_line.html'
Version 2018-09-10"
  (interactive)
  (if current-prefix-arg
      (progn
        (copy-region-as-kill (point-min) (point-max)))
    (if (use-region-p)
        (progn
          (copy-region-as-kill (region-beginning) (region-end)))
      (if (eq last-command this-command)
          (if (eobp)
              (progn )
            (progn
              (kill-append "\n" nil)
              (kill-append
               (buffer-substring-no-properties (line-beginning-position) (line-end-position))
               nil)
              (progn
                (end-of-line)
                (forward-char))))
        (if (eobp)
            (if (eq (char-before) 10 )
                (progn )
              (progn
                (copy-region-as-kill (line-beginning-position) (line-end-position))
                (end-of-line)))
          (progn
            (copy-region-as-kill (line-beginning-position) (line-end-position))
            (end-of-line)
            (forward-char)))))))

(global-set-key (kbd "M-w" ) 'xah-copy-line-or-region)
(global-set-key (kbd "C-w" ) 'xah-cut-line-or-region)


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; COLUMN WIDTH GUIDES AND ENFORCEMENT
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; use this if you want to see where you've exceeded column width
;; (use-package column-enforce-mode
;;   :disabled t
;;   :diminish column-enforce-mode
;;   :init
;;   (setq column-enforce-column 80)
;;   :config
;;   (progn
;;     (add-hook 'prog-mode-hook 'column-enforce-mode)))

;; show vertical line at  80 chars
(use-package fill-column-indicator
  :hook
  ((markdown-mode
    git-commit-mode
    org-mode) . fci-mode))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; LINE NUMBERS
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; ;; better version of linum-mode
;; (use-package display-line-numbers
;;   :hook
;;   ((prog-mode yaml-mode systemd-mode) . display-line-numbers-mode))

;; using native line-numbers now
(add-hook 'prog-mode-hook (lambda() (display-line-numbers-mode 1)))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; TAB/INDENT HIGHLIGHTING
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; ;; turned off for now, because they can annoy me
;; (use-package highlight-indent-guides
;;   :diminish
;;   :hook
;;   ((prog-mode yaml-mode) . highlight-indent-guides-mode)
;;   :custom
;;   (highlight-indent-guides-auto-enabled t)
;;   (highlight-indent-guides-responsive t)
;;   (highlight-indent-guides-method 'character)) ; column


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; SYMBOLE HIGHLIGHTING AND NAV
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; TODO: add keys to the top desc
(use-package highlight-symbol
  :bind
  (:map prog-mode-map
        ;; ("M-o h" . highlight-symbol) ;; TODO: switch to chord hh
        ("M-p" . highlight-symbol-prev)
        ("M-n" . highlight-symbol-next))
  :config
  (custom-set-faces
   ;; custom-set-faces was added by Custom.
   ;; If you edit it by hand, you could mess it up, so be careful.
   ;; Your init file should contain only one such instance.
   ;; If there is more than one, they won't work right.
   ;; TODO: fix the curretly selected symbol face
   '(highlight-symbol-face
     ((t (:background "color-17" :foreground "black"
                      :weight bold :height 1.3 :family "Sans Serif"))))))


(provide 'pkg-editing)
