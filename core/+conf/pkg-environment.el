;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	GLOBAL FUNCTIONS USED DURING INIT
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; ///////////////////////////////////////////////////////////////////////
;; NOTE: THIS IS COOL .. BUT TOO DRACONIAN!!!
;; !!!! https://stackoverflow.com/questions/10660060/how-do-i-bind-c-in-emacs

;; currently used for tab switching with nswbuff
;; ^TAB    ^[[emacs-C-TAB
;; ^⇫TAB   ^[[emacs-C-S-TAB

(defun my/global-map-and-set-key (key command &optional prefix suffix)
  "`my/map-key' KEY then `global-set-key' KEY with COMMAND.
 PREFIX or SUFFIX can wrap the key when passing to `global-set-key'."
  (my/map-key key)
  (global-set-key (kbd (concat prefix key suffix)) command))

(defun my/map-key (key)
  "Map KEY from escape sequence \"\e[emacs-KEY\."
  (define-key function-key-map (concat "\e[emacs-" key) (kbd key)))

;; (my/global-map-and-set-key "C-TAB" 'nswbuff-switch-to-previous-buffer)
;; ///////////////////////////////////////////////////////////////////////

;; add gitlab.com to known_hosts if not already done
;; TODO: find a better way to do this
(defun ensure-known-hosts (fqdn)
  "Add a fqdn's to ssh key to ~/.ssh/known_hosts if not already there.
Example:
(ensure-known-hosts \"gitlab.com\")
"
  (progn
    (let
	      ((marker-file  (format "%sssh-key-known" user-emacs-directory)))
	    (when (not (file-exists-p marker-file))
	      (let
	          ((result (string-trim
			                (shell-command-to-string
			                 (format "ssh-keygen -l -F %s || echo " fqdn)))))
	        (when (string-empty-p result)
	          (shell-command-to-string
	           (format
		          "ssh-keyscan -t rsa %s  >> ~/.ssh/known_hosts && touch %s" fqdn marker-file ))))))))




;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	GARBAGE COLLECTION
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(let ((normal-gc-cons-threshold (* 20 1024 1024))
      (init-gc-cons-threshold (* 128 1024 1024)))
  (setq gc-cons-threshold init-gc-cons-threshold)
  (add-hook 'emacs-startup-hook
            (lambda () (setq gc-cons-threshold normal-gc-cons-threshold))))

;; BUG: WORKAROUND FOR EMACS 26.3 STARTUP LEXIXAL BINDING FOR ^
(setq normal-gc-cons-threshold (* 20 1024 1024))


;; DOOM INIT

;; NOTE: stolen from doom, because they are amazing...
;; TODO: ensure not causing mem leaks in gui
(progn
  (defvar doom-gc-cons-threshold 16777216 ; 16mb
    "The default value to use for `gc-cons-threshold'. If you experience freezing,
  decrease this. If you experience stuttering, increase this.")

  (defvar doom-gc-cons-upper-limit 268435456 ; 256mb
    "The temporary value for `gc-cons-threshold' to defer it.")


  (defvar doom--file-name-handler-alist file-name-handler-alist)

  (defun doom|restore-startup-optimizations ()
    "Resets garbage collection settings to reasonable defaults (a large
    `gc-cons-threshold' can cause random freezes otherwise) and resets
    `file-name-handler-alist'."
	  (setq file-name-handler-alist doom--file-name-handler-alist)
	  ;; Do this on idle timer to defer a possible GC pause that could result; also
	  ;; allows deferred packages to take advantage of these optimizations.
	  (run-with-idle-timer
	   3 nil (lambda () (setq-default gc-cons-threshold doom-gc-cons-threshold))))

  (if (or after-init-time noninteractive)
      (setq gc-cons-threshold doom-gc-cons-threshold)
    ;; A big contributor to startup times is garbage collection. We up the gc
    ;; threshold to temporarily prevent it from running, then reset it later in
    ;; `doom|restore-startup-optimizations'.
    (setq gc-cons-threshold doom-gc-cons-upper-limit)
    ;; This is consulted on every `require', `load' and various path/io functions.
    ;; You get a minor speed up by nooping this.
    (setq file-name-handler-alist nil)
    ;; Not restoring these to their defaults will cause stuttering/freezes.
    (add-hook 'after-init-hook #'doom|restore-startup-optimizations)))


;; Ensure Doom is running out of this file's directory
;; (setq user-emacs-directory (file-name-directory load-file-name))

;; In noninteractive sessions, prioritize non-byte-compiled source files to
;; prevent stale, byte-compiled code from running. However, if you're getting
;; recursive load errors, it may help to set this to nil.
;; (setq load-prefer-newer noninteractive)
;; instead of
;; (setq load-prefer-newer t)

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; BASIC PROGRAMMING SUPPORT
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; NOTE: cl has been deprecated as of Emacs27, replaced by cl-lib
(require 'cl-lib)

;; NOTE: one or more of the below depend upon 'cl
(use-package dash :config (require 'dash))                               ;; lists --> https://github.com/magnars/dash.el
(use-package dash-functional :demand t :config (dash-enable-font-lock))  ;; function list combinators
(use-package ht :demand t)                                               ;; hash-tables --> https://github.com/Wilfred/ht.el
(use-package s :demand t)                                                ;; strings --> https://github.com/magnars/s.el
(use-package a :demand t)                                                ;; association lists --> https://github.com/plexus/a.el
(use-package f :demand t)                                                ;; files and directories --> https://github.com/rejeep/f.el
(use-package loop :demand t)                                             ;; imperative loop structures --> https://github.com/Wilfred/loop.el
(use-package async :demand t)                                            ;; asynchronous processing --> https://github.com/jwiegley/emacs-async
(use-package deferred :demand t)                                         ;; facilities to manage asynchronous tasks --> https://github.com/kiwanami/emacs-deferred


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	ENSURE PATHS
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; NOTE: must be loaded before secrets in order for gpg binary to be in path
;; this ensures that the gui path is the same for gui as tui
(use-package exec-path-from-shell
  :demand t
  :if (or (daemonp)(memq window-system '(mac x w32 ns))) ; NOTE: may need to add pgtk for wayland
  :config
  (progn
    ;; IMPORTANT: put all env vars in here that you want avail
    ;; to Emacs run from MacOS UI
    (dolist (var '(
                   "GOROOT"
                   "GOPATH"
                   "GO111MODULE"
                   "EMACS_CUSTOM_GITLAB_FORGE_FQDN"
                   "EMACS_ORG_DIR"
                   "EMACS_SECRETS_DIR"
                   "EMACS_DEFAULT_GPG_KEY"
                   "CARGO_HOME"
                   "DOTFILES"
                   "GITLAB_TOKEN"
                   "PYTHONPATH"
                   "PATH"
                   "SSH_AUTH_SOCK"
                   "SSH_AGENT_PID"
                   "GPG_AGENT_INFO"
                   "LANG"
                   "LC_CTYPE"
                   "WORKON_HOME"
                   "GOOGLE_CLOUD_PROJECT"
                   "GCLOUD_PROJECT"))
      (add-to-list 'exec-path-from-shell-variables var))
    (exec-path-from-shell-initialize)))

(when (f-exists? "~/bin")
  (add-to-list 'exec-path "~/bin"))

(when (f-exists? "~/.local/bin")
  (add-to-list 'exec-path "~/.local/bin"))

;; (when (f-exists? "/usr/local/bin")
;;   (add-to-list 'exec-path "/usr/local/bin"))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	GENERAL, HYDRA, AND BINDKEY
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(require 'bind-key)

;; good key binding guidance: https://sam217pa.github.io/2016/09/23/keybindings-strategies-in-emacs/
(use-package general
  :demand t
  :init
  (defalias 'gsetq #'general-setq)
  (defalias 'gsetq-local #'general-setq-local)
  (defalias 'gsetq-default #'general-setq-default))


;; https://github.com/abo-abo/hydra/blob/master/hydra-examples.el
(use-package hydra)



;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	NO LITTERING
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; put this before any other emacs directories are defined or used
(use-package no-littering
  :demand t
  :config
  ;; don't leave autosaved files in same dir as file, keep here..
  (progn
    (setq auto-save-file-name-transforms
          `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))

    ;; save place in files - will use no-littering dirs
    (if (fboundp #'save-place-mode)
        (save-place-mode +1)
      (setq-default save-place t))))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	RECENTF
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(use-package recentf
  :ensure nil
  :after (no-littering)
  :init
  (progn
    ;; (setq recentf-auto-cleanup 300)
    ;; (setq recentf-exclude '("~$" "\\.log$"))
    (setq recentf-max-menu-items 50)
    (setq recentf-max-saved-items 50)
    (recentf-mode 1))
  :config
  (progn
    (add-to-list 'recentf-exclude no-littering-var-directory)
    (add-to-list 'recentf-exclude no-littering-etc-directory)
    (add-to-list 'recentf-exclude (format "%s/\\.emacs\\.d/elpa/.*" (getenv "HOME"))))
  (global-set-key "\C-x\ \C-r" 'recentf-open-files))





;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	setup coding system and window property
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(prefer-coding-system 'utf-8)
(setenv "LANG" "en_US.UTF-8")
(setenv	"LC_ALL" "en_US.UTF-8")
(setenv	"LC_CTYPE" "en_US.UTF-8")

;; Default Encoding
(prefer-coding-system 'utf-8-unix)
(set-locale-environment "en_US.UTF-8")
(set-default-coding-systems 'utf-8-unix)
(set-selection-coding-system 'utf-8-unix)
(set-buffer-file-coding-system 'utf-8-unix)
(set-clipboard-coding-system 'utf-8) ; included by set-selection-coding-system
(set-keyboard-coding-system 'utf-8) ; configured by prefer-coding-system
(set-terminal-coding-system 'utf-8) ; configured by prefer-coding-system
(setq buffer-file-coding-system 'utf-8) ; utf-8-unix
(setq save-buffer-coding-system 'utf-8-unix) ; nil
(setq process-coding-system-alist
      (cons '("grep" utf-8 . utf-8) process-coding-system-alist))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; FRAME CONTROL (gui only)
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; show full current open file path in frame title
(setq frame-title-format
      (list (format "%s %%S: %%j " (system-name))
            '(buffer-file-name "%f" (dired-directory dired-directory "%b"))))

(add-to-list 'initial-frame-alist '(fullscreen . maximized))
;; (add-to-list 'default-frame-alist '(fullscreen . fullheight))

;; setup titlebar appearance
(add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))
(add-to-list 'default-frame-alist '(ns-appearance . dark))

(fset 'yes-or-no-p 'y-or-n-p)

;; (toggle-frame-fullscreen)
(toggle-frame-maximized)


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; CONFIGURE BASIC EMACS OPTIONS:
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; make pretty everywhere
(global-prettify-symbols-mode 1)

(setq set-mark-command-repeat-pop t)  ; Repeating C-SPC after popping mark pops it again
(setq track-eol nil)			; Keep cursor at end of lines.
(setq line-move-visual nil)		; To be required by track-eol
;; NOTE: turn this off if it get's annoying
(setq-default kill-whole-line t)	; Kill line including '\n'
(setq-default indent-tabs-mode nil)   ; use space

;; set to avoid problems with crontabs, etc.
(setq require-final-newline t)

;; prevent extraneous tabs
;; (setq-default indent-tabs-mode nil)

(setq inhibit-startup-message t)
(setq initial-scratch-message "")

;; TODO: move into a secret file
(setq user-full-name "Joel Holder"
      user-mail-address "jclosure@gmail.com")


;; disable the annoying bell ring
(setq ring-bell-function 'ignore)

;; auto-revert any buffers that change on disk if not dirty
(global-auto-revert-mode)

;; don't blink the cursor
(blink-cursor-mode 0)



;; i find the capitalzing of the first char with this annoying
(global-unset-key (kbd "M-c"))

;; no text wrapping
(setq-default truncate-lines t)

;; remove $ glyph from the end of truncated lines
;; TODO: try to change it to: ➜ or ⤾
;; http://xahlee.info/comp/unicode_arrows.html
;; (set-display-table-slot standard-display-table 0 ?\⤾)
(set-display-table-slot standard-display-table 0 ?\→)


;; set threshold for large files
(setq large-file-warning-threshold (* 15 1024 1024))

;; gdb configuration
(setq gdb-many-windows t
      gdb-show-main t)





;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	DOOM DEFAULTS
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(setq-default

 ;; disable lock files, e.g. .#init.el
 ;; create-lockfiles nil

 ;; follow symlinks to their actual location when opening files
 find-file-visit-truename t
 vc-follow-symlinks t

 ansi-color-for-comint-mode t
 bidi-display-reordering nil ; disable bidirectional text for tiny performance boost
 blink-matching-paren nil    ; don't blink--too distracting
 compilation-always-kill t        ; kill compilation process before starting another
 compilation-ask-about-save nil   ; save all buffers on `compile'
 compilation-scroll-output 'first-error
 confirm-nonexistent-file-or-buffer t

 cursor-in-non-selected-windows nil ; hide cursors in other windows

 display-line-numbers-width 3
 echo-keystrokes 0.02
 enable-recursive-minibuffers nil
 frame-inhibit-implied-resize t
 frame-title-format '("%b – Jclosure Emacs") ; simple name in frame title
 ;; remove continuation arrow on right fringe
 fringe-indicator-alist
 (delq (assq 'continuation fringe-indicator-alist)
       fringe-indicator-alist)
 highlight-nonselected-windows nil
 image-animate-loop t
 indicate-buffer-boundaries nil
 indicate-empty-lines nil
 max-mini-window-height 0.3
 mode-line-default-help-echo nil ; disable mode-line mouseovers
 mouse-yank-at-point t           ; middle-click paste at point, not at click
 resize-mini-windows 'grow-only  ; Minibuffer resizing
 show-help-function nil          ; hide :help-echo text
 use-dialog-box nil              ; always avoid GUI
 uniquify-buffer-name-style 'forward
 visible-cursor nil
 x-stretch-cursor nil
 ;; Favor vertical splits
 split-width-threshold 160
 split-height-threshold nil
 ;; `pos-tip' defaults
 pos-tip-internal-border-width 6
 pos-tip-border-width 1
 ;; no beeping or blinking please
 ring-bell-function #'ignore
 visible-bell nil
 ;; don't resize emacs in steps, it looks weird
 window-resize-pixelwise t
 frame-resize-pixelwise t)

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	setup history of edited file
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(savehist-mode 1)
;; (setq savehist-file "~/.emacs.d/.savehist") ;; managed by no-littering
(setq history-length t)
(setq history-delete-duplicates t)
(setq savehist-save-minibuffer-history 1)
(setq savehist-additional-variables
      '(kill-ring
        search-ring
        regexp-search-ring))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	MENUBAR and TOOLBAR CONFIG
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; prevent the glimpse of un-styled Emacs by setting these early.
(add-to-list 'default-frame-alist '(tool-bar-lines . 0))
(add-to-list 'default-frame-alist '(menu-bar-lines . 0))

;; enable menu-bar-mode if open menu items
(defun my-enable-menu-bar-mode ()
  "Enable menu bar mode before opening a menu item"
  (interactive)
  (menu-bar-mode 1))

;; ensures that menu-bar-mode is enabled before showing it
(defadvice menu-bar-open (before menu-bar-open-before activate)
  (my-enable-menu-bar-mode))

;; TODO: simplify
(defun my-menu-bar-toggle ()
  (interactive)
  (if menu-bar-mode
      (menu-bar-mode -1)
    (my-enable-menu-bar-mode)))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;  SCROLLING
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; ;; DEFAULT APPROACH
;; (use-package smooth-scrolling
;;   :init
;;   (setq scroll-step 1
;;         scroll-conservatively 10000
;;         smooth-scroll-margin 2
;;         scroll-preserve-screen-position t)
;;   :config
;;   (smooth-scrolling-mode 1))

;; (defun scroll-lock-previous-line-fixed-position (arg)
;;   (interactive "p")
;;   (when (not (pos-visible-in-window-p (point-min)))
;;     (scroll-down arg)) ; scroll, before moving cursor
;;   (previous-line arg) ; keep the cursor position fixed relative to the window
;;   (setq this-command 'previous-line))  ; make temporary-goal-column work correctly

;; (defun scroll-lock-next-line-fixed-position (arg)
;;   (interactive "p")
;;   (when (not (pos-visible-in-window-p (point-max)))
;;     (scroll-up arg)) ; scroll, before moving cursor
;;   (next-line arg) ; keep the cursor position fixed relative to the window
;;   (setq this-command 'next-line))  ; make temporary-goal-column work correctly

;; ;; scroll up/down with point at a fixed position by holding C-S and up or down
;; (global-set-key  [(control shift down)] 'scroll-lock-next-line-fixed-position)
;; (global-set-key  [(control shift up)] 'scroll-lock-previous-line-fixed-position)

;; ;; GOOD SCROLL SUB-PIXEL APPROACH
;; (setq
;;  ;; https://www.masteringemacs.org/article/improving-performance-emacs-display-engine
;;  ;; https://www.reddit.com/r/emacs/comments/8sw3r0/finally_scrolling_over_large_images_with_pixel/
;;  ;; https://www.reddit.com/r/emacs/comments/9rwb4h/why_does_fast_scrolling_freeze_the_screen/
;;  ;; https://emacs.stackexchange.com/questions/10354/smooth-mouse-scroll-for-inline-images
;;  ;; https://emacs.stackexchange.com/questions/28736/emacs-pointcursor-movement-lag
;;  redisplay-dont-pause            t ;; Fully redraw the display before it processes queued input events.
;;  ;; next-screen-context-lines       2 ;; Number of lines of continuity to retain when scrolling by full screens
;;  scroll-conservatively       10000 ;; only 'jump' when moving this far off the screen
;;  scroll-step                     1 ;; Keyboard scroll one line at a time
;;  ;; mouse-wheel-progressive-speed nil ;; Don't accelerate scrolling
;;  mouse-wheel-follow-mouse        t ;; Scroll window under mouse
;;  fast-but-imprecise-scrolling    nil ;; No (less) lag while scrolling lots.
;;  ;; auto-window-vscroll           nil ;; Cursor move faster
;;  )

;; ;; ;; Inertial Scrolling - Better than progressive-speed
;; ;; (use-package inertial-scroll
;; ;;   :ensure nil
;; ;;   :quelpa (inertial-scroll :fetcher github :repo "kiwanami/emacs-inertial-scroll")
;; ;;   ;; :straight t
;; ;;   :custom
;; ;;   (inertias-friction 50)
;; ;;   :bind
;; ;;   (("<mouse-4>"     . inertias-down-wheel) ;; Replace mwheel-scroll
;; ;;    ("<mouse-5>"     . inertias-up-wheel  ) ;; Replace mwheel-scroll
;; ;;    ("<wheel-up>"    . inertias-down-wheel)
;; ;;    ("<wheel-down>"  . inertias-up-wheel  )))

;; ;; Pixel-scroll alternative
;; (use-package good-scroll
;;   :ensure t
;;   ;; :straight (good-scroll :type git :host github :repo "io12/good-scroll.el")
;;   :hook (after-init . good-scroll-mode))

;; (mouse-wheel-mode 1)

;; Emacs has this unique behavior (which I'd like to avoid) when we press the arrow-down key till
;; it reaches the end of the frame, instead of revealing more lines "line-by-line" like any other
;; editor, Emacs' cursor pulls the content towards the center of the frame, revealing at once the
;; next half-window-height lines of content.  Without activating sublimity, I was able to suppress
;; this unique behavior with the following 2 lines:
(progn
  (setq scroll-conservatively 101) ; as long as it's > 100
  (setq scroll-preserve-screen-position t))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	final global system settings
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; y/n instead of yes/no
(fset #'yes-or-no-p #'y-or-n-p)

;; Truly silence startup message
(fset #'display-startup-echo-area-message #'ignore)

;; relegate tooltips to echo area only
(if (bound-and-true-p tooltip-mode) (tooltip-mode -1))

;; enabled by default; no thanks, too distracting
(blink-cursor-mode -1)


;; by commenting this, we get vertical scrollbars in gui mode
;;(add-to-list 'default-frame-alist '(vertical-scroll-bars))

;; allow selected region to be replaced by typing or hitting backspace
(delete-selection-mode 1)

;; indendation control
(setq-default indent-tabs-mode nil ;; use space
              line-spacing 1
              tab-width 2
              c-basic-offset 2
              cursor-type 'bar)


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	WINNERMODE
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(when (fboundp 'winner-mode)
  (winner-mode 1))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	CONFIGURE PARENS
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; configure parens
;; TODO: see smartparens section below for additional config
(electric-pair-mode 1)
(show-paren-mode 1)
(setq show-paren-delay 0)
;; change highlighted paren face
(set-face-attribute 'show-paren-match nil
                    :weight 'extra-bold
                    :background "blue"
                    ;; :background (face-background 'default)
                    :foreground "cyan")

(use-package smartparens
  :hook
  (prog-mode . smartparens-mode)
  :config
  (progn
    ;; enable visual
    ;; (show-smartparens-global-mode)

    (require 'smartparens-config)

    (sp-pair "\"" "\"") ;; adds `' as a local pair in emacs-lisp-mode

    ;; ;; chords for slurping and barfing
    ;; ;; NOTE: this is experimental. it will conflict with elixir binary literals
    ;; (key-chord-define-global "<<" 'sp-forward-slurp-sexp)
    ;; (key-chord-define-global ">>" 'sp-forward-barf-sexp)

    ;; bindings for slurping/barfing
    (global-set-key  (kbd "C-0") 'sp-forward-barf-sexp) ; [48;5u
    (global-set-key  (kbd "C-9") 'sp-forward-slurp-sexp) ; [57;5u

    ;; load support for elixir
    (require 'smartparens-elixir)
    ))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; RECENTER AFTER THE FOLLOWING JUMP EVENTS
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(let ((after-fn (lambda (&rest _) (recenter nil))))
  (advice-add 'goto-line :after after-fn)
  ;;  (advice-add 'pop-mark :after after-fn)
  (advice-add 'xref-goto-xref :after after-fn)
  (advice-add 'smart-jump-go :after after-fn)
  (advice-add 'dumb-jump-go :after after-fn)
  (advice-add 'helm-goto-source :after after-fn)
  (advice-add 'helm-goto-line :after after-fn)
  (advice-add 'helm-goto-next-file :after after-fn)
  (advice-add 'helm-xref-goto-xref-item :after after-fn)
  (advice-add 'helm-find-files :after after-fn)
  ;; etc...
  )


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	BACKUP AND BACKUP RELATED
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; backup file control configuration
(setq make-backup-files t               ; backup of a file the first time it is saved.
      backup-by-copying t               ; don't clobber symlinks
      version-control t                 ; version numbers for backup files
      delete-old-versions t             ; delete excess backup files silently
      delete-by-moving-to-trash t
      kept-old-versions 6               ; oldest versions to keep when a new numbered backup is made (default: 2)
      kept-new-versions 9               ; newest versions to keep when a new numbered backup is made (default: 2)
      auto-save-default t               ; auto-save every buffer that visits a file
      auto-save-timeout 20              ; number of seconds idle time before auto-save (default: 30)
      auto-save-interval 200            ; number of keystrokes between auto-saves (default: 300)
      )

;; browse backup files
;; M-x backup-walker-start
;; p or n to prev next
(use-package backup-walker
  :commands backup-walker-start)

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	operation system settings
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; (cond ((string-equal system-type "darwin")
;;        (progn
;; 	 ;; modify option and command key
;; 	 ;; (setq mac-command-modifier 'control)
;; 	 (setq mac-option-modifier 'meta)

;; 	 ;; batter copy and paste support for mac os x
;; 	 (defun copy-from-osx ()
;; 	   (shell-command-to-string "pbpaste"))
;; 	 (defun paste-to-osx (text &optional push)
;; 	   (let ((process-connection-type nil))
;; 	     (let ((proc (start-process "pbcopy" "*Messages*" "pbcopy")))
;; 	       (process-send-string proc text)
;; 	       (process-send-eof proc))))
;; 	 (setq interprogram-cut-function 'paste-to-osx)
;; 	 (setq interprogram-paste-function 'copy-from-osx)

;; 	 (message "Welcome To Mac OS X, Have A Nice Day!!!"))))

;; (use-package exec-path-from-shell)
;; (when (memq window-system '(mac ns x))
;;   (exec-path-from-shell-initialize))


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	misc settings
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; (setq custom-file "~/.emacs.d/.custom.el")
;; (load custom-file t)


;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;	Usefule global key bind
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; (global-set-key (kbd "C-\\") 'comment-line)
;; (global-set-key (kbd "C-+") 'text-scale-increase)
;; (global-set-key (kbd "C--") 'text-scale-decrease)

;; ;; Tmux already use F1
;; ;; Neotree refresh use F2
;; (global-set-key (kbd "<f3>") 'helm-recentf)
;; (global-set-key (kbd "<f4>") 'yas-insert-snippet)
;; (global-set-key (kbd "<f5>") 'grep-find)
;; (global-set-key (kbd "<f6>") 'pkg-org-publish)
;; (global-set-key (kbd "<f7>") 'helm-dash)
;; ;; Neotree toggle use F8
;; (global-set-key (kbd "<f9>") 'agenda-view)
;; (global-set-key (kbd "<f10>") 'helm-M-x)
;; (global-set-key (kbd "<f12>") 'fiplr-find-file)

;; (global-set-key (kbd "M-0") 'next-multiframe-window)
;; (global-set-key (kbd "M-9") 'previous-multiframe-window)

;; (global-set-key (kbd "C-x C-m") 'helm-M-x)

(provide 'pkg-environment)
