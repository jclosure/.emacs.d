;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; converts emacs to sane forward/backard deleting mechanism
;; overrides default word killing behavior with simple delete
;; stop at left/right margins on line when deleting words
;; mimic other editors behavior with Alt-backspace and Alt-d
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; TODO: publish this

  (defun get-position-of-beginning-of-line ()
    (let ((orig-point (point)))
      (beginning-of-line)
      (let ((beg-line-point (point)))
        (goto-char orig-point)
        beg-line-point)))

  (defun get-position-of-end-of-line ()
    (let ((orig-point (point)))
      (end-of-line)
      (let ((end-line-point (point)))
        (goto-char orig-point)
        end-line-point)))

  (defun line-bounds-nonwhitespace ()
    (save-excursion
      (goto-char (point-at-bol))
      (skip-syntax-forward " " (point-at-eol))
      (let ((beg (point)))
        (goto-char (point-at-eol))
        (skip-syntax-backward " " (point-at-bol))
        (list beg (point)))))

  (defun at-end-of-line-text-p ()
    (= (point) (cadr (line-bounds-nonwhitespace))))

  (defun at-beginning-of-line-text-p ()
    (= (point) (car (line-bounds-nonwhitespace))))

  (defun my-delete-backward-word ()
    (interactive "*")
    (push-mark)
    (backward-word)
    (delete-region (point) (mark))
    )

  (defun my-delete-forward-word ()
    (interactive "*")
    (push-mark)
    (forward-word)
    (delete-region (point) (mark)))

   ;;; DELETING M-<delete>, M-d

  (defun my-backward-delete-word-on-this-line ()
    (interactive)
    (letrec ((orig-point (point))
             (beg-line-point (get-position-of-beginning-of-line))
             (line-string (buffer-substring-no-properties beg-line-point orig-point)))
      (backward-word)
      (let ((backward-word-point (point)))
        (goto-char orig-point)
        (cond
         ((= beg-line-point orig-point)
          ;; opt 1: delete-backward-word
          ;; (my-delete-backward-word)
          ;; opt 2: go to end of line above
          (delete-backward-char 1))
         ((string-match "^[\s\t]*$" line-string)
          (progn
            (push-mark)
            (goto-char beg-line-point)
            (delete-region (point) (mark))))
         ((> beg-line-point backward-word-point)
          (progn
            (push-mark)
            (goto-char (car (line-bounds-nonwhitespace)))
            (delete-region (point) (mark))))
         (t
          (my-delete-backward-word))))))

  (defun my-forward-delete-word-on-this-line ()
    (interactive)
    (let ((orig-point (point))
          (end-line-point (cadr (line-bounds-nonwhitespace))))
      (forward-word 1)
      (let ((forward-word-point (point)))
        (goto-char orig-point)
        (print (list orig-point end-line-point))
        (cond
         ((= orig-point end-line-point)
          (progn
            (push-mark)
            (goto-char (+ end-line-point 1))
            (goto-char (car (line-bounds-nonwhitespace)))
            (delete-region (point) (mark))
            (goto-char end-line-point)))
         ((< end-line-point forward-word-point)
          (progn
            (push-mark)
            (goto-char end-line-point)
            (delete-region (point) (mark))))
         (t
          (my-delete-forward-word))))))

  ;; don't put in kill ring and stop on line
  (define-key global-map [remap backward-kill-word] 'my-backward-delete-word-on-this-line)
  (define-key global-map [remap kill-word] 'my-forward-delete-word-on-this-line)

 ;;; MOVEMENT M-<left>, M-<right>

  ;; (defun EXAMPLE-HOW-TO-STOP-AT-THE-BEGINNING-OF-THE-VISUAL-LINE ()
  ;;   (interactive)
  ;;   (let ((orig-point (point))
  ;;         (beg-line-point (get-position-of-beginning-of-line)))
  ;;     (forward-word (- 1))
  ;;     (let ((backward-word-point (point)))
  ;;       (goto-char orig-point)
  ;;       (cond
  ;;        ((= beg-line-point orig-point)
  ;;         (backward-char 1))
  ;;        ((> beg-line-point backward-word-point)
  ;;         (goto-char beg-line-point))
  ;;        (t (goto-char backward-word-point))
  ;;        )
  ;;       )))

  (defun my-backward-word-on-this-line ()
    (interactive)
    (let ((orig-point (point))
          (beg-line-point (get-position-of-beginning-of-line)))
      (forward-word (- 1))
      (let ((backward-word-point (point))
            (line-string (buffer-substring-no-properties beg-line-point orig-point)))
        (goto-char orig-point)
        (cond
         ((string-match "^[\s\t]*$" line-string)
          (goto-char (- beg-line-point 1)))
         ((> beg-line-point backward-word-point)
          (goto-char (car (line-bounds-nonwhitespace))))
         (t (goto-char backward-word-point))))))


  (defun my-forward-word-on-this-line ()
    (interactive)
    (let ((orig-point (point))
          (end-line-point (cadr (line-bounds-nonwhitespace))))
      (forward-word 1)
      (let ((forward-word-point (point)))
        (goto-char orig-point)
        ;;(print (list orig-point end-line-point))
        (cond
         ((= orig-point end-line-point)
          (progn
            (goto-char (+ end-line-point 1))
            (goto-char (car (line-bounds-nonwhitespace)))))
         ((< end-line-point forward-word-point)
          (goto-char end-line-point))
         (t (goto-char forward-word-point))))))

  (define-key global-map [remap backward-word] 'my-backward-word-on-this-line)
  (define-key global-map [remap forward-word] 'my-forward-word-on-this-line)



(provide 'pkg-sane-movement)
