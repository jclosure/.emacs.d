;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; CURSOR APPEARANCE
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


(when (not (display-graphic-p))
  ;; cursor config - (only works in gui)
  (use-package bar-cursor
    :config
    (bar-cursor-mode 1)))

(when (not (display-graphic-p))

  ;; my solution

  (require 'terminal-cursor)
  ;; when in terminal switch in/out of vertical-bar cursor
  ;; (set-cursor-type 'vertical-bar)
  ;; switch it back when we exit
  (add-hook 'kill-emacs-hook (lambda () (set-cursor-type 'block)))


  ;; NOTE: or use a seperate more flexible package "term-cursor.el"
  ;; ;; try it with: printf '\033[n q', where n is one of the options above
  ;; (el-get-bundle term-cursor-el :type github :pkgname "h0d/term-cursor.el")
  ;; (global-term-cursor-mode)

  ;; (setq-default cursor-type 'box)
  (setq-default cursor-type 'bar)
  ;; (setq-default cursor-type 'hbar)
  ;; (setq-default cursor-type 'hollow)

  ;; cursor blinks when not in use
  ;; (blink-cursor-mode 1)
  )

;; (defvar blink-cursor-colors (list  "#92c48f" "#6785c5" "#be369c" "#d9ca65")
;;    "On each blink the cursor will cycle to the next color in this list.")
;; (setq blink-cursor-count 3)
;; (defun blink-cursor-timer-function ()
;;   ;;   "Zarza wrote this cyberpunk variant of timer `blink-cursor-timer'.
;;   ;; Warning: overwrites original version in `frame.el'.

;;   ;; This one changes the cursor color on each blink. Define colors in `blink-cursor-colors'."
;;   (when (not (internal-show-cursor-p))
;;     (when (>= blink-cursor-count (length blink-cursor-colors))
;;       (setq blink-cursor-count 0))
;;     (set-cursor-color (nth blink-cursor-count blink-cursor-colors))
;;     (setq blink-cursor-count (+ 1 blink-cursor-count)))
;;   (internal-show-cursor nil (not (internal-show-cursor-p))))

(provide 'pkg-cursor)
