;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; SITE-LISP PACKAGES AND HELPERS
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; extend load-path recursively
;; add any path to this list to be loaded from top-level down
(defconst site-lisp-path (f-join user-emacs-directory "site-lisp"))
(defconst load-paths-list (list
                           site-lisp-path))
(mapcar #'(lambda(p)
           (add-to-list 'load-path p)
           (cd p) (normal-top-level-add-subdirs-to-load-path))
        load-paths-list)


;; HOW TO USE use-package to load a package from local dir (not already in load-path)
(require 'helpers)

(provide 'pkg-site-lisp)
