

;; git-blame
vc-annotate ;; (C-x v g)
a   vc-annotate-revision-previous-to-line
j   vc-annotate-revision-at-line
n   vc-annotate-next-revision
p   vc-annotate-prev-revision

;; git-messenger
git-messenger:popup-message ;; (C-x v p)

;; multiple-cursors
(general-define-key
 :prefix "C-c m"
 "r" 'set-rectangular-region-anchor
 "c" 'mc/edit-lines
 "e" 'mc/edit-ends-of-lines
 "a" 'mc/edit-beginnings-of-lines))
)
