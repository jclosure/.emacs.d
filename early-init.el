;;; early-init.el --- early bird  -*- no-byte-compile: t -*-

;; ref: https://github.com/emacscollective/auto-compile

;; (setq load-prefer-newer t)

(tool-bar-mode 0)
(menu-bar-mode 0)
(when (fboundp 'scroll-bar-mode)
  (scroll-bar-mode 0))

;; used for straight to prevent package.el from loading anything before straight.
(setq package-enable-at-startup nil)


;; ;; BREAKING WHEN UPDATE EMACS!!!! MAKE SURE TO REMOVE THIS WHEN UPGRADE EMACS!!!!!!!!~
;; (setq
;;  native-comp-eln-load-path comp-eln-load-path
;;  native-comp-deferred-compilation-deny-list comp-deferred-compilation-deny-list
;;  ;; native-comp-warning-on-missing-source comp-warning-on-missing-source
;;  ;; native-comp-driver-options comp-native-driver-options
;;  ;; native-comp-async-query-on-exit comp-async-query-on-exit
;;  ;; native-comp-async-report-warnings-errors comp-async-report-warnings-errors
;;  ;; native-comp-async-env-modifier-form comp-async-env-modifier-form
;;  ;; native-comp-async-all-done-hook comp-async-all-done-hook
;;  ;; native-comp-async-cu-done-functions comp-async-cu-done-functions
;;  ;; native-comp-async-jobs-number comp-async-jobs-number
;;  ;; native-comp-never-optimize-functions comp-never-optimize-functions
;;  ;; native-comp-bootstrap-deny-list comp-bootstrap-deny-list
;;  ;; native-comp-always-compile comp-always-compile
;;  ;; native-comp-verbose comp-verbose
;;  ;; native-comp-debug comp-debug
;;  ;; native-comp-speed comp-speed
;;  ;; native-comp-limple-mode comp-limple-mode
;;  )


;; ;; Load recursively a directory
;; (defconst elisp-path '("~/.emacs.d/elisp"))
;; (mapcar '(lambda(p)
;;            (add-to-list 'load-path p)
;;            (cd p) (normal-top-level-add-subdirs-to-load-path)) elisp-path)

;; NOTE: This is problematic when compiling helm in elpa
;; (use-package auto-compile
;;   :demand t
;;   :config
;;   (auto-compile-on-load-mode)
;;   (auto-compile-on-save-mode)
;;   (setq auto-compile-display-buffer               nil)
;;   (setq auto-compile-mode-line-counter            t)
;;   (setq auto-compile-source-recreate-deletes-dest t)
;;   (setq auto-compile-toggle-deletes-nonlib-dest   t)
;;   (setq auto-compile-update-autoloads             t))


;;; early-init.el ends here
