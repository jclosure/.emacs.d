;;; package --- Summary
;;; Code:
;;; Commentary:
;;
;; Mostly lifted from: https://www.emacswiki.org/emacs/ElispCookbook


(defun edit-init-file ()
  "Open the init file."
  (interactive)
  ;;(find-file user-init-file)
  (let ((user-init-file (expand-file-name "init.el" user-emacs-directory)))
    (find-file user-init-file)))

(defun reload-init-file ()
  "Reload the init file."
  (interactive)
  (load-file user-init-file))

(defun edit-zsh-dotfile ()
  "Open the init file."
  (interactive)
  (find-file "~/.zshrc"))

;; TODO: consider changing the switch-to commands to open in other buffer

(defun switch-to-messages ()
  (interactive)
  (switch-to-buffer "*Messages*"))

(defun switch-to-scratch ()
  (interactive)
  (switch-to-buffer "*scratch*"))

(defun switch-to-dashboard ()
  (interactive)
  (switch-to-buffer "*dashboard*"))

(defun switch-to-copy-log ()
  (interactive)
  (switch-to-buffer "*copy-log*"))

;;  KEYBOARD AUTOMATION
(defun just-push-it (&rest keys)
  "Function to simulate pushing arbitrary keys
Examples:
(just-push-it \"C-x 3\" \"C-x o\" \"M-x ansi-term\" \"RET\" \"RET\")
(just-push-it \"C-g\")
"
  (setq unread-command-events
        (append (apply 'vconcat (mapcar 'kbd keys)) nil)))

(defun send-keyboard-quit ()
  (interactive)
  (just-push-it "C-g"))


;; HELPER FUNCTIONS
(defun advice-unadvise (sym)
  "Remove all advices from symbol SYM."
  (interactive "aFunction symbol: ")
  (advice-mapc (lambda (advice _props) (advice-remove sym advice)) sym))

(defun string/ends-with (s ending)
  "Return non-nil if string S ends with ENDING."
  (cond ((>= (length s) (length ending))
         (string= (substring s (- (length ending))) ending))
        (t nil)))
(defun string/starts-with (s begins)
  "Return non-nil if string S starts with BEGINS."
  (cond ((>= (length s) (length begins))
         (string-equal (substring s 0 (length begins)) begins))
        (t nil)))


(defun fill-buffer ()
  "Format existing text to 80 columns"
  (interactive)
  (save-excursion
    (save-restriction
      (widen)
      (fill-region (point-min) (point-max)))))


;;; SMALL HELPERS

(defun alias (new-name prev-name)
  "Set an alias for another function. Forward name from one to another"
  (setf (symbol-function new-name)
        (symbol-function prev-name)))

(defvar print-maybe-on  nil)
(defun print-maybe(msg return-value)
  (when print-maybe-on
    (print msg))
  return-value)

(cl-defun print-path (&optional (path (getenv "PATH")))
  "Print a path string one line at a time. Defaults to $PATH
   Use also by passing an optional arg, e.g. $PYTHONPATH"
  (print (replace-regexp-in-string ":" "\n" path)))

(defun buffer-mode (&optional buffer-or-name)
  "Returns the major mode associated with a buffer.
      If buffer-or-name is nil return current buffer's mode."
  (buffer-local-value 'major-mode
                      (if buffer-or-name
                          (get-buffer buffer-or-name)
                        (current-buffer))))

(defun prevent-kill-buffer (buff-name)
  (with-current-buffer buff-name
    (emacs-lock-mode 'kill)))

(defun rename-file-and-buffer (new-name)
  "Renames both current buffer and file it's visiting to NEW-NAME."
  (interactive "sNew name: ")
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not filename)
        (message "Buffer '%s' is not visiting a file!" name)
      (if (get-buffer new-name)
          (message "A buffer named '%s' already exists!" new-name)
        (progn
          (rename-file filename new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil))))))


(defun copy-current-line-position-to-clipboard ()
  "Copy current line in file to clipboard as '</path/to/file>:<line-number>'"
  (interactive)
  (let ((path-with-line-number
         (concat (buffer-file-name) ":" (number-to-string (line-number-at-pos)))))
    (x-select-text path-with-line-number) ;; write to system copy buffer
    (kill-new  path-with-line-number) ;; write to emacs killring
    (message (concat path-with-line-number " copied to clipboard"))))

(defun find-file--line-number (orig-fun filename &optional wildcards)
  "Turn files like file.cpp:14 into file.cpp and puts point on 14-th line."
  (save-match-data
    (let* ((matched (string-match "^\\(.*\\):\\([0-9]+\\):?$" filename))
           (line-number (and matched
                             (match-string 2 filename)
                             (string-to-number (match-string 2 filename))))
           (filename (if matched (match-string 1 filename) filename)))
      (apply orig-fun (list filename wildcards))
      (when line-number
        ;; goto-line is for interactive use
        (goto-char (point-min))
        (forward-line (1- line-number))))))


(defun usage ()
  "Open the init file."
  (interactive)
  (find-file (f-join user-emacs-directory "usage.el")))


;; duplicate a line for playing with or changing it's syntax
(defun duplicate-line ()
  (interactive)
  (save-excursion
    (let ((line-text (buffer-substring-no-properties
                      (line-beginning-position)
                      (line-end-position))))
      (move-end-of-line 1)
      (newline)
      (insert line-text))))
(global-set-key (kbd "C-c d") 'duplicate-line)

(defun get-buffers-by-prefix (prefix)
  "Example: (get-buffers-by-prefix \"*Magit\")"
  (seq-filter (lambda (buf) (string-prefix-p (concat " " prefix) (buffer-name buf)))
              (buffer-list)))

(defun kill-buffers (list-of-buffers)
  (mapc 'kill-buffer list-of-buffers))

(defun kill-all-buffers ()
  (interactive)
  (kill-buffers (buffer-list)))

(defun face-exists-p (face)
  "check if font exists"
  (if (member face (face-list)) t nil))

(defun get-faces (pos)
  "Get the font faces at POS."
  (remq nil
        (list
         (get-char-property pos 'read-face-name)
         (get-char-property pos 'face)
         (plist-get (text-properties-at pos) 'face))))

(defun what-faces (pos)
  (interactive "d")
  (let ((face (or (get-char-property (point) 'read-face-name)
    	            (get-faces pos))))
    (if face (message "Face: %s" face) (message "No face at %d" pos))))

(defun elpa-recompile-all ()
  (interactive)
  (byte-recompile-directory package-user-dir 0 t))


(defun pp-all (obj)
  "pretty-print everything, e.g. (pp (x-family-fonts))"
  (let ((eval-expression-print-level nil)
        (eval-expression-print-length nil))
    (pp obj)))

(defun print-all (obj)
  (let ((eval-expression-print-level nil)
        (eval-expression-print-length nil))
    (print obj)))


(provide 'helpers)

;;; helpers.el ends here
