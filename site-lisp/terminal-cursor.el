;;; package --- Summary
;;; Code:
;;; Commentary:

;; cursor shape control for vt100-based terminals

;; TODO: Convert from frame.el to terminal using set-cursor-type
;; [WIP] Blinking cursor
;; NOTE: blinking is currently disabled in basic config settings. change it before working in here.

(when (not (display-graphic-p))

  (defun set-cursor-type (type)
    """Set the cursor type but only for terminal mode (iTerm tested)"
    (interactive "stype? <'bar, 'blinking-bar, 'block, 'blinking-block>:")
    (let ((n (cond
              ((eq type 'blinking-block) 1)
              ((eq type 'block) 2)
              ((eq type 'blinking-horizontal-bar) 3)
              ((eq type 'horizontal-bar) 4)
              ((eq type 'blinking-vertical-bar) 5)
              ((eq type 'vertical-bar) 6)
              (t 0))))
      (send-string-to-terminal (format "\e[%d q" n))))


;;   ;; START HERE: from frame.el below

;;   (defvar blink-cursor-idle-timer nil
;;     "Timer started after `blink-cursor-delay' seconds of Emacs idle time.
;;      The function `blink-cursor-start' is called when the timer fires.")

;;   (defvar blink-cursor-timer nil
;;     "Timer started from `blink-cursor-start'.
;;      This timer calls `blink-cursor-timer-function' every
;; `blink-cursor-interval' seconds.")

;;   (defgroup cursor nil
;;     "Displaying text cursors."
;;     :version "21.1"
;;     :group 'frames)

;;   (defcustom blink-cursor-delay 0.5
;;     "Seconds of idle time before the first blink of the cursor.
;;      Values smaller than 0.2 sec are treated as 0.2 sec."
;;     :type 'number
;;     :group 'cursor
;;     :set (lambda (symbol value)
;;            (set-default symbol value)
;;            (when blink-cursor-idle-timer (blink-cursor--start-idle-timer))))

;;   (defcustom blink-cursor-interval 0.5
;;     "Length of cursor blink interval in seconds."
;;     :type 'number
;;     :group 'cursor
;;     :set (lambda (symbol value)
;;            (set-default symbol value)
;;            (when blink-cursor-timer (blink-cursor--start-timer))))

;;   (defcustom blink-cursor-blinks 10
;;     "How many times to blink before using a solid cursor on NS, X, and MS-Windows.
;;      Use 0 or negative value to blink forever."
;;     :version "24.4"
;;     :type 'integer
;;     :group 'cursor)

;;   (defvar blink-cursor-blinks-done 1
;;     "Number of blinks done since we started blinking on NS, X, and MS-Windows.")

;;   (defun blink-cursor--start-idle-timer ()
;;     "Start the `blink-cursor-idle-timer'."
;;     (when blink-cursor-idle-timer (cancel-timer blink-cursor-idle-timer))
;;     (setq blink-cursor-idle-timer
;;           ;; The 0.2 sec limitation from below is to avoid erratic
;;           ;; behavior (or downright failure to display the cursor
;;           ;; during command execution) if they set blink-cursor-delay
;;           ;; to a very small or even zero value.
;;           (run-with-idle-timer (max 0.2 blink-cursor-delay)
;;                                :repeat #'blink-cursor-start)))

;;   (defun blink-cursor--start-timer ()
;;     "Start the `blink-cursor-timer'."
;;     (when blink-cursor-timer (cancel-timer blink-cursor-timer))
;;     (setq blink-cursor-timer
;;           (run-with-timer blink-cursor-interval blink-cursor-interval
;;                           #'blink-cursor-timer-function)))

;;   (defun blink-cursor-start ()
;;     "Timer function called from the timer `blink-cursor-idle-timer'.
;;      This starts the timer `blink-cursor-timer', which makes the cursor blink
;;      if appropriate.  It also arranges to cancel that timer when the next
;;      command starts, by installing a pre-command hook."
;;     (when (null blink-cursor-timer)
;;       ;; Set up the timer first, so that if this signals an error,
;;       ;; blink-cursor-end is not added to pre-command-hook.
;;       (setq blink-cursor-blinks-done 1)
;;       (blink-cursor--start-timer)
;;       (add-hook 'pre-command-hook 'blink-cursor-end)
;;       (internal-show-cursor nil nil)))

;;   (defun blink-cursor-timer-function ()
;;     "Timer function of timer `blink-cursor-timer'."
;;     (internal-show-cursor nil (not (internal-show-cursor-p)))
;;     ;; Suspend counting blinks when the w32 menu-bar menu is displayed,
;;     ;; since otherwise menu tooltips will behave erratically.
;;     (or (and (fboundp 'w32--menu-bar-in-use)
;;              (w32--menu-bar-in-use))
;;         (setq blink-cursor-blinks-done (1+ blink-cursor-blinks-done)))
;;     ;; Each blink is two calls to this function.
;;     (when (and (> blink-cursor-blinks 0)
;;                (<= (* 2 blink-cursor-blinks) blink-cursor-blinks-done))
;;       (blink-cursor-suspend)
;;       (add-hook 'post-command-hook 'blink-cursor-check)))


;;   (defun blink-cursor-end ()
;;     "Stop cursor blinking.
;;      This is installed as a pre-command hook by `blink-cursor-start'.
;;      When run, it cancels the timer `blink-cursor-timer' and removes
;;      itself as a pre-command hook."
;;     (remove-hook 'pre-command-hook 'blink-cursor-end)
;;     (internal-show-cursor nil t)
;;     (when blink-cursor-timer
;;       (cancel-timer blink-cursor-timer)
;;       (setq blink-cursor-timer nil)))

;;   (defun blink-cursor-suspend ()
;;     "Suspend cursor blinking.
;;      This is called when no frame has focus and timers can be suspended.
;;      Timers are restarted by `blink-cursor-check', which is called when a
;;      frame receives focus."
;;     (blink-cursor-end)
;;     (when blink-cursor-idle-timer
;;       (cancel-timer blink-cursor-idle-timer)
;;       (setq blink-cursor-idle-timer nil)))

;;   (defun blink-cursor-check ()
;;     "Check if cursor blinking shall be restarted.
;;      This is done when a frame gets focus.  Blink timers may be stopped by
;;      `blink-cursor-suspend'."
;;     (when (and blink-cursor-mode
;;                (not blink-cursor-idle-timer))
;;       (remove-hook 'post-command-hook 'blink-cursor-check)
;;       (blink-cursor--start-idle-timer)))

;;   (define-obsolete-variable-alias 'blink-cursor 'blink-cursor-mode "22.1")

;;   (define-minor-mode blink-cursor-mode
;;     "Toggle cursor blinking (Blink Cursor mode).
;;      With a prefix argument ARG, enable Blink Cursor mode if ARG is
;;      positive, and disable it otherwise.  If called from Lisp, enable
;;      the mode if ARG is omitted or nil.

;;      If the value of `blink-cursor-blinks' is positive (10 by default),
;;      the cursor stops blinking after that number of blinks, if Emacs
;;      gets no input during that time.

;;      See also `blink-cursor-interval' and `blink-cursor-delay'.

;;      This command is effective only on graphical frames.  On text-only
;;      terminals, cursor blinking is controlled by the terminal."
;;     :init-value (not (or noninteractive
;;                          no-blinking-cursor
;;                          (eq system-type 'ms-dos)
;;                          (not (memq window-system '(x w32 ns)))))
;;     :initialize 'custom-initialize-delay
;;     :group 'cursor
;;     :global t
;;     (blink-cursor-suspend)
;;     (remove-hook 'focus-in-hook #'blink-cursor-check)
;;     (remove-hook 'focus-out-hook #'blink-cursor-suspend)
;;     (when blink-cursor-mode
;;       (add-hook 'focus-in-hook #'blink-cursor-check)
;;       (add-hook 'focus-out-hook #'blink-cursor-suspend)
;;       (blink-cursor--start-idle-timer)))

)

(provide 'terminal-cursor)
;;; terminal-cursor.el ends here
