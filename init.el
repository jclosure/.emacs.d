;;; Commentary:
;;; --- Emacs optimized for productivity in terminal
;;
;; Copyright (c) 2016-2019 Joel Holder
;;
;; Author: Joel Holder <jclosure@gmail.com>
;; Keywords: one-ring, small tools, church of Emacs
;;
;; This file is not part of GNU Emacs.

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; DETECT ENVIRONMENT
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; Documentation for `system-type':
;; Value is symbol indicating type of operating system you are using.
;; Special values:
;;   `gnu'         compiled for a GNU Hurd system.
;;   `gnu/linux'   compiled for a GNU/Linux system.
;;   `darwin'      compiled for Darwin (GNU-Darwin, MacOS, ...).
;;   `ms-dos'      compiled as an MS-DOS application.
;;   `windows-nt'  compiled as a native W32 application.
;;   `cygwin'      compiled using the Cygwin library.
;; Anything else indicates some sort of Unix system.

(defconst *is-a-mac* (eq system-type 'darwin))
(defconst *is-a-linux* (eq system-type 'gnu/linux))
(defconst *is-in-tmux* (getenv "TMUX"))
(defconst *is-in-ssh* (or (getenv "SSH_CLIENT") (getenv "SSH_TTY") (getenv "SSH_CONNECTION")))
(defconst *is-in-terminal* (not (display-graphic-p)))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; NATIVE COMP CONTROL
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; HACK: in the future reenable this
;; disable compilation warnings
(setq  native-comp-async-report-warnings-errors t)

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; SPECIFY CUSTOMIZATIONS SEPARATE FILE
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(setq-default custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file))

;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; LOAD CORE
;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

;; load path for all core files
(let ((base  (expand-file-name "core" user-emacs-directory)))
  (add-to-list 'load-path base)
  (dolist (f (directory-files base))
    (let ((name (concat base "/" f)))
      (when (and (file-directory-p name)
                 (not (equal f ".."))
                 (not (equal f ".")))
        (add-to-list 'load-path name)))))


(require 'initial)
(require 'conf)
(require 'orgs)
(require 'lang)
;;(require 'private)
(require 'tools)
(require 'perspectives)
(require 'extra)
(require 'final)
(require 'experimental)

;;(let ((use-package-always-defer nil))
  (require 'old-init-dotfiles)
 ;;)

;; Local Variables:
;; byte-compile-warnings: (not free-vars)
;; End:
(put 'downcase-region 'disabled nil)
